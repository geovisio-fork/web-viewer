# Viewer URL settings

Various settings could be set from URL hash part in order to create permalinks.

These are set after the `#` symbol of the URL, following a `key=value` format, each being separated by `&` symbol.

Example:

`https://geovisio.fr/viewer#map=19.51/48.1204522/-1.7199004&pic=890b6268-7716-4e34-ada9-69985e6c1657`

__Contents__

[[_TOC_]]


## Available parameters

### `focus`: main shown element

Switch to choose which element between map, picture or metadata should be shown wide at start. Examples:

- `focus=map`
- `focus=pic`
- `focus=meta`

By default, picture is shown wide.

### `map`: map position

The map current position (if map is enabled), based on [MapLibre GL JS hash format](https://maplibre.org/maplibre-gl-js-docs/api/map/#map-parameters):

`zoom/latitude/longitude`

Example:

`map=19.51/48.1204522/-1.7199004`

This parameter is automatically updated when map is moved. If missing, whole world map is shown by default.

### `pic`: picture ID

The currently selected picture ID. Example:

`pic=890b6268-7716-4e34-ada9-69985e6c1657`

### `xyz`: picture position

The shown position in picture, following this format:

`x/y/z`

With:

- `x`: the heading in degrees (0 = North, 90 = East, 180 = South, 270 = West)
- `y`: top/bottom position in degrees (-90 = bottom, 0 = front, 90 = top)
- `z`: zoom level (0 = minimum/wide view, 100 = maximum/full zoom)

Example:

`10/25/50`

### `date_from` and `date_to`: date filtering

Minimum and maximum capture date for pictures and sequences to show on map (if map is enabled), in ISO format:

`date_from=2020-01-01&date_to=2023-12-31`

### `pic_type`: picture type

The type of picture (360° or classic) to show on map (if map is enabled). Examples:

- `pic_type=flat` for classic pictures
- `pic_type=equirectangular` for 360° pictures
- Not set for showing both

### `camera`: camera make and model

The camera make and model to filter shown pictures and sequences on map (if map is enabled). A fuzzy search is used to filter on map, but your string _should_ always start with camera make. Examples:

- `camera=gopro` will display all pictures taken with any _GoPro_ camera
- `camera=gopro%20max` will only show pictures taken with a _GoPro Max_ camera
- `camera=max` will not shown any picture on map, as it doesn't match any camera make

### `speed`: sequence play speed

The duration of stay on a picture during sequence play (excluding image dowloading time), in milliseconds. Authorized values are between 0 and 3000. Example:

`speed=1000`

### `theme`: pictures and sequences map colouring

The map theme to use for displaying pictures and sequences (if map is enabled). Available themes are:

- `theme=default` (or no setting defined): single color for display (no classification)
- `theme=age`: color based on picture/sequence age (red = recent, yellow = 2+ years old)
- `theme=type`: color based on camera type (orange = classic, green = 360°) 

### `users`: user filter for map data

This parameter filters pictures and sequences shown on map to only keep those of concerned users. Each user is defined by its UUID (not username). List is comma-separated. Example:

`users=abcdefgh-1234-5678-9012-abcdefgh12345678,dcf0d3be-0418-4b71-9315-0ff8a2f86a0b`


## Next steps

You can give a look at [our advanced practical examples](./04_Advanced_examples.md).
