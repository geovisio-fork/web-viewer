<!-- Generated by documentation.js. Update this documentation by updating the source code. -->

### Table of Contents

*   [StandaloneMap][1]
    *   [Parameters][2]
    *   [fitBounds][3]
        *   [Parameters][4]
    *   [reloadVectorTiles][5]
    *   [select][6]
        *   [Parameters][7]
*   [hover][8]
    *   [Properties][9]
*   [ready][10]
*   [ready][11]
*   [select][12]
    *   [Properties][13]
*   [Viewer][14]
    *   [Parameters][15]
    *   [destroy][16]
    *   [setPopup][17]
        *   [Parameters][18]
    *   [getXY][19]
    *   [getXYZ][20]
    *   [getPictureMetadata][21]
    *   [getMap][22]
    *   [getPhotoViewer][23]
    *   [goToPicture][24]
        *   [Parameters][25]
    *   [playSequence][26]
    *   [stopSequence][27]
    *   [isSequencePlaying][28]
    *   [toggleSequencePlaying][29]
    *   [goToNextPicture][30]
    *   [goToPrevPicture][31]
    *   [goToPosition][32]
        *   [Parameters][33]
    *   [moveCenter][34]
    *   [moveLeft][35]
    *   [moveRight][36]
    *   [moveUp][37]
    *   [moveDown][38]
    *   [isMapWide][39]
    *   [isSmall][40]
    *   [isHeightSmall][41]
    *   [reloadVectorTiles][42]
    *   [getMapBackground][43]
    *   [getVisibleUsers][44]
    *   [switchVisibleUsers][45]
        *   [Parameters][46]
    *   [toggleJOSMLive][47]
        *   [Parameters][48]
    *   [setFocus][49]
        *   [Parameters][50]
    *   [toggleFocus][51]
    *   [setUnfocusedVisible][52]
        *   [Parameters][53]
    *   [toggleUnfocusedVisible][54]
    *   [setPictureHigherContrast][55]
        *   [Parameters][56]
    *   [setXYZ][57]
        *   [Parameters][58]
    *   [getTransitionDuration][59]
    *   [setTransitionDuration][60]
        *   [Parameters][61]
    *   [setFilters][62]
        *   [Parameters][63]
*   [picture-preview-started][64]
    *   [Properties][65]
*   [picture-preview-stopped][66]
    *   [Properties][67]
*   [view-rotated][68]
    *   [Properties][69]
*   [picture-loaded][70]
    *   [Properties][71]
*   [picture-loading][72]
    *   [Properties][73]
*   [picture-tiles-loaded][74]
    *   [Properties][75]
*   [sequence-playing][76]
*   [sequence-stopped][77]
*   [map-background-changed][78]
    *   [Properties][79]
*   [users-changed][80]
    *   [Properties][81]
*   [josm-live-enabled][82]
*   [josm-live-disabled][83]
*   [focus-changed][84]
    *   [Properties][85]
*   [transition-duration-changed][86]
    *   [Properties][87]
*   [map-filters-changed][88]
    *   [Properties][89]
*   [createSearchBar][90]
    *   [Parameters][91]
*   [enableButton][92]
    *   [Parameters][93]
*   [disableButton][94]
    *   [Parameters][95]
*   [getThumbGif][96]
    *   [Parameters][97]
*   [createPicturesTilesLayer][98]
    *   [Parameters][99]
*   [reloadLayersStyles][100]
    *   [Parameters][101]
*   [filterUserLayersContent][102]
    *   [Parameters][103]
*   [switchUserLayers][104]
    *   [Parameters][105]
*   [reloadVectorTiles][106]
    *   [Parameters][107]
*   [attachPreviewToPictures][108]
    *   [Parameters][109]
*   [getSequenceThumbURL][110]
    *   [Parameters][111]
*   [forwardGeocodingNominatim][112]
    *   [Parameters][113]
*   [forwardGeocodingBAN][114]
    *   [Parameters][115]
*   [getTranslations][116]
    *   [Parameters][117]

## StandaloneMap

**Extends EventTarget**

The standalone map viewer allows to see STAC pictures data as a map.
It only embeds a map (no 360° pictures viewer) with a minimal picture preview (thumbnail).

### Parameters

*   `container` **([string][118] | [Node][119])** The DOM element to create viewer into
*   `endpoint` **[string][118]** URL to API to use (must be a [STAC API][120])
*   `options` **[object][121]?** Map options. Various settings can be passed, either the ones defined here, or any of [MapLibre GL settings][122]. (optional, default `{}`)

    *   `options.selectedSequence` **[string][118]?** The ID of sequence to highlight on load (defaults to none)
    *   `options.selectedPicture` **[string][118]?** The ID of picture to highlight on load (defaults to none)
    *   `options.fetchOptions` **[object][121]** Set custom options for fetch calls made against API ([same syntax as fetch options parameter][123]) (optional, default `null`)
    *   `options.picturesTiles` **[string][118]?** URL for fetching pictures vector tiles if map is enabled (defaults to "xyz" link advertised in STAC API landing page)
    *   `options.minZoom` **[number][124]** The minimum zoom level of the map (0-24). (optional, default `0`)
    *   `options.maxZoom` **[number][124]** The maximum zoom level of the map (0-24). (optional, default `24`)
    *   `options.style` **([object][121] | [string][118])?** The map's MapLibre style. This must be an a JSON object conforming to the schema described in the [MapLibre Style Specification][125], or a URL to such JSON.
        For example, `http://path/to/my/page.html#2.59/39.26/53.07/-24.1/60`.
        An additional string may optionally be provided to indicate a parameter-styled hash, e.g. [http://path/to/my/page.html#map=2.59/39.26/53.07/-24.1/60\&foo=bar][126], where foo is a custom parameter and bar is an arbitrary hash distinct from the map hash.
    *   `options.center` **external:maplibre-gl.LngLatLike** The initial geographical centerpoint of the map. If `center` is not specified in the constructor options, MapLibre GL JS will look for it in the map's style object. If it is not specified in the style, either, it will default to `[0, 0]` Note: MapLibre GL uses longitude, latitude coordinate order (as opposed to latitude, longitude) to match GeoJSON. (optional, default `[0,0]`)
    *   `options.zoom` **[number][124]** The initial zoom level of the map. If `zoom` is not specified in the constructor options, MapLibre GL JS will look for it in the map's style object. If it is not specified in the style, either, it will default to `0`. (optional, default `0`)
    *   `options.bounds` **external:maplibre-gl.LngLatBoundsLike?** The initial bounds of the map. If `bounds` is specified, it overrides `center` and `zoom` constructor options.
    *   `options.users` **[Array][127]<[string][118]>?** The IDs of users whom data should appear on map (defaults to all)

### fitBounds

Make map fit in given bounding box.
For more details on options, see [https://maplibre.org/maplibre-gl-js/docs/API/classes/maplibregl.Map/#fitbounds][128]

#### Parameters

*   `bounds` &#x20;
*   `options`   (optional, default `null`)

### reloadVectorTiles

Force refresh of GeoVisio vector tiles on the map.

### select

Highlights a certain sequence/picture on map.

#### Parameters

*   `seqId` **[string][118]?** The sequence ID (or null to unselect) (optional, default `null`)
*   `picId` **[string][118]?** The picture ID (or null to only select sequence) (optional, default `null`)

## hover

Event when a sequence on map is hovered (not selected)

Type: [object][121]

### Properties

*   `seqId` **[string][118]** The hovered sequence ID

## ready

Event for map being ready to use (API and data loaded)

Type: [object][121]

## ready

Event for viewer being ready to use (API loaded)

Type: [object][121]

## select

Event for sequence/picture selection

Type: [object][121]

### Properties

*   `seqId` **[string][118]** The selected sequence ID
*   `picId` **[string][118]** The selected picture ID (or null if not a precise picture clicked)

## Viewer

**Extends EventTarget**

Viewer is the main component of GeoVisio, showing pictures and map

### Parameters

*   `container` **([string][118] | [Node][119])** The DOM element to create viewer into
*   `endpoint` **[string][118]** URL to API to use (must be a [STAC API][120])
*   `options` **[object][121]?** Viewer options (optional, default `{}`)

    *   `options.picId` **[string][118]?** Initial picture identifier to display
    *   `options.position` **[Array][127]<[number][124]>?** Initial position to go to (in \[lat, lon] format)
    *   `options.hash` **[boolean][129]** Enable URL hash settings (optional, default `true`)
    *   `options.lang` **[string][118]?** Override language to use (defaults to navigator language, or English if translation not available)
    *   `options.transition` **int** Duration of stay on a picture during sequence play (excludes loading time) (optional, default `250`)
    *   `options.fetchOptions` **[object][121]** Set custom options for fetch calls made against API ([same syntax as fetch options parameter][123]) (optional, default `null`)
    *   `options.map` **([boolean][129] | [object][121])** Enable contextual map for locating pictures. Setting to true or passing an object enables the map. Various settings can be passed, either the ones defined here, or any of [MapLibre GL settings][122] (optional, default `false`)

        *   `options.map.picturesTiles` **[string][118]?** URL for fetching pictures vector tiles if map is enabled (defaults to "xyz" link advertised in STAC API landing page)
        *   `options.map.startWide` **[boolean][129]?** Show the map as main element at startup (defaults to false, viewer is wider at start)
        *   `options.map.minZoom` **[number][124]** The minimum zoom level of the map (0-24). (optional, default `0`)
        *   `options.map.maxZoom` **[number][124]** The maximum zoom level of the map (0-24). (optional, default `24`)
        *   `options.map.style` **([object][121] | [string][118])?** The MapLibre style for streets background. This must be an a JSON object conforming to the schema described in the [MapLibre Style Specification][125], or a URL to such JSON.
        *   `options.map.raster` **[object][121]?** The MapLibre raster source for aerial background. This must be a JSON object following [MapLibre raster source definition][130].
        *   `options.map.center` **external:maplibre-gl.LngLatLike** The initial geographical centerpoint of the map. If `center` is not specified in the constructor options, MapLibre GL JS will look for it in the map's style object. If it is not specified in the style, either, it will default to `[0, 0]` Note: MapLibre GL uses longitude, latitude coordinate order (as opposed to latitude, longitude) to match GeoJSON. (optional, default `[0,0]`)
        *   `options.map.zoom` **[number][124]** The initial zoom level of the map. If `zoom` is not specified in the constructor options, MapLibre GL JS will look for it in the map's style object. If it is not specified in the style, either, it will default to `0`. (optional, default `0`)
        *   `options.map.bounds` **external:maplibre-gl.LngLatBoundsLike?** The initial bounds of the map. If `bounds` is specified, it overrides `center` and `zoom` constructor options.
        *   `options.map.geocoder` **[object][121]?** Optional geocoder settings
        *   `options.map.theme` **[string][118]** The colouring scheme to use for pictures and sequences on map (default, age, type) (optional, default `default`)
        *   `options.map.users` **[Array][127]<[string][118]>?** The IDs of users whom data should appear on map (defaults to all). Only works with API having a "user-xyz" endpoint.
    *   `options.widgets` **[object][121]?** Settings related to viewer buttons and widgets

        *   `options.widgets.editIdUrl` **[string][118]?** URL to the OpenStreetMap iD editor (defaults to OSM.org iD instance)
        *   `options.widgets.customWidget` **([string][118] | [Element][131])?** A user-defined widget to add (will be shown over "Share" button)
        *   `options.widgets.mapAttribution` **[string][118]?** Override the default map attribution (read from MapLibre style)

### destroy

Ends all form of life in this object.

This is useful for Single Page Applications (SPA), to remove various event listeners.

### setPopup

Change full-page popup visibility and content

#### Parameters

*   `visible` **[boolean][129]** True to make it appear
*   `content` **([string][118] | [Array][127]<[Element][131]>)?** The new popup content (optional, default `null`)

### getXY

Get 2D position of sphere currently shown to user

Returns **[object][121]** Position in format { x: heading in degrees (0° = North, 90° = East, 180° = South, 270° = West), y: top/bottom position in degrees (-90° = bottom, 0° = front, 90° = top) }

### getXYZ

Get 3D position of sphere currently shown to user

Returns **[object][121]** Position in format { x: heading in degrees (0° = North, 90° = East, 180° = South, 270° = West), y: top/bottom position in degrees (-90° = bottom, 0° = front, 90° = top), z: zoom (0 = wide, 100 = zoomed in) }

### getPictureMetadata

Access currently shown picture metadata

Returns **[object][121]** Picture metadata

### getMap

Access the map object (if any)
Allows you to call any of [the MapLibre GL Map functions][132] for advanced map management

Returns **(null | external:maplibre-gl.Map)** The map

### getPhotoViewer

Access the photo viewer object
Allows you to call any of the [Photo Sphere Viewer functions][133]

Returns **external:@photo-sphere-viewer/core.Viewer** The photo viewer

### goToPicture

Displays in viewer specified picture

#### Parameters

*   `picId` **[string][118]** The picture unique identifier
*   `seqId` **[string][118]?** The sequence ID this picture belongs to. Optional, it allows faster retrieval from API

Returns **[Promise][134]** Resolves on picture loaded

### playSequence

Goes continuously to next picture in sequence as long as possible

### stopSequence

Stops playing current sequence

### isSequencePlaying

Is there any sequence being played right now ?

Returns **[boolean][129]** True if sequence is playing

### toggleSequencePlaying

Starts/stops the reading of pictures in a sequence

### goToNextPicture

Displays next picture in current sequence (if any)

### goToPrevPicture

Displays previous picture in current sequence (if any)

### goToPosition

Displays in viewer a picture near to given coordinates

#### Parameters

*   `lat` **[number][124]** Latitude (WGS84)
*   `lon` **[number][124]** Longitude (WGS84)

Returns **[Promise][134]** Resolves on picture ID if picture found, otherwise rejects

### moveCenter

Move the view of main component to its center.
For map, center view on selected picture.
For picture, center view on image center.

### moveLeft

Moves the view of main component slightly to the left.

### moveRight

Moves the view of main component slightly to the right.

### moveUp

Moves the view of main component slightly to the top.

### moveDown

Moves the view of main component slightly to the bottom.

### isMapWide

Is the map shown as main element instead of viewer (wide map mode) ?

Returns **[boolean][129]** True if map is wider than viewer

### isSmall

Is the viewer running in a small container (small embed or smartphone)

Returns **[boolean][129]** True if container is small

### isHeightSmall

Is the viewer running in a small-height container (small embed or smartphone)

Returns **[boolean][129]** True if container height is small

### reloadVectorTiles

Force refresh of GeoVisio vector tiles on the map.

### getMapBackground

Get the currently selected map background

Returns **[string][118]** aerial or streets

### getVisibleUsers

Get the currently visible users

Returns **[Array][127]<[string][118]>** List of visible users

### switchVisibleUsers

Change the visible users on map

#### Parameters

*   `usersIds` **[Array][127]<[string][118]>** List of user IDs to display (plus "geovisio" alias for everyone)

### toggleJOSMLive

Enable or disable JOSM live editing using [Remote][135]

#### Parameters

*   `enabled` **[boolean][129]** Set to true to enable JOSM live

Returns **[Promise][134]** Resolves on JOSM live being enabled or disabled

### setFocus

Change the viewer focus (either on picture or map)

#### Parameters

*   `focus` **[string][118]** The object to focus on (map, pic)
*   `skipEvent` **[boolean][129]** True to not send focus-changed event (optional, default `false`)

### toggleFocus

Toggle the viewer focus (either on picture or map)

### setUnfocusedVisible

Change the visibility of reduced component (picture or map)

#### Parameters

*   `visible` **[boolean][129]** True to make reduced component visible

### toggleUnfocusedVisible

Toggle the visibility of reduced component (picture or map)

### setPictureHigherContrast

Enable or disable higher contrast on picture

#### Parameters

*   `enable` **[boolean][129]** True to enable higher contrast

### setXYZ

Change the shown position in picture

#### Parameters

*   `x` **[number][124]** X position (in degrees)
*   `y` **[number][124]** Y position (in degrees)
*   `z` **[number][124]** Z position (0-100)

### getTransitionDuration

Get the duration of stay on a picture during a sequence play.

Returns **[number][124]** The duration (in milliseconds)

### setTransitionDuration

Changes the duration of stay on a picture during a sequence play.

#### Parameters

*   `value` **[number][124]** The new duration (in milliseconds, between 0 and 3000)

### setFilters

Change the map filters

#### Parameters

*   `filters` **[object][121]** Filtering values

    *   `filters.minDate` **[string][118]?** Start date for pictures (format YYYY-MM-DD)
    *   `filters.maxDate` **[string][118]?** End date for pictures (format YYYY-MM-DD)
    *   `filters.type` **[string][118]?** Type of picture to keep (flat, equirectangular)
    *   `filters.camera` **[string][118]?** Camera make and model to keep
    *   `filters.theme` **[string][118]?** Map theme to use
*   `skipZoomIn` **[boolean][129]** If true, doesn't force zoom in to map level >= 7 (optional, default `false`)

## picture-preview-started

Event for picture preview

Type: [object][121]

### Properties

*   `picId` **[string][118]** The picture ID
*   `coordinates` **[Array][127]<[number][124]>** \[x,y] coordinates
*   `direction` **[number][124]** The theorical picture orientation

## picture-preview-stopped

Event for end of picture preview

Type: [object][121]

### Properties

*   `picId` **[string][118]** The picture ID

## view-rotated

Event for viewer rotation

Type: [object][121]

### Properties

*   `x` **[number][124]** New x position (in degrees, 0-360), corresponds to heading (0° = North, 90° = East, 180° = South, 270° = West)
*   `y` **[number][124]** New y position (in degrees)
*   `z` **[number][124]** New Z position (between 0 and 100)

## picture-loaded

Event for picture load (low-resolution image is loaded)

Type: [object][121]

### Properties

*   `picId` **[string][118]** The picture unique identifier
*   `lon` **[number][124]** Longitude (WGS84)
*   `lat` **[number][124]** Latitude (WGS84)
*   `x` **[number][124]** New x position (in degrees, 0-360), corresponds to heading (0° = North, 90° = East, 180° = South, 270° = West)
*   `y` **[number][124]** New y position (in degrees)
*   `z` **[number][124]** New z position (0-100)

## picture-loading

Event for picture starting to load

Type: [object][121]

### Properties

*   `picId` **[string][118]** The picture unique identifier
*   `lon` **[number][124]** Longitude (WGS84)
*   `lat` **[number][124]** Latitude (WGS84)
*   `x` **[number][124]** New x position (in degrees, 0-360), corresponds to heading (0° = North, 90° = East, 180° = South, 270° = West)
*   `y` **[number][124]** New y position (in degrees)
*   `z` **[number][124]** New z position (0-100)

## picture-tiles-loaded

Event launched when all visible tiles of a picture are loaded

Type: [object][121]

### Properties

*   `picId` **[string][118]** The picture unique identifier

## sequence-playing

Event for sequence starting to play

Type: [object][121]

## sequence-stopped

Event for sequence stopped playing

Type: [object][121]

## map-background-changed

Event for map background changes

Type: [object][121]

### Properties

*   `background` **[string][118]?** The new selected background (aerial, streets)

## users-changed

Event for visible users changes

Type: [object][121]

### Properties

*   `usersIds` **[Array][127]<[string][118]>?** The list of newly selected users

## josm-live-enabled

Event for JOSM live enabled

## josm-live-disabled

Event for JOSM live disabled

## focus-changed

Event for focus change (either map or picture is shown wide)

Type: [object][121]

### Properties

*   `focus` **[string][118]** Component now focused on (map, pic)

## transition-duration-changed

Event for transition duration change

Type: [object][121]

### Properties

*   `duration` **[string][118]** New duration (in milliseconds)

## map-filters-changed

Event for map filters changes

Type: [object][121]

### Properties

*   `minDate` **[string][118]?** The minimum date in time range (ISO format)
*   `maxDate` **[string][118]?** The maximum date in time range (ISO format)
*   `type` **[string][118]?** Camera type (equirectangular, flat, null/empty string for both)
*   `camera` **[string][118]?** Camera make and model
*   `theme` **[string][118]?** Map theme

## createSearchBar

Creates a new search bar

### Parameters

*   `id` **[string][118]** The bar ID
*   `placeholder` **[string][118]** The default label to display when search field is empty
*   `onInput` **[function][136]** Event handler for search text input (should return a Promise)
*   `onResultClick` **[function][136]** Event handler for result entry click
*   `container` **Widgets** The widgets container
*   `nonClosingPanel` **[boolean][129]** Should the search result closes other panels (optional, default `false`)

## enableButton

Make a button usable

### Parameters

*   `btn` **[Element][131]**&#x20;

## disableButton

Make a button unusable

### Parameters

*   `btn` **[Element][131]**&#x20;

## getThumbGif

Get the GIF shown while thumbnail loads

### Parameters

*   `lang` **[object][121]** Translations

Returns **any** The DOM element for this GIF

## createPicturesTilesLayer

Creates source and layers for pictures and sequences.

### Parameters

*   `mapHandler` **[object][121]** The main object containing a MaplibreGL "\_map" property
*   `api` **API** The API handler
*   `tilesUrl` **[string][118]** The vector tile source URL
*   `isSmall` **[boolean][129]** Is the map shown on a small screen
*   `id` **[string][118]** The source and layer ID prefix
*   `seqLayerStyle` **[object][121]** The MaplibreGL JS style for sequences
*   `picLayerStyle` **[object][121]** The MaplibreGL JS style for pictures
*   `onPicClick` **[function][136]** Click handler for pictures layer
*   `onSeqClick` **[function][136]** Click handler for sequences layer

## reloadLayersStyles

Forces reload of pictures/sequences layer styles.
This is useful after a map theme change.

### Parameters

*   `mapHandler` **[object][121]** The main object containing a MaplibreGL "\_map" property

## filterUserLayersContent

Filter the visible data content in all visible map layers

### Parameters

*   `mapHandler` **[object][121]** The main object containing a MaplibreGL "\_map" property
*   `dataType` **[string][118]** sequences or pictures
*   `filter` **[object][121]** The MapLibre GL filter rule to apply

## switchUserLayers

Make given user layers visible on map, and hide all others (if any)

### Parameters

*   `mapHandler` **[object][121]** The main object containing a MaplibreGL "\_map" property
*   `visibleIds` **[Array][127]<[string][118]>** The user layers IDs to display
*   `api` **API** The API handler
*   `seqLayerStyle` **[object][121]** The MaplibreGL JS style for sequences
*   `picLayerStyle` **[object][121]** The MaplibreGL JS style for pictures
*   `onPicClick` **[function][136]** Click handler for pictures layer
*   `onSeqClick` **[function][136]** Click handler for sequences layer

## reloadVectorTiles

Creates popup manager for preview of pictures.

### Parameters

*   `mapHandler` **[object][121]** The main object containing a MaplibreGL "\_map" property

## attachPreviewToPictures

Creates popup manager for preview of pictures.

### Parameters

*   `mapHandler` **[object][121]** The main object containing a MaplibreGL "\_map" property
*   `api` **API** The API handler
*   `e` **[object][121]** The event thrown by MapLibre
*   `from` **[string][118]** The event source layer

## getSequenceThumbURL

Get picture thumbnail URL for a given sequence ID
It handles a client-side cache based on raw API responses.

### Parameters

*   `mapHandler` **[object][121]** The main object containing a MaplibreGL "\_map" property
*   `api` **API** The API handler
*   `seqId` **[string][118]** The sequence ID
*   `coordinates` **LngLat?** The map coordinates

Returns **[Promise][134]** Promise resolving on picture thumbnail URL, or null on timeout

## forwardGeocodingNominatim

Nominatim (OSM) geocoder, ready to use for our Map

### Parameters

*   `config` &#x20;

## forwardGeocodingBAN

Base adresse nationale (FR) geocoder, ready to use for our Map

### Parameters

*   `config` **[object][121]** Configuration sent by MapLibre GL Geocoder, following the geocoderApi format ( [https://github.com/maplibre/maplibre-gl-geocoder/blob/main/API.md#setgeocoderapi][137] )

Returns **[object][121]** GeoJSON Feature collection in Carmen GeoJSON format

## getTranslations

Get text labels translations in given language

### Parameters

*   `lang` **[string][118]** The language code (fr, en) (optional, default `""`)

Returns **[object][121]** Translations in given language, with fallback to english

[1]: #standalonemap

[2]: #parameters

[3]: #fitbounds

[4]: #parameters-1

[5]: #reloadvectortiles

[6]: #select

[7]: #parameters-2

[8]: #hover

[9]: #properties

[10]: #ready

[11]: #ready-1

[12]: #select-1

[13]: #properties-1

[14]: #viewer

[15]: #parameters-3

[16]: #destroy

[17]: #setpopup

[18]: #parameters-4

[19]: #getxy

[20]: #getxyz

[21]: #getpicturemetadata

[22]: #getmap

[23]: #getphotoviewer

[24]: #gotopicture

[25]: #parameters-5

[26]: #playsequence

[27]: #stopsequence

[28]: #issequenceplaying

[29]: #togglesequenceplaying

[30]: #gotonextpicture

[31]: #gotoprevpicture

[32]: #gotoposition

[33]: #parameters-6

[34]: #movecenter

[35]: #moveleft

[36]: #moveright

[37]: #moveup

[38]: #movedown

[39]: #ismapwide

[40]: #issmall

[41]: #isheightsmall

[42]: #reloadvectortiles-1

[43]: #getmapbackground

[44]: #getvisibleusers

[45]: #switchvisibleusers

[46]: #parameters-7

[47]: #togglejosmlive

[48]: #parameters-8

[49]: #setfocus

[50]: #parameters-9

[51]: #togglefocus

[52]: #setunfocusedvisible

[53]: #parameters-10

[54]: #toggleunfocusedvisible

[55]: #setpicturehighercontrast

[56]: #parameters-11

[57]: #setxyz

[58]: #parameters-12

[59]: #gettransitionduration

[60]: #settransitionduration

[61]: #parameters-13

[62]: #setfilters

[63]: #parameters-14

[64]: #picture-preview-started

[65]: #properties-2

[66]: #picture-preview-stopped

[67]: #properties-3

[68]: #view-rotated

[69]: #properties-4

[70]: #picture-loaded

[71]: #properties-5

[72]: #picture-loading

[73]: #properties-6

[74]: #picture-tiles-loaded

[75]: #properties-7

[76]: #sequence-playing

[77]: #sequence-stopped

[78]: #map-background-changed

[79]: #properties-8

[80]: #users-changed

[81]: #properties-9

[82]: #josm-live-enabled

[83]: #josm-live-disabled

[84]: #focus-changed

[85]: #properties-10

[86]: #transition-duration-changed

[87]: #properties-11

[88]: #map-filters-changed

[89]: #properties-12

[90]: #createsearchbar

[91]: #parameters-15

[92]: #enablebutton

[93]: #parameters-16

[94]: #disablebutton

[95]: #parameters-17

[96]: #getthumbgif

[97]: #parameters-18

[98]: #createpicturestileslayer

[99]: #parameters-19

[100]: #reloadlayersstyles

[101]: #parameters-20

[102]: #filteruserlayerscontent

[103]: #parameters-21

[104]: #switchuserlayers

[105]: #parameters-22

[106]: #reloadvectortiles-2

[107]: #parameters-23

[108]: #attachpreviewtopictures

[109]: #parameters-24

[110]: #getsequencethumburl

[111]: #parameters-25

[112]: #forwardgeocodingnominatim

[113]: #parameters-26

[114]: #forwardgeocodingban

[115]: #parameters-27

[116]: #gettranslations

[117]: #parameters-28

[118]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String

[119]: https://developer.mozilla.org/docs/Web/API/Node/nextSibling

[120]: https://github.com/radiantearth/stac-api-spec/blob/main/overview.md

[121]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object

[122]: https://maplibre.org/maplibre-gl-js-docs/api/map/#map-parameters

[123]: https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters

[124]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number

[125]: https://maplibre.org/maplibre-gl-js-docs/style-spec/

[126]: http://path/to/my/page.html#map=2.59/39.26/53.07/-24.1/60&foo=bar

[127]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array

[128]: https://maplibre.org/maplibre-gl-js/docs/API/classes/maplibregl.Map/#fitbounds

[129]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean

[130]: https://maplibre.org/maplibre-style-spec/sources/#raster

[131]: https://developer.mozilla.org/docs/Web/API/Element

[132]: https://maplibre.org/maplibre-gl-js-docs/api/map/

[133]: https://photo-sphere-viewer.js.org/api/classes/core.viewer

[134]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Promise

[135]: https://josm.openstreetmap.de/wiki/Help/RemoteControlCommands

[136]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function

[137]: https://github.com/maplibre/maplibre-gl-geocoder/blob/main/API.md#setgeocoderapi
