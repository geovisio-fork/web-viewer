/**
 * API contains various utility functions to communicate with the backend
 *
 * @param {string} endpoint The API endpoint. It corresponds to the <a href="https://github.com/radiantearth/stac-api-spec/blob/main/overview.md#example-landing-page">STAC landing page</a>, with all links describing the API capabilites.
 * @param {object} [options] Options received from viewer that may change API behaviour
 * @param {string} [options.tiles] API route serving pictures & sequences vector tiles
 * @param {boolean} [options.skipReadLanding] True to not call API landing page automatically (defaults to false)
 * @param {object} [options.fetch] Set custom options for fetch calls made against API ([same syntax as fetch options parameter](https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters))
 * @private
 */
class API {
	constructor(endpoint, options = {}) {
		if(endpoint === null || endpoint === undefined || typeof endpoint !== "string") {
			throw new Error("endpoint parameter is empty or not a valid string");
		}

		// Parse local endpoints
		if(endpoint.startsWith("/")) {
			endpoint = window.location.href.split("/").slice(0, 3).join("/") + endpoint;
		}

		// Check endpoint
		if(!API.isValidHttpUrl(endpoint)) {
			throw new Error(`endpoint parameter is not a valid URL: ${endpoint}`);
		}

		this._endpoint = endpoint;
		this._isReady = 0;
		this._dataBbox = null;
		this._fetchOpts = options?.fetch || {};

		if(options.skipReadLanding) { return; }
		this._readLanding = fetch(endpoint, this._getFetchOptions())
			.then(res => res.json())
			.then(landing => this._parseLanding(landing, options))
			.then(() => {
				// Fallback for GeoVisio API <= 2.0.1 for vector tiles
				if(!this._endpoints.tiles) {
					return fetch(`${this._endpoint}/map/0/0/0.mvt`, this._getFetchOptions())
						.then(() => {
							this._endpoints.tiles = `${this._endpoint}/map/{z}/{x}/{y}.mvt`;
							console.log("Using fallback endpoint for vector tiles");
						})
						.finally(() => {
							this._isReady = 1;
							return "API is ready";
						});
				}
				else {
					this._isReady = 1;
					return "API is ready";
				}
			})
			.catch(e => {
				this._isReady = -1;
				console.error(e);
				return "Viewer failed to communicate with API";
			});
	}

	/**
	 * This function resolves when API is ready to be used
	 *
	 * @returns {Promise} Resolves when API is ready
	 */
	onceReady() {
		if(this._isReady == -1) {
			return Promise.reject("Viewer failed to communicate with API");
		}
		else if(this._isReady == 1) {
			return Promise.resolve("API is ready");
		}
		else {
			return this._readLanding;
		}
	}

	/**
	 * Check if API is ready to be used
	 *
	 * @returns {boolean} True if ready
	 */
	isReady() {
		return this._isReady == 1;
	}

	/**
	 * Interprets JSON landing page and store important information
	 *
	 * @private
	 */
	_parseLanding(landing, options) {
		this._endpoints = {
			"collections": null,
			"search": null,
			"tiles": options?.tiles || null,
			"user_tiles": null,
			"user_search": null,
			"collection_preview": null,
			"item_preview": null,
			"rss": null,
		};

		if(!landing || !landing.links || !Array.isArray(landing.links)) {
			throw new Error("API Landing page doesn't contain 'links' list");
		}

		if(!landing.stac_version.startsWith("1.0")) {
			throw new Error(`API is not in a supported version (GeoVisio viewer supports only 1.0, API is ${landing.stac_version})`);
		}

		// Read links
		for(let link of landing.links) {
			if(link.rel == "search" && link.type == "application/geo+json") {
				if(!API.isValidHttpUrl(link.href)) {
					throw new Error(`API search endpoint is not a valid URL: ${link.href}`);
				}
				this._endpoints.search = link.href;
			}

			if(link.rel == "data" && link.type == "application/json") {
				if(!API.isValidHttpUrl(link.href)) {
					throw new Error(`API data endpoint (application/json) is not a valid URL: ${link.href}`);
				}
				this._endpoints.collections = link.href;
			}

			if(link.rel == "data" && link.type == "application/rss+xml") {
				if(!API.isValidHttpUrl(link.href)) {
					throw new Error(`API data endpoint (application/rss+xml) is not a valid URL: ${link.href}`);
				}
				this._endpoints.rss = link.href;
			}

			if(!this._endpoints.tiles && link.rel == "xyz" && link.type == "application/vnd.mapbox-vector-tile") {
				if(!API.isValidHttpUrl(link.href)) {
					throw new Error(`API xyz endpoint is not a valid URL: ${link.href}`);
				}
				this._endpoints.tiles = link.href;
			}

			if(link.rel == "user-xyz" && link.type == "application/vnd.mapbox-vector-tile") {
				if(!API.isValidHttpUrl(link.href)) {
					throw new Error(`API user-xyz endpoint is not a valid URL: ${link.href}`);
				}
				this._endpoints.user_tiles = link.href;
			}

			if(link.rel == "user-search" && link.type == "application/json") {
				if(!API.isValidHttpUrl(link.href)) {
					throw new Error(`API user-search endpoint is not a valid URL: ${link.href}`);
				}
				this._endpoints.user_search = link.href;
			}

			if(link.rel == "collection-preview" && link.type == "image/jpeg") {
				if(!API.isValidHttpUrl(link.href)) {
					throw new Error(`API collection-preview endpoint is not a valid URL: ${link.href}`);
				}
				this._endpoints.collection_preview = link.href;
			}

			if(link.rel == "item-preview" && link.type == "image/jpeg") {
				if(!API.isValidHttpUrl(link.href)) {
					throw new Error(`API item-preview endpoint is not a valid URL: ${link.href}`);
				}
				this._endpoints.item_preview = link.href;
			}
		}

		// Check if mandatory links are set
		if(!this._endpoints.collections) {
			throw new Error("API doesn't offer a 'data' endpoint in its links");
		}
		if(!this._endpoints.search) {
			throw new Error("API doesn't offer a 'search' endpoint in its links");
		}
		if(!this._endpoints.tiles) {
			console.warn("API doesn't offer a 'xyz' endpoint in its links.\nMap widget will not be available.");
		}
		if(!this._endpoints.user_tiles) {
			console.warn("API doesn't offer a 'user-xyz' endpoint in its links.\nFilter map data by user ID will not be available.");
		}
		if(!this._endpoints.user_search) {
			console.warn("API doesn't offer a 'user-search' endpoint in its links.\nFilter map data by user name will not be available.");
		}
		if(!this._endpoints.collection_preview) {
			console.warn("API doesn't offer a 'collection-preview' endpoint in its links.\nDisplay of thumbnail could be slower.");
		}
		if(!this._endpoints.item_preview) {
			console.warn("API doesn't offer a 'item-preview' endpoint in its links.\nDisplay of thumbnail could be slower.");
		}

		// Look for data BBox
		const bbox = landing?.extent?.spatial?.bbox;
		this._dataBbox = (
			bbox &&
			Array.isArray(bbox) &&
			bbox.length > 0 &&
			Array.isArray(bbox[0]) && bbox[0].length === 4
		) ?
			[[bbox[0][0], bbox[0][1]], [bbox[0][2], bbox[0][3]]]
			: null;

		this._isReady = 1;
	}

	/**
	 * Get the defaults fetch options to pass during API calls
	 *
	 * @private
	 * @returns {object} The fetch options
	 */
	_getFetchOptions() {
		return Object.assign({}, this._fetchOpts);
	}

	/**
	 * Get the RequestTransformFunction for MapLibre to handle fetch options
	 *
	 * @private
	 * @returns {function} The RequestTransformFunction
	 */
	_getMapRequestTransform() {
		// Only if tiles endpoint is enabled and fetch options set
		if(this._endpoints.tiles && Object.keys(this._getFetchOptions()).length > 0) {
			return (url) => {
				// As MapLibre will use this function for all its calls
				// We must make sure fetch options are sent only for
				// the STAC API calls, particularly the tiles endpoint
				const tilesRoot = this._endpoints.tiles.replace("{z}/{x}/{y}.mvt", "");
				const userTilesRoot = this._endpoints.user_tiles ? this._endpoints.user_tiles.replace("{z}/{x}/{y}.mvt", "") : null;
				if(url.startsWith(tilesRoot) || (userTilesRoot && url.startsWith(userTilesRoot))) {
					return {
						url,
						...this._getFetchOptions()
					};
				}
			};
		}
	}

	/**
	 * Get full URL for listing pictures around a specific location
	 *
	 * @param {number} lat Latitude
	 * @param {number} lon Longitude
	 * @returns {string} The corresponding URL
	 */
	getPicturesAroundCoordinatesUrl(lat, lon) {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }

		if(isNaN(parseFloat(lat)) || isNaN(parseFloat(lon))) {
			throw new Error("lat and lon parameters should be valid numbers");
		}

		const factor = 0.0005;
		const bbox = [ lon - factor, lat - factor, lon + factor, lat + factor ].map(d => d.toFixed(4)).join(",");
		return `${this._endpoints.search}?bbox=[${bbox}]`;
	}

	/**
	 * Get full URL for retrieving a specific picture metadata
	 *
	 * @param {string} picId The picture unique identifier
	 * @param {string} [seqId] The sequence ID
	 * @returns {string} The corresponding URL
	 */
	getPictureMetadataUrl(picId, seqId) {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }

		if(API.isPictureIdValid(picId)) {
			if(seqId) { return `${this._endpoints.collections}/${seqId}/items/${picId}`; }
			else { return `${this._endpoints.search}?ids=${picId}`; }
		}
	}

	/**
	 * Get full URL for pictures vector tiles
	 *
	 * @returns {string} The URL
	 * @fires Error If URL can't be determined
	 */
	getPicturesTilesUrl() {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }

		// Explicitly defined URL
		if(this._endpoints.tiles) {
			return this._endpoints.tiles;
		}
		// Unknown endpoint, throw an error
		else {
			throw new Error("Pictures vector tiles URL is unknown");
		}
	}

	/**
	 * Get full URL for single-user pictures vector tiles
	 *
	 * @param {string} userId The ID of user
	 * @returns {string} The URL
	 * @fires Error If URL can't be determined
	 */
	getUserPicturesTilesUrl(userId) {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }

		// Explicitly defined URL
		if(this._endpoints.user_tiles) {
			return this._endpoints.user_tiles.replace("{userId}", userId);
		}
		// Unknown endpoint, throw an error
		else {
			throw new Error("User pictures vector tiles URL is unknown");
		}
	}

	/**
	 * Find the thumbnail URL for a given picture
	 *
	 * @param {object} picture The picture GeoJSON feature
	 * @returns {string} The thumbnail URL, or null if not found
	 * @private
	 */
	findThumbnailInPictureFeature(picture) {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }
		if(!picture || !picture.assets) { return null; }

		let visualFallback = null;
		for(let a of Object.values(picture.assets)) {
			if(a.roles.includes("thumbnail") && a.type == "image/jpeg" && API.isValidHttpUrl(a.href)) {
				return a.href;
			}
			else if(a.roles.includes("visual") && a.type == "image/jpeg" && API.isValidHttpUrl(a.href)) {
				visualFallback = a.href;
			}
		}
		return visualFallback;
	}

	/**
	 * Get a picture thumbnail URL for a given sequence
	 *
	 * @param {string} seqId The sequence ID
	 * @param {object} [seq] The sequence metadata (with links) if already loaded
	 * @returns {Promise} Promise resolving on the picture thumbnail URL, or null if not found
	 */
	getPictureThumbnailURLForSequence(seqId, seq) {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }

		// Look for a dedicated endpoint in API
		if(this._endpoints.collection_preview) {
			return Promise.resolve(this._endpoints.collection_preview.replace("{id}", seqId));
		}

		// Check if a preview link exists in sequence metadata
		if(seq && Array.isArray(seq.links) && seq.links.length > 0) {
			let preview = seq.links.find(l => l.rel === "preview" && l.type === "image/jpeg");
			if(preview && API.isValidHttpUrl(preview.href)) {
				return Promise.resolve(preview.href);
			}
		}

		// Otherwise, search for a single picture in collection
		const url = `${this._endpoints.search}?limit=1&collections=${seqId}`;

		return fetch(url, this._getFetchOptions())
			.then(res => res.json())
			.then(res => {
				if(!Array.isArray(res.features) || res.features.length == 0) {
					return null;
				}

				return this.findThumbnailInPictureFeature(res.features.pop());
			});
	}

	/**
	 * Get thumbnail URL for a specific picture
	 *
	 * @param {string} picId The picture unique identifier
	 * @param {string} [seqId] The sequence ID
	 * @returns {Promise} The corresponding URL on resolve, or undefined if no thumbnail could be found
	 */
	getPictureThumbnailURL(picId, seqId) {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }

		// Look for a dedicated endpoint in API
		if(this._endpoints.item_preview) {
			return Promise.resolve(this._endpoints.item_preview.replace("{id}", picId));
		}

		// Pic + sequence IDs defined -> use direct item metadata
		if(picId && seqId) {
			return fetch(`${this._endpoints.collections}/${seqId}/items/${picId}`, this._getFetchOptions())
				.then(res => res.json())
				.then(picture => {
					return picture ? this.findThumbnailInPictureFeature(picture) : null;
				});
		}

		// Picture ID only -> use search as fallback
		return fetch(`${this._endpoints.search}?ids=${picId}`, this._getFetchOptions())
			.then(res => res.json())
			.then(res => {
				if(!res || !Array.isArray(res.features) || res.features.length == 0) { return null; }
				return this.findThumbnailInPictureFeature(res.features.pop());
			});
	}

	/**
	 * Get the RSS feed URL with map parameters (if map is enabled)
	 * 
	 * @param {LngLatBounds} [bbox] The map current bounding box, or null if not available
	 * @returns {string} The URL, or null if no RSS feed is available
	 */
	getRSSURL(bbox) {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }
		
		if(this._endpoints.rss) {
			let url = this._endpoints.rss;
			if(bbox) {
				url += url.includes("?") ? "&": "?";
				url += "bbox=" + [bbox.getWest(), bbox.getSouth(), bbox.getEast(), bbox.getNorth()].join(",");
			}
			return url;
		}
		else {
			return null;
		}
	}

	/**
	 * Get full URL for retrieving a specific sequence metadata
	 *
	 * @param {string} seqId The sequence ID
	 * @returns {string} The corresponding URL
	 */
	getSequenceMetadataUrl(seqId) {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }
		return `${this._endpoints.collections}/${seqId}`;
	}

	/**
	 * List pictures in a sequence
	 * 
	 * @param {string} seqId The sequence ID
	 * @returns {Promise} Resolves on pictures metadata, or null if no result found
	 */
	getSequencePictures(seqId) {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }

		return fetch(`${this._endpoints.collections}/${seqId}/items`, this._getFetchOptions())
			.then(res => res.json())
			.then(res => {
				return res?.features || null;
			});
	}

	/**
	 * Get available data bounding box
	 *
	 * @returns {LngLatBoundsLike} The bounding box or null if not available
	 */
	getDataBbox() {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }

		return this._dataBbox;
	}

	/**
	 * Look for user ID based on user name query
	 * @param {string} query The user name to look for
	 * @returns {Promise} Resolves on list of potential users
	 */
	searchUsers(query) {
		if(!this.isReady()) { throw new Error("API is not ready to use"); }
		if(!this._endpoints.user_search) { throw new Error("User search is not available"); }

		return fetch(`${this._endpoints.user_search}?q=${query}`, this._getFetchOptions())
			.then(res => res.json())
			.then(res => {
				return res?.features || null;
			});
	}

	/**
	 * Checks URL string validity
	 *
	 * @param {string} str The URL to check
	 * @returns {boolean} True if valid
	 */
	static isValidHttpUrl(str) {
		let url;

		try {
			url = new URL(str);
		} catch (_) {
			return false;
		}

		return url.protocol === "http:" || url.protocol === "https:";
	}

	/**
	 * Checks picture ID validity
	 *
	 * @param {string} picId The picture unique identifier
	 * @returns {boolean} True if valid
	 * @throws {Error} If not valid
	 */
	static isPictureIdValid(picId) {
		if(!picId || typeof picId !== "string" || picId.length === 0) {
			throw new Error("picId should be a valid picture unique identifier");
		}
		return true;
	}
}

export default API;
