const MAP_FILTERS_JS2URL = {
	"minDate": "date_from",
	"maxDate": "date_to",
	"type": "pic_type",
	"camera": "camera",
	"theme": "theme",
};
const MAP_FILTERS_URL2JS = Object.fromEntries(Object.entries(MAP_FILTERS_JS2URL).map(v => [v[1], v[0]]));
const UPDATE_HASH_EVENTS = [
	"view-rotated", "picture-loaded", "focus-changed",
	"map-filters-changed", "transition-duration-changed",
	"map-background-changed", "users-changed"
];

/**
 * Updates the URL hash with various viewer information.
 * This doesn't handle the "map" parameter, which is managed by MapLibre GL JS
 *
 * Based on https://github.com/maplibre/maplibre-gl-js/blob/main/src/ui/hash.ts
 *
 * @returns {URLHash} `this`
 *
 * @private
 */
class URLHash extends EventTarget {
	constructor(viewer) {
		super();
		this._viewer = viewer;
		this._delay = null;
		this._hashChangeHandler = this._onHashChange.bind(this);
		window.addEventListener("hashchange", this._hashChangeHandler, false);
		UPDATE_HASH_EVENTS.forEach(e => this._viewer.addEventListener(e, this._updateHash.bind(this)));
	}

	/**
	 * Ends all form of life in this object.
	 */
	destroy() {
		window.removeEventListener("hashchange", this._hashChangeHandler);
		delete this._hashChangeHandler;
		this._viewer.map._map.off("moveend", this._updateHash);
		UPDATE_HASH_EVENTS.forEach(e => this._viewer.removeEventListener(e, this._updateHash));
		delete this._viewer;
	}

	/**
	 * Start listening to map movend event
	 */
	bindMapEvents() {
		this._viewer.map._map.on("moveend", this._updateHash.bind(this));
	}

	/**
	 * Get the hash string with current map/psv parameters
	 * @return {string} The hash, starting with #
	 */
	getHashString() {
		let hash = "";
		let hashParts = {};

		if(typeof this._viewer.getTransitionDuration() == "number") {
			hashParts.speed = this._viewer.getTransitionDuration();
		}

		const picMeta = this._viewer.getPictureMetadata();
		if (picMeta) {
			hashParts.pic = picMeta.id;
			hashParts.xyz = this._getXyzHashString();
		}

		if(this._viewer.map) {
			hashParts.map = this._getMapHashString();
			hashParts.focus = "pic";
			if(this._viewer.isMapWide()) { hashParts.focus = "map"; }
			if(!this._viewer.popupContainer.classList.contains("gvs-hidden")) { hashParts.focus = "meta"; }
			if(this._viewer.map._hasTwoBackgrounds && this._viewer.getMapBackground()) {
				hashParts.background = this._viewer.getMapBackground();
			}

			const vu = this._viewer.getVisibleUsers();
			if(vu.length > 1 || !vu.includes("geovisio")) {
				hashParts.users = vu.join(",");
			}

			if(this._viewer._mapFilters) {
				for(let k in MAP_FILTERS_JS2URL) {
					if(this._viewer._mapFilters[k]) {
						hashParts[MAP_FILTERS_JS2URL[k]] = this._viewer._mapFilters[k];
					}
				}
			}
		}

		Object.entries(hashParts)
			.sort((a,b) => a[0].localeCompare(b[0]))
			.forEach(entry => {
				let [ hashName, value ] = entry;
				let found = false;
				const parts = hash.split("&").map(part => {
					const key = part.split("=")[0];
					if (key === hashName) {
						found = true;
						return `${key}=${value}`;
					}
					return part;
				}).filter(a => a);
				if (!found) {
					parts.push(`${hashName}=${value}`);
				}
				hash = `${parts.join("&")}`;
			});

		return `#${hash}`.replace(/^#+/, "#");
	}

	/**
	 * Transforms window.location.hash into key->value object
	 * @return {object} Key-value read from hash
	 * @private
	 */
	_getCurrentHash() {
		// Get the current hash from location, stripped from its number sign
		const hash = window.location.hash.replace("#", "");

		// Split the parameter-styled hash into parts and find the value we need
		let keyvals = {};
		hash.split("&").map(
			part => part.split("=")
		)
			.filter(part => part[0] !== undefined && part[0].length > 0)
			.forEach(part => {
				keyvals[part[0]] = part[1];
			});

		return keyvals;
	}

	/**
	 * Get string representation of map position
	 * @returns {string} zoom/lat/lon or zoom/lat/lon/bearing/pitch
	 * @private
	 */
	_getMapHashString() {
		const center = this._viewer.map._map.getCenter(),
			zoom = Math.round(this._viewer.map._map.getZoom() * 100) / 100,
			// derived from equation: 512px * 2^z / 360 / 10^d < 0.5px
			precision = Math.ceil((zoom * Math.LN2 + Math.log(512 / 360 / 0.5)) / Math.LN10),
			m = Math.pow(10, precision),
			lng = Math.round(center.lng * m) / m,
			lat = Math.round(center.lat * m) / m,
			bearing = this._viewer.map._map.getBearing(),
			pitch = this._viewer.map._map.getPitch();
		let hash = `${zoom}/${lat}/${lng}`;

		if (bearing || pitch) hash += (`/${Math.round(bearing * 10) / 10}`);
		if (pitch) hash += (`/${Math.round(pitch)}`);

		return hash;
	}

	/**
	 * Get PSV view position as string
	 * @returns {string} x/y/z
	 * @private
	 */
	_getXyzHashString() {
		const xyz = this._viewer.getXYZ();
		const x = xyz.x.toFixed(2),
			y = xyz.y.toFixed(2),
			z = Math.round(xyz.z || 0);
		return `${x}/${y}/${z}`;
	}

	/**
	 * Updates map and PSV according to current hash values
	 * @private
	 */
	_onHashChange() {
		const vals = this._getCurrentHash();

		// Restore selected picture
		if(vals.pic) {
			this._viewer.goToPicture(vals.pic);
		}

		// Change focus
		if(vals.focus && ["map", "pic"].includes(vals.focus)) {
			this._viewer.setFocus(vals.focus);
		}
		if(vals.focus && vals.focus == "meta") {
			this._viewer._widgets._showPictureMetadataPopup();
		}

		// Change speed
		if(vals.speed) {
			this._viewer.setTransitionDuration(vals.speed);
		}

		// Change map position & users
		if(vals.map && this._viewer.map) {
			const mapOpts = this.getMapOptionsFromHashString(vals.map);
			if(mapOpts) {
				this._viewer.map._map.jumpTo(mapOpts);
			}

			let vu = (vals.users || "").split(",");
			if(vu.length === 0 || (vu.length === 1 && vu[0].trim() === "")) { vu = ["geovisio"]; }
			this._viewer.switchVisibleUsers(vu);
		}

		// Change xyz position
		if(vals.xyz) {
			const coords = this.getXyzOptionsFromHashString(vals.xyz);
			this._viewer.setXYZ(coords.x, coords.y, coords.z);
		}

		// Change map filters
		this._viewer.setFilters(this.getMapFiltersFromHashVals(vals));

		// Change map background
		this._viewer.switchMapBackground(vals.background);
	}

	/**
	 * Extracts from hash parsed keys all map filters values
	 * @param {*} vals Hash keys
	 * @returns {object} Map filters
	 */
	getMapFiltersFromHashVals(vals) {
		const newMapFilters = {};
		for(let k in MAP_FILTERS_URL2JS) {
			if(vals[k]) {
				newMapFilters[MAP_FILTERS_URL2JS[k]] = vals[k];
			}
		}
		return newMapFilters;
	}

	/**
	 * Extracts from string map position
	 * @param {string} str The map position as hash string
	 * @returns {object} { center, zoom, pitch, bearing }
	 */
	getMapOptionsFromHashString(str) {
		const loc = str.split("/");
		if (loc.length >= 3 && !loc.some(v => isNaN(v))) {
			const res = {
				center: [+loc[2], +loc[1]],
				zoom: +loc[0],
				pitch: +(loc[4] || 0)
			};

			if(this._viewer.map) {
				res.bearing = this._viewer.map._map.dragRotate.isEnabled() && this._viewer.map._map.touchZoomRotate.isEnabled() ? +(loc[3] || 0) : this._viewer.map._map.getBearing();
			}

			return res;
		}
		else { return null; }
	}

	/**
	 * Extracts from string xyz position
	 * @param {string} str The xyz position as hash string
	 * @returns {object} { x, y, z }
	 */
	getXyzOptionsFromHashString(str) {
		const loc = str.split("/");
		if (loc.length === 3 && !loc.some(v => isNaN(v))) {
			const res = {
				x: +loc[0],
				y: +loc[1],
				z: +loc[2]
			};

			return res;
		}
		else { return null; }
	}

	/**
	 * Changes the URL hash using current viewer parameters
	 * @private
	 */
	_updateHash() {
		if(this._delay) {
			clearTimeout(this._delay);
			this._delay = null;
		}

		this._delay = setTimeout(() => {
			// Replace if already present, else append the updated hash string
			const location = window.location.href.replace(/(#.+)?$/, this.getHashString());
			try {
				window.history.replaceState(window.history.state, null, location);
				const event = new CustomEvent("url-changed", { detail: {url: location}});
				this.dispatchEvent(event);
			} catch (SecurityError) {
				// IE11 does not allow this if the page is within an iframe created
				// with iframe.contentWindow.document.write(...).
				// https://github.com/mapbox/mapbox-gl-js/issues/7410
			}
		}, 500);
	}

}

export default URLHash;
