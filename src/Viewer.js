import "./css/Viewer.css";
import { Viewer as PSViewer, SYSTEM as PSSystem, DEFAULTS as PSDefaults } from "@photo-sphere-viewer/core";
import { EquirectangularTilesAdapter } from "@photo-sphere-viewer/equirectangular-tiles-adapter";
import { VirtualTourPlugin } from "@photo-sphere-viewer/virtual-tour-plugin";
import API from "./API";
import Widgets from "./Widgets";
import { Map, THEMES as MAP_THEMES, COLORS_HEX as MAP_COLORS, RASTER_LAYER_ID } from "./Map";
import { filterUserLayersContent, reloadVectorTiles, switchUserLayers } from "./MapUtils";
import URLHash from "./URLHash";
import LoaderImgBase from "./img/loader_base.jpg";
import LoaderImgTile0 from "./img/loader_0.jpg";
import LoaderImgTile1 from "./img/loader_1.jpg";
import LogoDead from "./img/logo_dead.svg";
import LoaderImg from "./img/logo_anime.gif";
import { getTranslations } from "./I18n";
import { getDistance, sortPicturesInDirection, getSimplifiedAngle } from "./Utils";
import PACKAGE_JSON from "../package.json";

const BASE_PANORAMA = {
	baseUrl: LoaderImgBase,
	width: 1280,
	cols: 2,
	rows: 1,
	tileUrl: (col) => (col === 0 ? LoaderImgTile0 : LoaderImgTile1)
};

const PSV_DEFAULT_ZOOM = 30;
const PSV_ZOOM_DELTA = 20;
const PSV_ANIM_DURATION = 250;
const PSV_MOVE_DELTA = Math.PI / 6;
const MAP_MOVE_DELTA = 100;
const PIC_MAX_STAY_DURATION = 3000;
const PIC_DEFAULT_STAY_DURATION = 250;
const JOSM_REMOTE_URL = "http://127.0.0.1:8111";


/**
 * Viewer is the main component of GeoVisio, showing pictures and map
 *
 * @fires picture-loaded
 * @fires picture-loading
 * @fires picture-preview-started
 * @fires picture-preview-stopped
 * @fires focus-changed
 * @fires view-rotated
 * @fires picture-tiles-loaded
 * @fires map-background-changed
 * @fires transition-duration-changed
 * @fires josm-live-enabled
 * @fires josm-live-disabled
 * @fires sequence-playing
 * @fires sequence-stopped
 * @fires map-filters-changed
 * @fires focus-changed
 * @fires users-changed
 * @fires ready
 * @param {string|Node} container The DOM element to create viewer into
 * @param {string} endpoint URL to API to use (must be a [STAC API](https://github.com/radiantearth/stac-api-spec/blob/main/overview.md))
 * @param {object} [options] Viewer options
 * @param {string} [options.picId] Initial picture identifier to display
 * @param {number[]} [options.position] Initial position to go to (in [lat, lon] format)
 * @param {boolean} [options.hash=true] Enable URL hash settings
 * @param {string} [options.lang] Override language to use (defaults to navigator language, or English if translation not available)
 * @param {int} [options.transition=250] Duration of stay on a picture during sequence play (excludes loading time)
 * @param {object} [options.fetchOptions=null] Set custom options for fetch calls made against API ([same syntax as fetch options parameter](https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters))
 * @param {boolean|object} [options.map=false] Enable contextual map for locating pictures. Setting to true or passing an object enables the map. Various settings can be passed, either the ones defined here, or any of [MapLibre GL settings](https://maplibre.org/maplibre-gl-js-docs/api/map/#map-parameters)
 * @param {string} [options.map.picturesTiles] URL for fetching pictures vector tiles if map is enabled (defaults to "xyz" link advertised in STAC API landing page)
 * @param {boolean} [options.map.startWide] Show the map as main element at startup (defaults to false, viewer is wider at start)
 * @param {number} [options.map.minZoom=0] The minimum zoom level of the map (0-24).
 * @param {number} [options.map.maxZoom=24] The maximum zoom level of the map (0-24).
 * @param {object|string} [options.map.style] The MapLibre style for streets background. This must be an a JSON object conforming to the schema described in the [MapLibre Style Specification](https://maplibre.org/maplibre-gl-js-docs/style-spec/), or a URL to such JSON.
 * @param {object} [options.map.raster] The MapLibre raster source for aerial background. This must be a JSON object following [MapLibre raster source definition](https://maplibre.org/maplibre-style-spec/sources/#raster).
 * @param {external:maplibre-gl.LngLatLike} [options.map.center=[0, 0]] The initial geographical centerpoint of the map. If `center` is not specified in the constructor options, MapLibre GL JS will look for it in the map's style object. If it is not specified in the style, either, it will default to `[0, 0]` Note: MapLibre GL uses longitude, latitude coordinate order (as opposed to latitude, longitude) to match GeoJSON.
 * @param {number} [options.map.zoom=0] The initial zoom level of the map. If `zoom` is not specified in the constructor options, MapLibre GL JS will look for it in the map's style object. If it is not specified in the style, either, it will default to `0`.
 * @param {external:maplibre-gl.LngLatBoundsLike} [options.map.bounds] The initial bounds of the map. If `bounds` is specified, it overrides `center` and `zoom` constructor options.
 * @param {object} [options.map.geocoder] Optional geocoder settings
 * @param {string} [options.map.theme=default] The colouring scheme to use for pictures and sequences on map (default, age, type)
 * @param {string[]} [options.map.users] The IDs of users whom data should appear on map (defaults to all). Only works with API having a "user-xyz" endpoint.
 * @param {object} [options.widgets] Settings related to viewer buttons and widgets
 * @param {string} [options.widgets.editIdUrl] URL to the OpenStreetMap iD editor (defaults to OSM.org iD instance)
 * @param {string|Element} [options.widgets.customWidget] A user-defined widget to add (will be shown over "Share" button)
 * @param {string} [options.widgets.mapAttribution] Override the default map attribution (read from MapLibre style)
 */
class Viewer extends EventTarget {
	constructor(container, endpoint, options = {}){
		super();

		if(options == null) { options = {}; }
		if(options.map == null) { options.map = {}; }
		if(options.widgets == null) { options.widgets = {}; }

		if(!options.testing) {
			// Display version in logs
			console.info(`
 ▄▄▄▄▄▄▄ ▄▄▄▄▄▄▄ ▄▄▄▄▄▄▄ ▄▄   ▄▄ ▄▄▄ ▄▄▄▄▄▄▄ ▄▄▄ ▄▄▄▄▄▄▄
█       █       █       █  █ █  █   █       █   █       █
█   ▄▄▄▄█    ▄▄▄█   ▄   █  █▄█  █   █  ▄▄▄▄▄█   █   ▄   █
█  █  ▄▄█   █▄▄▄█  █ █  █       █   █ █▄▄▄▄▄█   █  █ █  █
█  █ █  █    ▄▄▄█  █▄█  █       █   █▄▄▄▄▄  █   █  █▄█  █
█  █▄▄█ █   █▄▄▄█       ██     ██   █▄▄▄▄▄█ █   █       █
█▄▄▄▄▄▄▄█▄▄▄▄▄▄▄█▄▄▄▄▄▄▄█ █▄▄▄█ █▄▄▄█▄▄▄▄▄▄▄█▄▄▄█▄▄▄▄▄▄▄█

📷 GeoVisio - Version ${PACKAGE_JSON.version} (${__COMMIT_HASH__})

🆘 Issues can be reported at ${PACKAGE_JSON.repository.url}`);
		}

		// Set variables
		this._sequencePlaying = false;
		this._prevSequence = null;
		this._t = getTranslations(options.lang || navigator.language || navigator.userLanguage);
		this._picturesSequences = {}; // Pic ID -> ID of sequence it belongs to

		if(!options.transition) { options.transition = PIC_DEFAULT_STAY_DURATION; }
		this._transitionDuration = options.transition;

		// Skip all init phases for more in-depth testing
		if(options.testing) { return; }

		// Init all DOM and components
		this._initContainerStructure(container);
		this._initLoader();
		try { this._initPSV(options); }
		catch(e) {
			let err = !PSSystem.isWebGLSupported ? this._t.gvs.error_webgl : this._t.gvs.error_psv;
			this._dismissLoader(e, err);
		}

		endpoint = endpoint.replace("/api/search", "/api");
		this._api = new API(endpoint, { tiles: options?.map?.picturesTiles, fetch: options?.fetchOptions });

		// Read initial options from URL hash
		let hashOpts;
		if(options.hash === true || options.hash === undefined) {
			this._hash = new URLHash(this);
			hashOpts = this._hash._getCurrentHash();
			if(typeof options.map === "object") { options.map.hash = false; }

			// Restore picture from URL hash
			if(hashOpts.pic) {
				options.picId = this._hash._getCurrentHash().pic;
			}

			// Restore focus
			if(options.map && hashOpts.focus) {
				options.map.startWide = hashOpts.focus === "map";
			}

			// Restore map background
			if(options.map && hashOpts.background) {
				options.map.background = hashOpts.background;
			}

			// Restore viewer position
			if(hashOpts.xyz) {
				this._myVTour.addEventListener("node-changed", () => {
					const coords = this._hash.getXyzOptionsFromHashString(hashOpts.xyz);
					this.setXYZ(coords.x, coords.y, coords.z);
				}, { once: true });
			}

			// Restore map zoom/center
			if(options.map && typeof hashOpts.map === "string") {
				const mapOpts = this._hash.getMapOptionsFromHashString(hashOpts.map);
				if(mapOpts) {
					options.map = Object.assign({}, options.map, mapOpts);
				}
			}

			// Restore map filters
			if(options.map) {
				this.setFilters(this._hash.getMapFiltersFromHashVals(hashOpts), true);
			}

			// Restore play speed
			if(typeof hashOpts.speed === "string") {
				this._transitionDuration = parseInt(hashOpts.speed);
			}
		}

		// Call appropriate functions at start according to initial options
		const onceStuffReady = () => {
			this._widgets = new Widgets(this, options.widgets);
			
			if(options.picId) {
				this.goToPicture(options.picId).catch(e => this._dismissLoader(e, this._t.gvs.error_pic));
				this.addEventListener("picture-loaded", () => {
					if(this.map && options.map) {
						this.map._map.jumpTo(options.map);
					}
					if(hashOpts?.focus === "meta") {
						this._widgets._showPictureMetadataPopup();
					}
					this._dismissLoader();
				}, { once: true });
			}
			else {
				this._dismissLoader();
			}

			if(options.position) {
				this.goToPosition(...options.position).catch(e => this._dismissLoader(e, this._t.gvs.error_nopic));
			}

			if(this._hash && this.map) {
				this._hash.bindMapEvents();
				if(this._mapFilters) {
					this.setFilters(this._mapFilters, true);
				}
			}
		};

		this._api.onceReady()
			.then(() => {
				if(options.map) {
					options.map.transformRequest = this._api._getMapRequestTransform();
					
					this._initMap(options.map)
						.then(onceStuffReady)
						.catch(e => this._dismissLoader(e, this._t.gvs.error_api_compatibility));
				}
				else {
					onceStuffReady();
				}
			})
			.catch(e => this._dismissLoader(e, this._t.gvs.error_api));
	}

	/**
	 * Ends all form of life in this object.
	 * 
	 * This is useful for Single Page Applications (SPA), to remove various event listeners.
	 */
	destroy() {
		// Delete sub-components
		delete this._t;
		delete this._api;
		this._hash.destroy();
		delete this._hash;
		delete this._myVTour;
		this._widgets.destroy();
		delete this._widgets;
		this.map.destroy();
		delete this.map;
		delete this._mapFilters;
		delete this.psv._geovisio;
		this.psv.destroy();
		delete this.psv;

		// Clean-up DOM
		this.miniContainer.remove();
		this.mainContainer.remove();
		this.mapContainer.remove();
		this.psvContainer.remove();
		this.popupContainer.remove();
		this.container.innerHTML = "";
		this.container.classList.remove(...[...this.container.classList].filter(c => c.startsWith("gvs")));
	}

	/**
	 * Creates appropriate HTML elements in container to host map + viewer
	 *
	 * @private
	 * @param {string|Element} container The container, either as HTML Element or ID of an in-document element
	 */
	_initContainerStructure(container) {
		this.container = typeof container === "string" ? document.getElementById(container) : container;

		if(this.container instanceof Element) {
			// Add classes to container
			this.container.classList.add("gvs");

			// Create mini-component container
			this.miniContainer = document.createElement("div");
			this.miniContainer.classList.add("gvs-mini");

			// Create main-component container
			this.mainContainer = document.createElement("div");
			this.mainContainer.classList.add("gvs-main");

			// Create a loaders container
			this.loaderContainer = document.createElement("div");
			this.loaderContainer.classList.add("gvs-loader", "gvs-loader-visible");

			// Crate a popup container
			this.popupContainer = document.createElement("div");
			this.popupContainer.classList.add("gvs-popup", "gvs-hidden");

			// Create PSV container
			this.psvContainer = document.createElement("div");
			this.psvContainer.classList.add("gvs-psv");
			this.mainContainer.appendChild(this.psvContainer);

			// Create map container
			this.mapContainer = document.createElement("div");
			this.mapContainer.classList.add("gvs-map");
			this.miniContainer.appendChild(this.mapContainer);

			// Add in root container
			this.container.appendChild(this.mainContainer);
			this.container.appendChild(this.miniContainer);
			this.container.appendChild(this.popupContainer);
			this.container.appendChild(this.loaderContainer);
		}
		else {
			throw new Error("Container is not a valid HTML element, does it exist in your page ?");
		}
	}

	/**
	 * Inits Photo Sphere Viewer component
	 *
	 * @private
	 * @param {object} options Parameters to send to photo sphere viewer
	 */
	_initPSV(options = {}) {
		this.psv = new PSViewer({
			container: this.psvContainer,
			adapter: [EquirectangularTilesAdapter, {
				showErrorTile: false,
				baseBlur: false,
				resolution: this.isSmall() ? 32 : 64,
			}],
			withCredentials: options?.fetchOptions?.credentials == "include",
			requestHeaders: options.fetchOptions?.headers,
			panorama: BASE_PANORAMA,
			lang: this._t.psv,
			loadingImg: LoaderImg,
			minFov: 5,
			navbar:	null,
			plugins: [
				[VirtualTourPlugin, {
					dataMode: "server",
					positionMode: "gps",
					renderMode: "3d",
					arrowPosition: "bottom",
					preload: true,
					getNode: this._getNodeFromAPI.bind(this),
					transitionOptions: this._psvNodeTransition.bind(this)
				}],
			],
			keyboard: "always",
			keyboardActions: {
				...PSDefaults.keyboardActions,
				"8": "ROTATE_UP",
				"2": "ROTATE_DOWN",
				"4": "ROTATE_LEFT",
				"6": "ROTATE_RIGHT",

				"PageUp": () => this.goToNextPicture(),
				"9": () => this.goToNextPicture(),

				"PageDown": () => this.goToPrevPicture(),
				"3": () => this.goToPrevPicture(),

				"5": () => this.moveCenter(),
				"*": () => this.moveCenter(),

				"Home": () => this.toggleFocus(),
				"7": () => this.toggleFocus(),

				"End": () => this.toggleUnfocusedVisible(),
				"1": () => this.toggleUnfocusedVisible(),

				" ": () => this.toggleSequencePlaying(),
				"0": () => this.toggleSequencePlaying(),
			},
		});

		this.psv._geovisio = this;
		this._myVTour = this.psv.getPlugin(VirtualTourPlugin);

		// Offer custom events for hover on PSV arrow
		this._myVTour.addEventListener("enter-arrow", e => {
			const fromLink = e.link;
			const fromNode = e.node;

			// Find probable direction for previewed picture
			let direction;
			const fromLinkPos = fromNode ? this._myVTour.__getLinkPosition(fromNode, fromLink) : null;
			if(fromNode) {
				if(fromNode.horizontalFov === 360) {
					if(fromLinkPos) {
						direction = fromLinkPos.yaw * (180/Math.PI);
					}
				}
				else {
					direction = this.psv.getPosition().yaw * (180/Math.PI);
				}
			}
			
			/**
			 * Event for picture preview
			 *
			 * @event picture-preview-started
			 * @type {object}
			 * @property {string} picId The picture ID
			 * @property {number[]} coordinates [x,y] coordinates
			 * @property {number} direction The theorical picture orientation
			 */
			const event = new CustomEvent("picture-preview-started", { detail: {
				picId: fromLink.nodeId,
				coordinates: fromLink.gps,
				direction,
			}});
			this.dispatchEvent(event);
		});

		this._myVTour.addEventListener("leave-arrow", e => {
			const fromLink = e.link;
			
			/**
			 * Event for end of picture preview
			 *
			 * @event picture-preview-stopped
			 * @type {object}
			 * @property {string} picId The picture ID
			 */
			const event = new CustomEvent("picture-preview-stopped", { detail: {
				picId: fromLink.nodeId,
			}});
			this.dispatchEvent(event);
		});

		// Fix for loader circle background not showing up
		this.psv.loader.size = 150;
		this.psv.loader.color = "rgba(61, 61, 61, 0.5)";
		this.psv.loader.textColor = "rgba(255, 255, 255, 0.7)";
		this.psv.loader.border = 5;
		this.psv.loader.thickness = 10;
		this.psv.loader.canvas.setAttribute("viewBox", "0 0 150 150");
		this.psv.loader.__updateContent();

		
		// Custom events handlers
		this.psv.addEventListener("position-updated", ({position}) => {
			/**
			 * Event for viewer rotation
			 *
			 * @event view-rotated
			 * @type {object}
			 * @property {number} x New x position (in degrees, 0-360), corresponds to heading (0° = North, 90° = East, 180° = South, 270° = West)
			 * @property {number} y New y position (in degrees)
			 * @property {number} z New Z position (between 0 and 100)
			 */
			const event = new CustomEvent("view-rotated", { detail: this._positionToXYZ(position, this.psv.getZoomLevel()) });
			this.dispatchEvent(event);

			this._onTilesStartLoading();
		});

		this.psv.addEventListener("zoom-updated", ({zoomLevel}) => {
			const event = new CustomEvent("view-rotated", { detail: this._positionToXYZ(this.psv.getPosition(), zoomLevel) });
			this.dispatchEvent(event);

			this._onTilesStartLoading();
		});

		this._myVTour.addEventListener("node-changed", (e) => {
			if(e.node.id) {
				const picMeta = this.getPictureMetadata();
				this._prevSequence = picMeta.sequence.id;

				/**
				 * Event for picture load (low-resolution image is loaded)
				 *
				 * @event picture-loaded
				 * @type {object}
				 * @property {string} picId The picture unique identifier
				 * @property {number} lon Longitude (WGS84)
				 * @property {number} lat Latitude (WGS84)
				 * @property {number} x New x position (in degrees, 0-360), corresponds to heading (0° = North, 90° = East, 180° = South, 270° = West)
				 * @property {number} y New y position (in degrees)
				 * @property {number} z New z position (0-100)
				 */
				const event = new CustomEvent("picture-loaded", {
					detail: {
						...this._positionToXYZ(this.psv.getPosition(), this.psv.getZoomLevel()),
						picId: e.node.id,
						lon: picMeta.gps[0],
						lat: picMeta.gps[1]
					}
				});
				this.dispatchEvent(event);

				// Change download URL
				if(picMeta.panorama.hdUrl) {
					this.psv.setOption("downloadUrl", picMeta.panorama.hdUrl);
					this.psv.setOption("downloadName", e.node.id+".jpg");
				}
				else {
					this.psv.setOption("downloadUrl", null);
				}
			}

			this._onTilesStartLoading();
		});
	}

	/**
	 * Inits MapLibre GL component
	 *
	 * @private
	 * @param {object} mapOptions Parameters to send to MapLibre
	 * @returns {Promise} Resolves when map is ready
	 */
	_initMap(mapOptions = {}) {
		return new Promise(resolve => {
			this.map = new Map(this, mapOptions, this._t.map);
			this.map._map.once("load", resolve);
			this.container.classList.add("gvs-has-mini");

			if(typeof mapOptions === "object" && mapOptions.startWide) {
				this.setFocus("map", true);
			}
			else {
				this.setFocus("pic", true);
			}
		});
	}

	/**
	 * Inits overlay loader component
	 *
	 * @private
	 */
	_initLoader() {
		// Logo
		const logo = document.createElement("img");
		logo.src = LoaderImg;
		logo.title = this._t.map.loading;
		this.loaderContainer.appendChild(logo);

		// Label
		const labelWrapper = document.createElement("div");
		const label = document.createElement("span");
		const nextLabel = () => (
			this._t.gvs.loading_labels[
				Math.floor(Math.random() * this._t.gvs.loading_labels.length)
			]
		);
		label.innerHTML = nextLabel();
		this._loaderLabelChanger = null;
		const nextLabelFct = () => setTimeout(() => {
			label.innerHTML = nextLabel();
			this._loaderLabelChanger = nextLabelFct();
		}, 500 + Math.random() * 2000);
		this._loaderLabelChanger = nextLabelFct();
		labelWrapper.appendChild(label);

		// Blinking dots
		for(let i=0; i < 3; i++) {
			const dot = document.createElement("span");
			dot.classList.add("gvs-loader-dot");
			dot.appendChild(document.createTextNode("."));
			labelWrapper.appendChild(dot);
		}

		this.loaderContainer.appendChild(labelWrapper);
	}

	/**
	 * Dismiss loader, or show error
	 * @param {object} [err] Optional error object to show in browser console
	 * @param {str} [errMeaningful] Optional error message to show to user
	 * @private
	 */
	_dismissLoader(err = null, errMeaningful = null) {
		clearTimeout(this._loaderLabelChanger);

		if(!err) {
			this.loaderContainer.classList.remove("gvs-loader-visible");
			setTimeout(() => this.loaderContainer.remove(), 2000);

			/**
			 * Event for viewer being ready to use (API loaded)
			 *
			 * @event ready
			 * @type {object}
			 */
			const readyEvt = new CustomEvent("ready");
			this.dispatchEvent(readyEvt);
		}
		else {
			if(err !== true) { console.error(err); }

			// Change content
			this.loaderContainer.children[0].src = LogoDead;
			this.loaderContainer.children[0].style.width = "200px";
			let errHtml = errMeaningful;
			if(errHtml) { errHtml += "<br /><small>" + this._t.gvs.error_subtitle + "</small>"; }
			else { errHtml = "<small>" + this._t.gvs.error_subtitle + "</small>"; }
			this.loaderContainer.children[1].innerHTML = `${this._t.gvs.error}<br />${errHtml}`;

			// Throw error
			throw new Error(errMeaningful || "GeoVisio had a blocking exception");
		}
	}

	/**
	 * Change full-page popup visibility and content
	 * @param {boolean} visible True to make it appear
	 * @param {string|Element[]} [content] The new popup content
	 */
	setPopup(visible, content = null) {
		if(!visible) {
			this.popupContainer.classList.add("gvs-hidden");
			this.setFocus("pic");
		}
		else if(content) {
			this.popupContainer.innerHTML = "";
			const backdrop = document.createElement("div");
			backdrop.classList.add("gvs-popup-backdrop");
			backdrop.addEventListener("click", () => this.setPopup(false));
			const innerDiv = document.createElement("div");
			innerDiv.classList.add("gvs-widget-bg");

			if(typeof content === "string") { innerDiv.innerHTML = content; }
			else if(Array.isArray(content)) { content.forEach(c => innerDiv.appendChild(c)); }

			this.popupContainer.appendChild(backdrop);
			this.popupContainer.appendChild(innerDiv);
			this.popupContainer.classList.remove("gvs-hidden");
		}
		else {
			this.popupContainer.classList.remove("gvs-hidden");
		}
	}

	/**
	 * Calls API to retrieve a certain picture, then transforms into PSV format
	 *
	 * @private
	 * @param {string} picId The picture UUID
	 * @returns {Promise} Resolves on PSV node metadata
	 */
	_getNodeFromAPI(picId) {
		if(!picId) { return; }

		return fetch(this._api.getPictureMetadataUrl(picId, this._picturesSequences[picId]), this._api._getFetchOptions())
			.then(res => res.json())
			.then(metadata => {
				if(metadata.features) { metadata = metadata.features.pop(); }
				if(!metadata) { throw new Error("Picture with ID "+picId+" was not found"); }

				this._picturesSequences[picId] = metadata.collection;

				const isHorizontalFovDefined = metadata.properties["pers:interior_orientation"] && metadata.properties["pers:interior_orientation"]["field_of_view"] != null;
				let horizontalFov = isHorizontalFovDefined ? parseInt(metadata.properties["pers:interior_orientation"]["field_of_view"]) : 70;
				const is360 = horizontalFov === 360;
				const hdUrl = (Object.values(metadata.assets).find(a => a.roles?.includes("data")) || {}).href;
				const matrix = metadata.properties["tiles:tile_matrix_sets"] ? metadata.properties["tiles:tile_matrix_sets"].geovisio : null;
				const prev = metadata.links.find(l => l?.rel === "prev" && l?.type === "application/geo+json");
				const next = metadata.links.find(l => l?.rel === "next" && l?.type === "application/geo+json");
				const baseUrlWebp = Object.values(metadata.assets).find(a => a.roles?.includes("visual") && a.type === "image/webp");
				const baseUrlJpeg = Object.values(metadata.assets).find(a => a.roles?.includes("visual") && a.type === "image/jpeg");
				const tileUrl = metadata?.asset_templates?.tiles_webp || metadata?.asset_templates?.tiles;

				if(prev) { this._picturesSequences[prev.id] = metadata.collection; }
				if(next) { this._picturesSequences[next.id] = metadata.collection; }

				const res = {
					id: metadata.id,
					caption: this._getNodeCaption(metadata),
					panorama: is360 ? {
						baseUrl: (baseUrlWebp || baseUrlJpeg).href,
						hdUrl,
						cols: matrix && matrix.tileMatrix[0].matrixWidth,
						rows: matrix && matrix.tileMatrix[0].matrixHeight,
						width: matrix && (matrix.tileMatrix[0].matrixWidth * matrix.tileMatrix[0].tileWidth),
						tileUrl: matrix && ((col, row) => tileUrl.href.replace(/\{TileCol\}/g, col).replace(/\{TileRow\}/g, row))
					} : {
						// Flat pictures are shown only using a cropped base panorama
						baseUrl: hdUrl,
						hdUrl,
						basePanoData: (img) => {
							if(img.width < img.height && !isHorizontalFovDefined) {
								horizontalFov = 35;
							}
							const verticalFov = horizontalFov * img.height / img.width;
							const panoWidth = img.width * 360 / horizontalFov;
							const panoHeight = img.height * 180 / verticalFov;

							return {
								fullWidth: panoWidth,
								fullHeight: panoHeight,
								croppedWidth: img.width,
								croppedHeight: img.height,
								croppedX: (panoWidth - img.width) / 2,
								croppedY: (panoHeight - img.height) / 2,
								poseHeading: metadata.properties["view:azimuth"] || 0
							};
						},
						// This is only to mock loading of tiles (which are not available for flat pictures)
						cols: 2, rows: 1, width: 2, tileUrl: () => null
					},
					links: this._filterRelatedPicsLinks(metadata),
					gps: metadata.geometry.coordinates,
					sequence: {
						id: metadata.collection,
						nextPic: next ? next.id : undefined,
						prevPic: prev ? prev.id : undefined
					},
					sphereCorrection: metadata.properties["view:azimuth"] ? {
						pan: - metadata.properties["view:azimuth"] * (Math.PI / 180)
					} : { pan: 0 },
					horizontalFov,
					properties: metadata.properties,
				};

				return res;
			});
	}

	/**
	 * Generates the navbar caption based on a single picture metadata
	 *
	 * @private
	 * @param {object} metadata The picture metadata
	 * @returns {object} Normalized object with user name, licence and date
	 */
	_getNodeCaption(metadata) {
		const caption = {};

		// Timestamp
		if(metadata?.properties?.datetime) {
			caption.date = new Date(metadata.properties.datetime);
		}

		// Producer
		if(metadata?.providers) {
			const producerRoles = metadata?.providers?.filter(el => el?.roles?.includes("producer"));
			if(producerRoles?.length >= 0) {
				caption.producer = producerRoles.map(p => p.name).join(", ");
			}
		}

		// License
		if(metadata?.properties?.license) {
			caption.license = metadata.properties.license;
			// Look for URL to license
			if(metadata?.links) {
				const licenseLink = metadata.links.find(l => l?.rel === "license");
				if(licenseLink) {
					caption.license = `<a href="${licenseLink.href}" title="${this._t.gvs.metadata_general_license_link}" target="_blank">${caption.license}</a>`;
				}
			}
		}

		return caption;
	}

	/**
	 * PSV node transition handler
	 * @param {*} toNode Next loading node
	 * @param {*} [fromNode] Currently shown node (previous)
	 * @param {*} [fromLink] Link clicked by user to go from current to next node
	 * @private
	 */
	_psvNodeTransition(toNode, fromNode, fromLink) {
		let nodeTransition = {};

		const animationDuration = Math.min(PSV_ANIM_DURATION, this._transitionDuration);
		const animated = animationDuration > 0;
		const following = (fromLink || fromNode?.links.find(a => a.nodeId == toNode.id)) != null;

		this.psv.setOption("maxFov", Math.min(toNode.horizontalFov * 3/4, 90));

		const centerNoAnim = {
			speed: 0,
			fadeIn: false,
			rotation: false,
			rotateTo: { pitch: 0, yaw: - toNode.sphereCorrection.pan },
			zoomTo: PSV_DEFAULT_ZOOM
		};

		// Going to 360
		if(toNode.horizontalFov == 360) {
			// No previous sequence -> Point to center + no animation
			if(!fromNode) {
				nodeTransition = centerNoAnim;
			}
			// Has a previous sequence
			else {
				// Far away sequences -> Point to center + no animation
				if(getDistance(fromNode.gps, toNode.gps) >= 0.001) {
					nodeTransition = centerNoAnim;
				}
				// Nearby sequences
				else {
					// Compute relative orientation of picture
					const psvPrevPos = this.psv.getPosition();

					// Accessed by viewer arrow click -> look in arrow direction + animation if not vomiting
					// Accessed by map click -> keep relative orientation + animation if not vomiting
					let psvNextPos = fromLink ? this._myVTour.__getLinkPosition(fromNode, fromLink) : null;
					if(psvNextPos === null) {
						psvNextPos = Object.assign({}, psvPrevPos);
						if(fromNode.sphereCorrection?.pan) { psvNextPos.yaw += fromNode.sphereCorrection.pan; }
						if(toNode.sphereCorrection?.pan) { psvNextPos.yaw -= toNode.sphereCorrection.pan; }
					}
					while(psvNextPos.yaw < 0) { psvNextPos.yaw += 2 * Math.PI; }
					const notTooMuchRotation = Math.abs(psvPrevPos.yaw - psvNextPos.yaw) <= Math.PI / 4;

					// Check if viewer is looking in capture direction
					let wayOrientation = this._myVTour.__getLinkPosition(fromNode, toNode)?.yaw;
					while(wayOrientation < 0) { wayOrientation += 2 * Math.PI; }
					const diffOrientation = Math.abs(wayOrientation - psvNextPos.yaw) % Math.PI;
					const lookingForward = diffOrientation <= Math.PI / 4 || diffOrientation >= 3 * Math.PI / 4;

					nodeTransition = {
						speed: animationDuration,
						fadeIn: following && notTooMuchRotation && animated,
						rotation: following && notTooMuchRotation && animated && lookingForward,
						rotateTo: psvNextPos
					};
				}
			}
		}
		// Going to flat
		else {
			// Same sequence -> Point to center + animation if following pics + not vomiting
			if(fromNode && toNode.sequence.id === fromNode.sequence.id) {
				const notTooMuchRotation = Math.abs(fromNode.sphereCorrection?.pan - toNode.sphereCorrection?.pan) <= Math.PI / 4;
				nodeTransition = {
					speed: animationDuration,
					fadeIn: following && notTooMuchRotation && animated,
					rotation: following && notTooMuchRotation && animated,
					rotateTo: { pitch: 0, yaw: - toNode.sphereCorrection.pan },
					zoomTo: PSV_DEFAULT_ZOOM
				};
			}
			// Different sequence -> Point to center + no animation
			else {
				nodeTransition = centerNoAnim;
			}
		}


		/**
		 * Event for picture starting to load
		 *
		 * @event picture-loading
		 * @type {object}
		 * @property {string} picId The picture unique identifier
		 * @property {number} lon Longitude (WGS84)
		 * @property {number} lat Latitude (WGS84)
		 * @property {number} x New x position (in degrees, 0-360), corresponds to heading (0° = North, 90° = East, 180° = South, 270° = West)
		 * @property {number} y New y position (in degrees)
		 * @property {number} z New z position (0-100)
		 */
		const event = new CustomEvent("picture-loading", {
			detail: {
				...this._positionToXYZ(
					Object.assign({}, this.psv.getPosition(), nodeTransition.rotateTo),
					nodeTransition.zoomTo || this.psv.getZoomLevel()
				),
				picId: toNode.id,
				lon: toNode.gps[0],
				lat: toNode.gps[1]
			}
		});

		this.dispatchEvent(event);
		return nodeTransition;
	}

	/**
	 * Converts result from getPosition or position-updated event into x/y/z coordinates
	 *
	 * @private
	 * @param {object} pos pitch/yaw as given by PSV
	 * @param {number} zoom zoom as given by PSV
	 * @returns {object} Coordinates as x/y in degrees and zoom as given by PSV
	 */
	_positionToXYZ(pos, zoom = undefined) {
		const res = {
			x: pos.yaw * (180/Math.PI),
			y: pos.pitch * (180/Math.PI)
		};

		if(zoom) { res.z = zoom; }
		return res;
	}

	/**
	 * Converts x/y/z coordinates into PSV position (lat/lon/zoom)
	 *
	 * @private
	 * @param {number} x The X coordinate (in degrees)
	 * @param {number} y The Y coordinate (in degrees)
	 * @param {number} z The zoom level (0-100)
	 * @returns {object} Position coordinates as yaw/pitch/zoom
	 */
	_xyzToPosition(x, y, z) {
		return {
			yaw: x / (180/Math.PI),
			pitch: y / (180/Math.PI),
			zoom: z
		};
	}

	/**
	 * Filter surrounding pictures links to avoid too much arrows on viewer.
	 * 
	 * @private
	 */
	_filterRelatedPicsLinks(metadata) {
		const picLinks = metadata.links
			.filter(l => ["next", "prev", "related"].includes(l?.rel) && l?.type === "application/geo+json")
			.map(l => {
				if(l.datetime) {
					l.date = l.datetime.split("T")[0];
				}
				return l;
			});
		const picPos = metadata.geometry.coordinates;

		// Filter to keep a single link per direction, in same sequence or most recent one
		const filteredLinks = [];
		const picSurroundings = { "N": [], "ENE": [], "ESE": [], "S": [], "WSW": [], "WNW": [] };

		for(let picLink of picLinks) {
			const a = getSimplifiedAngle(picPos, picLink.geometry.coordinates);
			picSurroundings[a].push(picLink);
		}

		for(let direction in picSurroundings) {
			const picsInDirection = picSurroundings[direction];
			if(picsInDirection.length == 0) { continue; }
			picsInDirection.sort(sortPicturesInDirection(picPos));
			filteredLinks.push(picsInDirection.shift());
		}

		return filteredLinks.map(l => ({
			nodeId: l.id,
			gps: l.geometry.coordinates,
			arrowStyle: {
				color: l.rel === "related" ? MAP_COLORS.BASE : MAP_COLORS.SELECTED,
				hoverColor: 0xaaaaaa,
				size: 1.2,
			},
		}));
	}

	/**
	 * Event handler for loading a new range of tiles
	 *
	 * @private
	 */
	_onTilesStartLoading() {
		if(this._tilesQueueTimer) {
			clearInterval(this._tilesQueueTimer);
			delete this._tilesQueueTimer;
		}
		this._tilesQueueTimer = setInterval(() => {
			if(Object.keys(this.psv.adapter.queue.tasks).length === 0) {
				if(this._myVTour.state.currentNode) {
					/**
					 * Event launched when all visible tiles of a picture are loaded
					 *
					 * @event picture-tiles-loaded
					 * @type {object}
					 * @property {string} picId The picture unique identifier
					 */
					const event = new Event("picture-tiles-loaded", { picId: this._myVTour.state.currentNode.id });
					this.dispatchEvent(event);
				}
				clearInterval(this._tilesQueueTimer);
				delete this._tilesQueueTimer;
			}
		}, 100);
	}

	/**
	 * Click handler for next/prev navbar buttons
	 *
	 * @param {('next'|'prev')} type Set if it's next or previous button
	 * @private
	 */
	_onNextPrevPicClick(type) {
		if(this.getPictureMetadata() && this.getPictureMetadata().sequence[type+"Pic"]) {
			// Actually change current picture
			if(type === "prev") {
				this.goToPrevPicture();
			}
			else if(type === "next") {
				this.goToNextPicture();
			}
		}
	}

	/**
	 * Get 2D position of sphere currently shown to user
	 *
	 * @returns {object} Position in format { x: heading in degrees (0° = North, 90° = East, 180° = South, 270° = West), y: top/bottom position in degrees (-90° = bottom, 0° = front, 90° = top) }
	 */
	getXY() {
		return this._positionToXYZ(this.psv.getPosition());
	}

	/**
	 * Get 3D position of sphere currently shown to user
	 *
	 * @returns {object} Position in format { x: heading in degrees (0° = North, 90° = East, 180° = South, 270° = West), y: top/bottom position in degrees (-90° = bottom, 0° = front, 90° = top), z: zoom (0 = wide, 100 = zoomed in) }
	 */
	getXYZ() {
		return this._positionToXYZ(this.psv.getPosition(), this.psv.getZoomLevel());
	}

	/**
	 * Access currently shown picture metadata
	 *
	 * @returns {object} Picture metadata
	 */
	getPictureMetadata() {
		return this._myVTour.state.currentNode ? Object.assign({}, this._myVTour.state.currentNode) : null;
	}

	/**
	 * Access the map object (if any)
	 * Allows you to call any of [the MapLibre GL Map functions](https://maplibre.org/maplibre-gl-js-docs/api/map/) for advanced map management
	 *
	 * @returns {null|external:maplibre-gl.Map} The map
	 */
	getMap() {
		return this.map ? this.map._map : null;
	}

	/**
	 * Access the photo viewer object
	 * Allows you to call any of the [Photo Sphere Viewer functions](https://photo-sphere-viewer.js.org/api/classes/core.viewer)
	 *
	 * @returns {external:@photo-sphere-viewer/core.Viewer} The photo viewer
	 */
	getPhotoViewer() {
		return this.psv;
	}

	/**
	 * Displays in viewer specified picture
	 *
	 * @param {string} picId The picture unique identifier
	 * @param {string} [seqId] The sequence ID this picture belongs to. Optional, it allows faster retrieval from API
	 * @returns {Promise} Resolves on picture loaded
	 */
	goToPicture(picId, seqId) {
		if(seqId) {
			this._picturesSequences[picId] = seqId;
		}

		if(this._myVTour.getCurrentNode()?.id !== picId) {
			this.psv.loader.show();
		}
		return this._myVTour.setCurrentNode(picId);
	}

	/**
	 * Goes continuously to next picture in sequence as long as possible
	 */
	playSequence() {
		this._sequencePlaying = true;

		/**
		 * Event for sequence starting to play
		 *
		 * @event sequence-playing
		 * @type {object}
		 */
		const event = new Event("sequence-playing");
		this.dispatchEvent(event);

		const nextPicturePlay = () => {
			if(this._sequencePlaying) {
				this.addEventListener("picture-loaded", () => {
					this._playTimer = setTimeout(() => {
						nextPicturePlay();
					}, this._transitionDuration);
				}, { once: true });

				try {
					this.goToNextPicture();
				}
				catch(e) {
					this.stopSequence();
				}
			}
		};

		// Stop playing if user clicks on image
		this.psv.addEventListener("click", () => {
			this.stopSequence();
		});

		nextPicturePlay();
	}

	/**
	 * Stops playing current sequence
	 */
	stopSequence() {
		this._sequencePlaying = false;

		// Next picture timer is pending
		if(this._playTimer) {
			clearTimeout(this._playTimer);
			delete this._playTimer;
		}

		/**
		 * Event for sequence stopped playing
		 *
		 * @event sequence-stopped
		 * @type {object}
		 */
		const event = new Event("sequence-stopped");
		this.dispatchEvent(event);
	}

	/**
	 * Is there any sequence being played right now ?
	 *
	 * @returns {boolean} True if sequence is playing
	 */
	isSequencePlaying() {
		return this._sequencePlaying;
	}

	/**
	 * Starts/stops the reading of pictures in a sequence
	 */
	toggleSequencePlaying() {
		if(this.isSequencePlaying()) {
			this.stopSequence();
		}
		else {
			this.playSequence();
		}
	}

	/**
	 * Displays next picture in current sequence (if any)
	 */
	goToNextPicture() {
		if(!this.getPictureMetadata()) {
			throw new Error("No picture currently selected");
		}

		const next = this.getPictureMetadata().sequence.nextPic;
		if(next) {
			this.goToPicture(next);
		}
		else {
			throw new Error("No next picture available");
		}
	}

	/**
	 * Displays previous picture in current sequence (if any)
	 */
	goToPrevPicture() {
		if(!this.getPictureMetadata()) {
			throw new Error("No picture currently selected");
		}

		const prev = this.getPictureMetadata().sequence.prevPic;
		if(prev) {
			this.goToPicture(prev);
		}
		else {
			throw new Error("No previous picture available");
		}
	}

	/**
	 * Displays in viewer a picture near to given coordinates
	 *
	 * @param {number} lat Latitude (WGS84)
	 * @param {number} lon Longitude (WGS84)
	 * @returns {Promise} Resolves on picture ID if picture found, otherwise rejects
	 */
	goToPosition(lat, lon) {
		return fetch(this._api.getPicturesAroundCoordinatesUrl(lat, lon), this._api._getFetchOptions())
			.then(res => res.json())
			.then(res => {
				if(res.features.length > 0) {
					this.goToPicture(res.features[0].id);
					return res.features[0].id;
				}
				else {
					return Promise.reject(new Error("No picture found nearby given coordinates"));
				}
			});
	}

	/**
	 * Move the view of main component to its center.
	 * For map, center view on selected picture.
	 * For picture, center view on image center.
	 */
	moveCenter() {
		const meta = this.getPictureMetadata();
		if(!meta) { return; }

		if(this.map && this.isMapWide()) {
			this.map._map.flyTo({ center: meta.gps, zoom: 20 });
		}
		else {
			this._psvAnimate({
				speed: PSV_ANIM_DURATION,
				yaw: -meta.sphereCorrection.pan,
				pitch: 0,
				zoom: PSV_DEFAULT_ZOOM
			});
		}
	}

	/**
	 * Moves the view of main component slightly to the left.
	 */
	moveLeft() {
		this._moveToDirection("left");
	}

	/**
	 * Moves the view of main component slightly to the right.
	 */
	moveRight() {
		this._moveToDirection("right");
	}

	/**
	 * Moves the view of main component slightly to the top.
	 */
	moveUp() {
		this._moveToDirection("up");
	}

	/**
	 * Moves the view of main component slightly to the bottom.
	 */
	moveDown() {
		this._moveToDirection("down");
	}

	/**
	 * Moves map or picture viewer to given direction.
	 * @param {string} dir Direction to move to (up, left, down, right)
	 * @private
	 */
	_moveToDirection(dir) {
		if(this.map && this.isMapWide()) {
			let pan;
			switch(dir) {
			case "up":
				pan = [0, -MAP_MOVE_DELTA];
				break;
			case "left":
				pan = [-MAP_MOVE_DELTA, 0];
				break;
			case "down":
				pan = [0, MAP_MOVE_DELTA];
				break;
			case "right":
				pan = [MAP_MOVE_DELTA, 0];
				break;
			}
			this.map._map.panBy(pan);
		}
		else {
			let pos = this.psv.getPosition();
			switch(dir) {
			case "up":
				pos.pitch += PSV_MOVE_DELTA;
				break;
			case "left":
				pos.yaw -= PSV_MOVE_DELTA;
				break;
			case "down":
				pos.pitch -= PSV_MOVE_DELTA;
				break;
			case "right":
				pos.yaw += PSV_MOVE_DELTA;
				break;
			}
			this._psvAnimate({ speed: PSV_ANIM_DURATION, ...pos });
		}
	}

	/**
	 * Overrided PSV animate function to ensure a single animation plays at once.
	 * @param {object} options PSV animate options
	 * @private
	 */
	_psvAnimate(options) {
		if(this._lastPsvAnim) { this._lastPsvAnim.cancel(); }
		this._lastPsvAnim = this.psv.animate(options);
	}

	/**
	 * Is the map shown as main element instead of viewer (wide map mode) ?
	 *
	 * @returns {boolean} True if map is wider than viewer
	 */
	isMapWide() {
		if(!this.map) { throw new Error("Map is not enabled"); }
		return this.mapContainer.parentNode == this.mainContainer;
	}

	/**
	 * Is the viewer running in a small container (small embed or smartphone)
	 * @returns {boolean} True if container is small
	 */
	isSmall() {
		return this.container?.offsetWidth < 576;
	}

	/**
	 * Is the viewer running in a small-height container (small embed or smartphone)
	 * @returns {boolean} True if container height is small
	 */
	isHeightSmall() {
		return this.container?.offsetHeight < 400;
	}

	/**
	 * Force refresh of GeoVisio vector tiles on the map.
	 */
	reloadVectorTiles() {
		if(!this.map) { throw new Error("Map is not enabled"); }
		reloadVectorTiles(this.map);
	}

	/**
	 * Get the currently selected map background
	 * @returns {string} aerial or streets
	 */
	getMapBackground() {
		if(
			!this.map._hasTwoBackgrounds
			|| !this.map._map.getLayer(RASTER_LAYER_ID)
		) {
			return;
		}

		const aerialVisible = this.map._map.getLayoutProperty(RASTER_LAYER_ID, "visibility") == "visible";
		return aerialVisible ? "aerial" : "streets";
	}

	/**
	 * Change the shown background in map.
	 * @param {string} bg The new background to display (aerial or streets)
	 * @private
	 */
	switchMapBackground(bg) {
		if(!this.map) { throw new Error("Map is not enabled"); }
		if(!this.map._hasTwoBackgrounds && bg === "aerial") { throw new Error("No aerial imagery available"); }
		if(this.map._hasTwoBackgrounds) {
			this.map._map.setLayoutProperty(RASTER_LAYER_ID, "visibility", bg === "aerial" ? "visible" : "none");

			/**
			 * Event for map background changes
			 *
			 * @event map-background-changed
			 * @type {object}
			 * @property {string} [background] The new selected background (aerial, streets)
			 */
			const event = new CustomEvent("map-background-changed", { detail: { background: bg || "streets" }});
			this.dispatchEvent(event);
		}
	}

	/**
	 * Get the currently visible users
	 * @returns {string[]} List of visible users
	 */
	getVisibleUsers() {
		if(!this.map) { throw new Error("Map is not enabled"); }
		return [...this.map._userLayers].filter(l => this.map._map.getLayoutProperty(`${l}_pictures`, "visibility") === "visible");
	}

	/**
	 * Change the visible users on map
	 * @param {string[]} usersIds List of user IDs to display (plus "geovisio" alias for everyone)
	 */
	switchVisibleUsers(usersIds) {
		if(!this.map) { throw new Error("Map is not enabled"); }

		switchUserLayers(
			this.map,
			usersIds,
			this._api,
			this.map._getSequencesLayerStyleProperties(),
			this.map._getPicturesLayerStyleProperties(),
			this.map._onPicClick.bind(this.map),
			this.map._onSeqClick.bind(this.map)
		);

		/**
		 * Event for visible users changes
		 *
		 * @event users-changed
		 * @type {object}
		 * @property {string[]} [usersIds] The list of newly selected users
		 */
		const event = new CustomEvent("users-changed", { detail: { usersIds }});
		this.dispatchEvent(event);
	}

	/**
	 * Get the query string for JOSM to load current picture area
	 * @returns {string} The query string, or null if not available
	 * @private
	 */
	_josmBboxParameters() {
		const meta = this.getPictureMetadata();
		if(meta) {
			const coords = meta.gps;
			const heading = typeof meta?.sphereCorrection?.pan === "number" ? - meta.sphereCorrection.pan / (Math.PI / 180) : null;
			const delta = 0.0002;
			const values = {
				left: coords[0] - (heading === null || heading >= 180 ? delta : 0),
				right: coords[0] + (heading === null || heading <= 180 ? delta : 0),
				top: coords[1] + (heading === null || heading <= 90 || heading >= 270 ? delta : 0),
				bottom: coords[1] - (heading === null || (heading >= 90 && heading <= 270) ? delta : 0),
				changeset_source: "Panoramax"
			};
			return Object.entries(values).map(e => e.join("=")).join("&");
		}
		else { return null; }
	}

	/**
	 * Enable or disable JOSM live editing using [Remote](https://josm.openstreetmap.de/wiki/Help/RemoteControlCommands)
	 * @param {boolean} enabled Set to true to enable JOSM live
	 * @returns {Promise} Resolves on JOSM live being enabled or disabled
	 */
	toggleJOSMLive(enabled) {
		if(enabled) {
			/**
			 * Event for JOSM live enabled
			 *
			 * @event josm-live-enabled
			 */
			const event = new CustomEvent("josm-live-enabled");
			this.dispatchEvent(event);

			// Check if JOSM remote is enabled
			return fetch(JOSM_REMOTE_URL+"/version")
				.catch(e => {
					this.dispatchEvent(new CustomEvent("josm-live-disabled"));
					throw e;
				})
				.then(() => {
					// First loading : download + zoom
					const p1 = this._josmBboxParameters();
					if(p1) {
						const url = `${JOSM_REMOTE_URL}/load_and_zoom?${p1}`;
						fetch(url).catch(e => console.warn(e));
					}

					// Enable event listening
					this._josmListener = () => {
						const p2 = this._josmBboxParameters();
						if(p2) {
							// Next loadings : just zoom
							//   This avoids desktop focus to go on JOSM instead of
							//   staying on web browser
							const url = `${JOSM_REMOTE_URL}/zoom?${p2}`;
							fetch(url).catch(e => console.warn(e));
						}
					};
					this.addEventListener("picture-loaded", this._josmListener);
					this.addEventListener("picture-loading", this._josmListener);
				});
		}
		else {
			/**
			 * Event for JOSM live disabled
			 *
			 * @event josm-live-disabled
			 */
			const event = new CustomEvent("josm-live-disabled");
			this.dispatchEvent(event);

			if(this._josmListener) {
				this.removeEventListener("picture-loading", this._josmListener);
				this.removeEventListener("picture-loaded", this._josmListener);
				delete this._josmListener;
			}
			return Promise.resolve();
		}
	}

	/**
	 * Change the viewer focus (either on picture or map)
	 *
	 * @param {string} focus The object to focus on (map, pic)
	 * @param {boolean} [skipEvent=false] True to not send focus-changed event
	 */
	setFocus(focus, skipEvent = false) {
		if(!this.map) { throw new Error("Map is not enabled"); }

		this.mapContainer.parentElement?.removeChild(this.mapContainer);
		this.psvContainer.parentElement?.removeChild(this.psvContainer);

		if(focus === "map") {
			this.psv.stopKeyboardControl();
			this.map._map.keyboard.enable();
			this.container.classList.add("gvs-focus-map");
			this.mainContainer.appendChild(this.mapContainer);
			this.miniContainer.appendChild(this.psvContainer);
			this.map._map.getCanvas().focus();
		}
		else {
			this.map._map.keyboard.disable();
			this.psv.startKeyboardControl();
			this.container.classList.remove("gvs-focus-map");
			this.mainContainer.appendChild(this.psvContainer);
			this.miniContainer.appendChild(this.mapContainer);
			this.psvContainer.focus();
		}

		this.map._map.resize();
		this.psv.autoSize();

		if(!skipEvent) {
			/**
			 * Event for focus change (either map or picture is shown wide)
			 *
			 * @event focus-changed
			 * @type {object}
			 * @property {string} focus Component now focused on (map, pic)
			 */
			const event = new CustomEvent("focus-changed", { detail: { focus } });
			this.dispatchEvent(event);
		}
	}

	/**
	 * Toggle the viewer focus (either on picture or map)
	 */
	toggleFocus() {
		if(!this.map) { throw new Error("Map is not enabled"); }
		this.setFocus(this.isMapWide() ? "pic" : "map");
	}

	/**
	 * Change the visibility of reduced component (picture or map)
	 *
	 * @param {boolean} visible True to make reduced component visible
	 */
	setUnfocusedVisible(visible) {
		if(!this.map) { throw new Error("Map is not enabled"); }

		if(visible) {
			this.container.classList.remove("gvs-mini-hidden");
		}
		else {
			this.container.classList.add("gvs-mini-hidden");
		}

		this.map._map.resize();
		this.psv.autoSize();
	}

	/**
	 * Toggle the visibility of reduced component (picture or map)
	 */
	toggleUnfocusedVisible() {
		if(!this.map) { throw new Error("Map is not enabled"); }
		this.setUnfocusedVisible(this.container.classList.contains("gvs-mini-hidden"));
	}

	/**
	 * Enable or disable higher contrast on picture
	 * @param {boolean} enable True to enable higher contrast
	 */
	setPictureHigherContrast(enable) {
		this.psv.renderer.renderer.toneMapping = enable ? 3 : 0;
		this.psv.renderer.renderer.toneMappingExposure = enable ? 2 : 1;
		this.psv.needsUpdate();
	}

	/**
	 * Change the shown position in picture
	 *
	 * @param {number} x X position (in degrees)
	 * @param {number} y Y position (in degrees)
	 * @param {number} z Z position (0-100)
	 */
	setXYZ(x, y, z) {
		const coords = this._xyzToPosition(x, y, z);
		this.psv.rotate({ yaw: coords.yaw, pitch: coords.pitch });
		this.psv.zoom(coords.zoom);
	}

	/**
	 * Get the duration of stay on a picture during a sequence play.
	 * @returns {number} The duration (in milliseconds)
	 */
	getTransitionDuration() {
		return this._transitionDuration;
	}

	/**
	 * Changes the duration of stay on a picture during a sequence play.
	 * 
	 * @param {number} value The new duration (in milliseconds, between 0 and 3000)
	 */
	setTransitionDuration(value) {
		value = parseFloat(value);
		if(value < 0 || value > PIC_MAX_STAY_DURATION) {
			throw new Error("Invalid transition duration (should be between 0 and "+PIC_MAX_STAY_DURATION+")");
		}
		this._transitionDuration = value;

		/**
		 * Event for transition duration change
		 *
		 * @event transition-duration-changed
		 * @type {object}
		 * @property {string} duration New duration (in milliseconds)
		 */
		const event = new CustomEvent("transition-duration-changed", { detail: { value } });
		this.dispatchEvent(event);
	}

	/**
	 * Change the map filters
	 * @param {object} filters Filtering values
	 * @param {string} [filters.minDate] Start date for pictures (format YYYY-MM-DD)
	 * @param {string} [filters.maxDate] End date for pictures (format YYYY-MM-DD)
	 * @param {string} [filters.type] Type of picture to keep (flat, equirectangular)
	 * @param {string} [filters.camera] Camera make and model to keep
	 * @param {string} [filters.theme] Map theme to use
	 * @param {boolean} [skipZoomIn=false] If true, doesn't force zoom in to map level >= 7
	 */
	setFilters(filters, skipZoomIn = false) {
		let mapSeqFilters = [];
		let mapPicFilters = [];
		this._mapFilters = {};

		if(filters.minDate && filters.minDate !== "") {
			this._mapFilters.minDate = filters.minDate;
			mapSeqFilters.push([">=", ["get", "date"], filters.minDate]);
			mapPicFilters.push([">=", ["get", "ts"], filters.minDate]);
		}
		if(filters.maxDate && filters.maxDate !== "") {
			this._mapFilters.maxDate = filters.maxDate;
			mapSeqFilters.push(["<=", ["get", "date"], filters.maxDate]);

			// Get tomorrow date for pictures filtering
			// (because ts is date+time, so comparing date only string would fail otherwise)
			let d = new Date(filters.maxDate);
			d.setDate(d.getDate() + 1);
			d = d.toISOString().split("T")[0];
			mapPicFilters.push(["<=", ["get", "ts"], d]);
		}
		if(filters.type && filters.type !== "") {
			this._mapFilters.type = filters.type;
			mapSeqFilters.push(["==", ["get", "type"], filters.type]);
			mapPicFilters.push(["==", ["get", "type"], filters.type]);
		}
		if(filters.camera && filters.camera !== "") {
			this._mapFilters.camera = filters.camera;
			// low/high model hack : to enable fuzzy filtering of camera make and model
			const lowModel = filters.camera.toLowerCase().trim() + "                    ";
			const highModel = filters.camera.toLowerCase().trim() + "zzzzzzzzzzzzzzzzzzzz";
			const collator = ["collator", { "case-sensitive": false, "diacritic-sensitive": false } ];
			mapSeqFilters.push([">=", ["get", "model"], lowModel, collator]);
			mapSeqFilters.push(["<=", ["get", "model"], highModel, collator]);
			mapPicFilters.push([">=", ["get", "model"], lowModel, collator]);
			mapPicFilters.push(["<=", ["get", "model"], highModel, collator]);
		}

		if(filters.theme && Object.values(MAP_THEMES).includes(filters.theme)) {
			this._mapFilters.theme = filters.theme;
			if(this.getMap()) {
				this.map._theme = this._mapFilters.theme;
				this.map._reloadLayersStyles();
			}
		}

		if(mapSeqFilters.length == 0) { mapSeqFilters = null; }
		else {
			mapSeqFilters.unshift("all");
			mapSeqFilters = ["step", ["zoom"],
				true,
				7, mapSeqFilters
			];
		}

		if(mapPicFilters.length == 0) { mapPicFilters = null; }
		else {
			mapPicFilters.unshift("all");
			mapPicFilters = ["step", ["zoom"],
				true,
				13, mapPicFilters
			];
		}

		if(this.getMap()) {
			filterUserLayersContent(this.map, "sequences", mapSeqFilters);
			filterUserLayersContent(this.map, "pictures", mapPicFilters);
			if(
				!skipZoomIn
				&& (
					mapSeqFilters !== null
					|| mapPicFilters !== null
					|| (this._mapFilters.theme !== null && this._mapFilters.theme !== MAP_THEMES.DEFAULT)
				)
				&& this.map._map.getZoom() < 7
			) {
				this.map._map.easeTo({ zoom: 7 });
			}
		}

		/**
		 * Event for map filters changes
		 *
		 * @event map-filters-changed
		 * @type {object}
		 * @property {string} [minDate] The minimum date in time range (ISO format)
		 * @property {string} [maxDate] The maximum date in time range (ISO format)
		 * @property {string} [type] Camera type (equirectangular, flat, null/empty string for both)
		 * @property {string} [camera] Camera make and model
		 * @property {string} [theme] Map theme
		 */
		const event = new CustomEvent("map-filters-changed", { detail: Object.assign({}, this._mapFilters) });
		this.dispatchEvent(event);
	}
}

export {
	Viewer as default,
	Viewer,
	PSV_DEFAULT_ZOOM as DEFAULT_ZOOM,
	PSV_ZOOM_DELTA,
	PSV_ANIM_DURATION,
	PIC_MAX_STAY_DURATION,
};