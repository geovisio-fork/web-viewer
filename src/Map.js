import "./css/Map.css";
import "maplibre-gl/dist/maplibre-gl.css";
// DO NOT REMOVE THE "!": bundled builds breaks otherwise !!!
import maplibregl from "!maplibre-gl";
import maplibreglWorker from "maplibre-gl/dist/maplibre-gl-csp-worker";
import * as pmtiles from "pmtiles";
import MarkerBaseSVG from "./img/marker.svg";
import MarkerSelectedSVG from "./img/marker_blue.svg";
import {
	getThumbGif, reloadLayersStyles, getPictureIdForSequence,
	forwardGeocodingBAN, forwardGeocodingNominatim
} from "./MapUtils";

maplibregl.workerClass = maplibreglWorker;
maplibregl.addProtocol("pmtiles", new pmtiles.Protocol().tile);

const COLORS = {
	BASE: "#FF6F00",
	SELECTED: "#1E88E5",
	HIDDEN: "#34495E",

	QUALI_1: "#00695C", // 360
	QUALI_2: "#fd8d3c", // flat

	PALETTE_1: "#fecc5c", // Oldest
	PALETTE_2: "#fd8d3c",
	PALETTE_3: "#f03b20",
	PALETTE_4: "#bd0026" // Newest
};

const COLORS_HEX = Object.fromEntries(Object.entries(COLORS).map(e => {
	e[1] = parseInt(e[1].slice(1), 16);
	return e;
}));

const VECTOR_STYLES = {
	PICTURES: {
		"paint": {
			"circle-radius": ["interpolate", ["linear"], ["zoom"], 14, 3, 17, 8, 22, 12],
			"circle-opacity": ["interpolate", ["linear"], ["zoom"], 14, 0, 15, 1],
			"circle-stroke-color": "#ffffff",
			"circle-stroke-width": ["interpolate", ["linear"], ["zoom"], 17, 0, 20, 2],
		},
		"layout": {}
	},
	SEQUENCES: {
		"paint": {
			"line-width": ["interpolate", ["linear"], ["zoom"], 0, 0.5, 10, 2, 14, 4, 16, 5, 22, 3],
		},
		"layout": {
			"line-cap": "square",
		}
	},
};

const THEMES = {
	DEFAULT: "default",
	AGE: "age",
	TYPE: "type",
};

const DEFAULT_TILES = "https://panoramax.openstreetmap.fr/pmtiles/basic.json";
const RASTER_LAYER_ID = "gvs-aerial";


/**
 * @summary Map showing photo location
 * @private
 */
class Map {
	/**
	 * @param {Viewer} viewer The viewer
	 * @param {object} [options] Optional settings (can be any of [MapLibre GL settings](https://maplibre.org/maplibre-gl-js-docs/api/map/#map-parameters))
	 * @param {object|string} [options.style] The MapLibre style for streets background. This must be an a JSON object conforming to the schema described in the [MapLibre Style Specification](https://maplibre.org/maplibre-gl-js-docs/style-spec/), or a URL to such JSON.
 	 * @param {object} [options.raster] The MapLibre raster source for aerial background. This must be a JSON object following [MapLibre raster source definition](https://maplibre.org/maplibre-style-spec/sources/#raster).
	 * @param {string} [options.background] The default background to display (aerial, streets)
	 * @param {object} [options.geocoder] Optional geocoder settings
	 * @param {string} [options.geocoder.engine] Use a particular geocoder engine (nominatim, ban)
	 * @param {string} [options.theme] The map theme to use (default, age, type)
	 * @param {string[]} [options.users] The IDs of users whom data should appear on map (defaults to all)
	 * @param {object} [lang] The translated labels to use
	 */
	constructor(viewer, options = {}, lang = {}) {
		this.viewer = viewer;
		this._lang = lang;
		this._theme = options.theme || THEMES.DEFAULT;
		this._hasTwoBackgrounds = false;
		this._userLayers = new Set();

		// Check if API supports tiles per user
		if(options.users && options.users.length > 0) {
			this.viewer._api.getUserPicturesTilesUrl(options.users[0]); // Raises error if API not compatible
		}

		// Create map
		this._map = new maplibregl.Map({
			container: this.viewer.mapContainer,
			style: DEFAULT_TILES,
			center: [0,0],
			zoom: 0,
			maxZoom: 24,
			attributionControl: false,
			dragRotate: false,
			pitchWithRotate: false,
			...options
		});

		// Handle raster source
		if(options.raster) {
			this._hasTwoBackgrounds = true;
			options.background = options.background || "streets";
			this._map.on("load", () => {
				this._map.addSource("gvs-aerial", options.raster);
				this._map.addLayer({
					"id": RASTER_LAYER_ID,
					"type": "raster",
					"source": "gvs-aerial",
				});
				this.viewer.switchMapBackground(options.background);
			});
		}

		// Create invisible attribution control
		// Just to get string for GeoVisio's custom widgets
		this._attribution = new maplibregl.AttributionControl({ compact: false });
		this._map.addControl(this._attribution);
		this._attribution._container.classList.add("gvs-hidden");

		this._initGeocoder(options.geocoder);

		// Retrieve data area (if no center/zoom options has been defined)
		if((!options.center || options.center == [0,0]) && (!options.zoom || options.zoom === 0)) {
			this.viewer._api.onceReady().then(() => {
				let bbox = this.viewer?._api?.getDataBbox();
				if(bbox) {
					try {
						bbox = new maplibregl.LngLatBounds(bbox);
						if(this._map.loaded()) { this._map.fitBounds(bbox, { "animate": false }); }
						else { this._map.on("load", () => this._map.fitBounds(bbox, { "animate": false })); }
					}
					catch(e) {
						console.warn("Received invalid bbox: "+bbox);
					}
				}
			});
		}

		// Widgets and markers
		this._picMarker = this._getPictureMarker();
		this._picMarkerPreview = this._getPictureMarker(false);
		this._thumbGif = getThumbGif(this._lang);
		this._map.on("load", () => {
			this.viewer.switchVisibleUsers(options.users && options.users.length > 0 ? options.users : ["geovisio"]);
			this._listenToViewerEvents();
			this._listenToKeyboard();
			this._map.resize();
		});

		// Cache for pictures and sequences thumbnails
		this._picThumbUrl = {};
		this._seqPictures = {};
	}


	/**
	 * Ends all form of life in this object.
	 */
	destroy() {
		this._map.remove();
		delete this._map;
		delete this._attribution;
		delete this.viewer;
	}

	/**
	 * Event handler for picture click on map
	 * @param {object} f The clicked feature
	 * @private
	 */
	_onPicClick(f) {
		// Look for a potential sequence ID
		let seqId = null;
		try {
			if(f.properties.sequences) {
				if(!Array.isArray(f.properties.sequences)) { f.properties.sequences = JSON.parse(f.properties.sequences); }
				seqId = f.properties.sequences.pop();
			}
		}
		catch(e) {
			console.log("Sequence ID is not available in vector tiles for picture "+f.properties.id);
		}
		this.viewer.goToPicture(f.properties.id, seqId);
		if(!this.viewer._myVTour.state.currentNode) { this.viewer.setFocus("pic"); }
	}

	/**
	 * Event handler for sequence click on map
	 * @param {string} seqId 
	 * @param {number[]} coords 
	 * @private
	 */
	_onSeqClick(seqId, coords) {
		getPictureIdForSequence(this, this.viewer._api, seqId, coords)
			.then(picId => {
				if(picId) {
					this.viewer.goToPicture(picId);
					if(!this.viewer._myVTour.state.currentNode) { this.viewer.setFocus("pic"); }
				}
			});
	}

	/**
	 * Computes dates to use for map theme by picture/sequence age
	 * @private
	 */
	_getDatesForLayerColors() {
		const oneDay = 24 * 60 * 60 * 1000;
		const d0 = Date.now();
		const d1 = d0 - 30 * oneDay;
		const d2 = d0 - 365 * oneDay;
		const d3 = d0 - 2 * 365 * oneDay;
		return [d1, d2, d3].map(d => new Date(d).toISOString().split("T")[0]);
	}

	/**
	 * Retrieve map layer color scheme according to selected theme.
	 * @private
	 */
	_getLayerColorStyle(layer) {
		// Hidden style
		const s = ["case",
			["==", ["get", "hidden"], true], COLORS.HIDDEN
		];

		// Selected sequence style
		const picId = this.viewer._myVTour?.state?.loadingNode || this.viewer._myVTour?.state?.currentNode?.id;
		const seqId = picId ? this.viewer._picturesSequences[picId] : null;
		if(layer == "sequences" && seqId) {
			s.push(["==", ["get", "id"], seqId], COLORS.SELECTED);
		}
		else if(layer == "pictures" && seqId) {
			s.push(["in", seqId, ["get", "sequences"]], COLORS.SELECTED);
		}
		
		// Themes styles
		if(this._theme == THEMES.AGE) {
			const prop = layer == "sequences" ? "date" : "ts";
			const dt = this._getDatesForLayerColors();

			s.push(
				["!", ["has", prop]], COLORS.BASE,
				[">=", ["get", prop], dt[0]], COLORS.PALETTE_4,
				[">=", ["get", prop], dt[1]], COLORS.PALETTE_3,
				[">=", ["get", prop], dt[2]], COLORS.PALETTE_2,
				COLORS.PALETTE_1
			);
		}
		else if(this._theme == THEMES.TYPE) {
			s.push(
				["!", ["has", "type"]], COLORS.BASE,
				["==", ["get", "type"], "equirectangular"], COLORS.QUALI_1,
				COLORS.QUALI_2
			);
		}
		else {
			s.push(COLORS.BASE);
		}

		return s;
	}

	/**
	 * Retrieve map sort key according to selected theme.
	 * @private
	 */
	_getLayerSortStyle(layer) {
		// Values
		//  - 100 : on top / selected feature
		//  - 90  : hidden feature
		//  - 20-80 : custom ranges
		//  - 10  : basic feature
		//  - 0   : on bottom / feature with undefined property
		// Hidden style
		const s = ["case",
			["==", ["get", "hidden"], true], 90
		];

		// Selected sequence style
		const picId = this.viewer._myVTour?.state?.loadingNode || this.viewer._myVTour?.state?.currentNode?.id;
		const seqId = picId ? this.viewer._picturesSequences[picId] : null;
		if(layer == "sequences" && seqId) {
			s.push(["==", ["get", "id"], seqId], 100);
		}
		else if(layer == "pictures" && seqId) {
			s.push(["in", seqId, ["get", "sequences"]], 100);
		}

		// Themes styles
		if(this._theme == THEMES.AGE) {
			const prop = layer == "sequences" ? "date" : "ts";
			const dt = this._getDatesForLayerColors();
			s.push(
				["!", ["has", prop]], 0,
				[">=", ["get", prop], dt[0]], 50,
				[">=", ["get", prop], dt[1]], 49,
				[">=", ["get", prop], dt[2]], 48,
			);
		}
		else if(this._theme == THEMES.TYPE) {
			s.push(
				["!", ["has", "type"]], 0,
				["==", ["get", "type"], "equirectangular"], 50,
			);
		}

		s.push(10);
		return s;
	}

	/**
	 * Forces reload of pictures/sequences layer styles.
	 * This is useful after a map theme change.
	 * @private
	 */
	_reloadLayersStyles() {
		reloadLayersStyles(this);
	}

	/**
	 * MapLibre paint/layout properties for pictures layer
	 * This is useful when selected picture changes to allow partial update
	 *
	 * @returns {object} Paint/layout properties
	 * @private
	 */
	_getPicturesLayerStyleProperties() {
		return {
			"paint": Object.assign({
				"circle-color": this._getLayerColorStyle("pictures"),
			}, VECTOR_STYLES.PICTURES.paint),
			"layout": Object.assign({
				"circle-sort-key": this._getLayerSortStyle("pictures"),
			}, VECTOR_STYLES.PICTURES.layout)
		};
	}

	/**
	 * MapLibre paint/layout properties for sequences layer
	 *
	 * @returns {object} Paint/layout properties
	 * @private
	 */
	_getSequencesLayerStyleProperties() {
		return {
			"paint": Object.assign({
				"line-color": this._getLayerColorStyle("sequences"),
			}, VECTOR_STYLES.SEQUENCES.paint),
			"layout": Object.assign({
				"line-sort-key": this._getLayerSortStyle("sequences"),
			}, VECTOR_STYLES.SEQUENCES.layout)
		};
	}

	/**
	 * Creates the geocoder search bar
	 * @private
	 * @param {object} options The Geocoder options
	 */
	_initGeocoder(options = {}) {
		const engines = { "ban": forwardGeocodingBAN, "nominatim": forwardGeocodingNominatim };
		const engine = options.engine || "nominatim";
		this.geocoder = engines[engine];
	}

	/**
	 * Create a ready-to-use picture marker
	 *
	 * @returns {maplibregl.Marker} The generated marker
	 * @private
	 */
	_getPictureMarker(selected = true) {
		const img = document.createElement("img");
		img.src = selected ? MarkerSelectedSVG : MarkerBaseSVG;
		return new maplibregl.Marker({
			element: img
		});
	}

	/**
	 * Start listening to picture changes in PSV
	 *
	 * @private
	 */
	_listenToViewerEvents() {
		// Switched picture
		const onPicLoad = e => {
			this._picMarkerPreview.remove();

			// Show marker corresponding to selection
			this._picMarker
				.setLngLat([e.detail.lon, e.detail.lat])
				.setRotation(e.detail.x)
				.addTo(this._map);
			
			// Update map style to see selected sequence
			this._reloadLayersStyles();

			// Move map to picture coordinates
			this._map.flyTo({
				center: [e.detail.lon, e.detail.lat],
				zoom: this._map.getZoom() < 15 ? 20 : this._map.getZoom(),
				maxDuration: 2000
			});
		};
		this.viewer.addEventListener("picture-loading", onPicLoad);
		this.viewer.addEventListener("picture-loaded", onPicLoad);

		// Picture view rotated
		this.viewer.addEventListener("view-rotated", () => {
			const x = this.viewer.psv.getPosition().yaw * (180/Math.PI);
			this._picMarker.setRotation(x);
		});

		// Picture preview
		this.viewer.addEventListener("picture-preview-started", e => {
			// Show marker corresponding to selection
			this._picMarkerPreview
				.setLngLat(e.detail.coordinates)
				.setRotation(e.detail.direction || 0)
				.addTo(this._map);
		});

		this.viewer.addEventListener("picture-preview-stopped", () => {
			this._picMarkerPreview.remove();
		});

		this.viewer.addEventListener("picture-loaded", e => {
			if(this.viewer.isSmall() && e.detail.picId == this._picPopup._picId) {
				this._picPopup.remove();
			}
		});
	}

	/**
	 * Adds events related to keyboard
	 * @private
	 */
	_listenToKeyboard() {
		const v = this.viewer;
		this._map.keyboard.keydown = function(e) {
			if (e.altKey || e.ctrlKey || e.metaKey) return;
	
			// Custom GeoVisio keys
			switch(e.key) {
			case "*":
			case "5":
				v.moveCenter();
				return;

			case "PageUp":
			case "9":
				v.goToNextPicture();
				return;
			
			case "PageDown":
			case "3":
				v.goToPrevPicture();
				return;
			
			case "Home":
			case "7":
				e.stopPropagation();
				v.toggleFocus();
				return;
			
			case "End":
			case "1":
				v.toggleUnfocusedVisible();
				return;
			
			case " ":
			case "0":
				v.toggleSequencePlaying();
				return;
			}

			let zoomDir = 0;
			let bearingDir = 0;
			let pitchDir = 0;
			let xDir = 0;
			let yDir = 0;
	
			switch (e.keyCode) {
			case 61:
			case 107:
			case 171:
			case 187:
				zoomDir = 1;
				break;

			case 189:
			case 109:
			case 173:
				zoomDir = -1;
				break;

			case 37:
			case 100:
				if (e.shiftKey) {
					bearingDir = -1;
				} else {
					e.preventDefault();
					xDir = -1;
				}
				break;

			case 39:
			case 102:
				if (e.shiftKey) {
					bearingDir = 1;
				} else {
					e.preventDefault();
					xDir = 1;
				}
				break;

			case 38:
			case 104:
				if (e.shiftKey) {
					pitchDir = 1;
				} else {
					e.preventDefault();
					yDir = -1;
				}
				break;

			case 40:
			case 98:
				if (e.shiftKey) {
					pitchDir = -1;
				} else {
					e.preventDefault();
					yDir = 1;
				}
				break;

			default:
				return;
			}
	
			if (this._rotationDisabled) {
				bearingDir = 0;
				pitchDir = 0;
			}
	
			return {
				cameraAnimation: (map) => {
					const tr = this._tr;
					map.easeTo({
						duration: 300,
						easeId: "keyboardHandler",
						easing: t => t * (2-t),
						zoom: zoomDir ? Math.round(tr.zoom) + zoomDir * (e.shiftKey ? 2 : 1) : tr.zoom,
						bearing: tr.bearing + bearingDir * this._bearingStep,
						pitch: tr.pitch + pitchDir * this._pitchStep,
						offset: [-xDir * this._panStep, -yDir * this._panStep],
						center: tr.center
					}, {originalEvent: e});
				}
			};
		}.bind(this._map.keyboard);
	}
}

export {
	Map as default,
	Map,
	THEMES,
	COLORS,
	COLORS_HEX,
	DEFAULT_TILES,
	VECTOR_STYLES,
	RASTER_LAYER_ID,
};