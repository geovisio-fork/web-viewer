/**
 * Get cartesian distance between two points
 * @param {number[]} from Start [x,y] coordinates
 * @param {number[]} to End [x,y] coordinates
 * @returns {number} The distance
 * @private
 */
export function getDistance(from, to) {
	const dx = from[0] - to[0];
	const dy = from[1] - to[1];
	return Math.sqrt(dx*dx + dy*dy);
}

/**
 * Compare function to retrieve most appropriate picture in a single direction.
 * 
 * @param {number[]} picPos The picture [x,y] position
 * @returns {function} A compare function for sorting
 * @private
 */
export function sortPicturesInDirection(picPos) {
	return (a,b) => {
		// Two prev/next links = no sort
		if(a.rel != "related" && b.rel != "related") { return 0; }
		// First is prev/next link = goes first
		else if(a.rel != "related") { return -1; }
		// Second is prev/next link = goes first
		else if(b.rel != "related") { return 1; }
		// Two related links same day = nearest goes first
		else if(a.date == b.date) { return getDistance(picPos, a.geometry.coordinates) - getDistance(picPos, b.geometry.coordinates); }
		// Two related links at different day = recent goes first
		else { return b.date.localeCompare(a.date); }
	};
}

/**
 * Get direction based on angle
 * @param {number[]} from Start [x,y] coordinates
 * @param {number[]} to End [x,y] coordinates
 * @returns {string} Direction (N/ENE/ESE/S/WSW/WNW)
 * @private
 */
export function getSimplifiedAngle(from, to) {
	const angle = Math.atan2(to[0] - from[0], to[1] - from[1]) * (180 / Math.PI); // -180 to 180°

	// 6 directions version
	if (Math.abs(angle) < 30) { return "N"; }
	else if (angle >= 30 && angle < 90) { return "ENE"; }
	else if (angle >= 90 && angle < 150) { return "ESE"; }
	else if (Math.abs(angle) >= 150) { return "S"; }
	else if (angle <= -30 && angle > -90) { return "WNW"; }
	else if (angle <= -90 && angle > -150) { return "WSW"; }
}