import "maplibre-gl/dist/maplibre-gl.css";
import "./css/StandaloneMap.css";
// DO NOT REMOVE THE "!": bundled builds breaks otherwise !!!
import maplibregl from "!maplibre-gl";
import maplibreglWorker from "maplibre-gl/dist/maplibre-gl-csp-worker";
import API from "./API";
import { COLORS as MAP_COLORS, DEFAULT_TILES, VECTOR_STYLES } from "./Map";
import { getTranslations } from "./I18n";
import { getThumbGif, attachPreviewToPictures, reloadVectorTiles, switchUserLayers, reloadLayersStyles } from "./MapUtils";
import PACKAGE_JSON from "../package.json";

maplibregl.workerClass = maplibreglWorker;


/**
 * The standalone map viewer allows to see STAC pictures data as a map.
 * It only embeds a map (no 360° pictures viewer) with a minimal picture preview (thumbnail).
 * 
 * @fires ready
 * @fires select
 * @fires hover
 * @param {string|Node} container The DOM element to create viewer into
 * @param {string} endpoint URL to API to use (must be a [STAC API](https://github.com/radiantearth/stac-api-spec/blob/main/overview.md))
 * @param {object} [options] Map options. Various settings can be passed, either the ones defined here, or any of [MapLibre GL settings](https://maplibre.org/maplibre-gl-js-docs/api/map/#map-parameters).
 * @param {string} [options.selectedSequence] The ID of sequence to highlight on load (defaults to none)
 * @param {string} [options.selectedPicture] The ID of picture to highlight on load (defaults to none)
 * @param {object} [options.fetchOptions=null] Set custom options for fetch calls made against API ([same syntax as fetch options parameter](https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters))
 * @param {string} [options.picturesTiles] URL for fetching pictures vector tiles if map is enabled (defaults to "xyz" link advertised in STAC API landing page)
 * @param {number} [options.minZoom=0] The minimum zoom level of the map (0-24).
 * @param {number} [options.maxZoom=24] The maximum zoom level of the map (0-24).
 * @param {object|string} [options.style] The map's MapLibre style. This must be an a JSON object conforming to the schema described in the [MapLibre Style Specification](https://maplibre.org/maplibre-gl-js-docs/style-spec/), or a URL to such JSON.
 *   For example, `http://path/to/my/page.html#2.59/39.26/53.07/-24.1/60`.
 *   An additional string may optionally be provided to indicate a parameter-styled hash, e.g. http://path/to/my/page.html#map=2.59/39.26/53.07/-24.1/60&foo=bar, where foo is a custom parameter and bar is an arbitrary hash distinct from the map hash.
 * @param {external:maplibre-gl.LngLatLike} [options.center=[0, 0]] The initial geographical centerpoint of the map. If `center` is not specified in the constructor options, MapLibre GL JS will look for it in the map's style object. If it is not specified in the style, either, it will default to `[0, 0]` Note: MapLibre GL uses longitude, latitude coordinate order (as opposed to latitude, longitude) to match GeoJSON.
 * @param {number} [options.zoom=0] The initial zoom level of the map. If `zoom` is not specified in the constructor options, MapLibre GL JS will look for it in the map's style object. If it is not specified in the style, either, it will default to `0`.
 * @param {external:maplibre-gl.LngLatBoundsLike} [options.bounds] The initial bounds of the map. If `bounds` is specified, it overrides `center` and `zoom` constructor options.
 * @param {string[]} [options.users] The IDs of users whom data should appear on map (defaults to all)
 */
class StandaloneMap extends EventTarget {
	constructor(container, endpoint, options = {}) {
		super();

		if(options == null) { options = {}; }

		if(!options.testing) {
			// Display version in logs
			console.info(`🗺️ GeoVisio Standalone Map - Version ${PACKAGE_JSON.version} (${__COMMIT_HASH__})

🆘 Issues can be reported at ${PACKAGE_JSON.repository.url}`);
		}

		// Cache for pictures and sequences thumbnails
		this._picThumbUrl = {};
		this._seqPictures = {};

		this._lang = getTranslations(options.lang || navigator.language || navigator.userLanguage);
		this._thumbGif = getThumbGif(this._lang);
		this._selectedSeqId = options.selectedSequence || null;
		this._selectedPicId = options.selectedPicture || null;
		this._userLayers = new Set();

		container = typeof container === "string" ? document.getElementById(container) : container;
		container.classList.add("gvs-stmap", "gvs-stmap-loading");
		container.appendChild(this._thumbGif);

		// Init API
		endpoint = endpoint.replace("/api/search", "/api");
		this._api = new API(endpoint, { tiles: options?.picturesTiles, fetch: options?.fetchOptions });

		this._api.onceReady()
			.then(() => {
				// Check if API supports tiles per user
				if(options.users && options.users.length > 0) {
					this._api.getUserPicturesTilesUrl(options.users[0]); // Raises error if API not compatible
				}
				
				options.transformRequest = this._api._getMapRequestTransform();
				this._initMaplibre(container, options);
			});
	}

	/**
	 * Creates MapLibre map object
	 * @private
	 */
	_initMaplibre(container, options) {
		this._map = new maplibregl.Map({
			container,
			style: DEFAULT_TILES,
			center: [0,0],
			zoom: 0,
			maxZoom: 24,
			attributionControl: true,
			dragRotate: false,
			pitchWithRotate: false,
			hash: true,
			...options
		});

		this._map.addControl(new maplibregl.NavigationControl({ showCompass: false }));

		this._map.on("load", () => {
			container.classList.remove("gvs-stmap-loading");
			
			switchUserLayers(
				this,
				options.users && options.users.length > 0 ? options.users : ["geovisio"],
				this._api,
				this._getSequencesLayerStyleProperties(),
				this._getPicturesLayerStyleProperties(),
				this._onPicClick.bind(this),
				seqId => this.select(seqId)
			);

			// Hover event
			this._map.on("mousemove", "sequences", e => {
				e.preventDefault();
				if(e.features.length > 0 && e.features[0].properties?.id) {
					/**
					 * Event when a sequence on map is hovered (not selected)
					 *
					 * @event hover
					 * @type {object}
					 * @property {string} seqId The hovered sequence ID
					 */
					this.dispatchEvent(new CustomEvent("hover", {
						detail: {
							seqId: e.features[0].properties.id
						}
					}));
				}
			});

			/**
			 * Event for map being ready to use (API and data loaded)
			 *
			 * @event ready
			 * @type {object}
			 */
			this.dispatchEvent(new CustomEvent("ready"));
		});
	}

	/**
	 * Event handler for picture click on map
	 * @param {object} f The clicked feature
	 * @private
	 */
	_onPicClick(f) {
		// Look for a potential sequence ID
		let seqId = null;
		try {
			if(f.properties.sequences) {
				if(!Array.isArray(f.properties.sequences)) { f.properties.sequences = JSON.parse(f.properties.sequences); }
				seqId = f.properties.sequences.pop();
			}
		}
		catch(e) {
			console.log("Sequence ID is not available in vector tiles for picture "+f.properties.id);
		}
		
		this.select(seqId, f.properties.id);
	}

	/**
	 * MapLibre paint/layout properties for pictures layer
	 * This is useful when selected picture changes to allow partial update
	 *
	 * @returns {object} Paint/layout properties
	 * @private
	 */
	_getPicturesLayerStyleProperties() {
		return {
			"paint": Object.assign({
				"circle-color": this._getLayerColorStyle("pictures"),
			}, VECTOR_STYLES.PICTURES.paint),
			"layout": Object.assign({
				"circle-sort-key": this._getLayerSortStyle("pictures"),
			}, VECTOR_STYLES.PICTURES.layout)
		};
	}

	/**
	 * MapLibre paint/layout properties for sequences layer
	 *
	 * @returns {object} Paint/layout properties
	 * @private
	 */
	_getSequencesLayerStyleProperties() {
		return {
			"paint": Object.assign({
				"line-color": this._getLayerColorStyle("sequences"),
			}, VECTOR_STYLES.SEQUENCES.paint),
			"layout": Object.assign({
				"line-sort-key": this._getLayerSortStyle("sequences"),
			}, VECTOR_STYLES.SEQUENCES.layout)
		};
	}

	/**
	 * Retrieve map layer color scheme according to selected theme.
	 * @private
	 */
	_getLayerColorStyle(layer) {
		// Hidden style
		const s = ["case",
			["==", ["get", "hidden"], true], MAP_COLORS.HIDDEN
		];

		// Selected sequence style
		const seqId = this._selectedSeqId; //picId ? this.viewer._picturesSequences[picId] : null;
		if(layer == "sequences" && seqId) {
			s.push(["==", ["get", "id"], seqId], MAP_COLORS.SELECTED);
		}
		else if(layer == "pictures" && seqId) {
			s.push(["in", seqId, ["get", "sequences"]], MAP_COLORS.SELECTED);
		}
		
		// Classic style
		s.push(MAP_COLORS.BASE);

		return s;
	}

	/**
	 * Retrieve map sort key according to selected theme.
	 * @private
	 */
	_getLayerSortStyle(layer) {
		// Values
		//  - 100 : on top / selected feature
		//  - 90  : hidden feature
		//  - 20-80 : custom ranges
		//  - 10  : basic feature
		//  - 0   : on bottom / feature with undefined property
		// Hidden style
		const s = ["case",
			["==", ["get", "hidden"], true], 90
		];

		// Selected sequence style
		const seqId = this._selectedSeqId;
		if(layer == "sequences" && seqId) {
			s.push(["==", ["get", "id"], seqId], 100);
		}
		else if(layer == "pictures" && seqId) {
			s.push(["in", seqId, ["get", "sequences"]], 100);
		}

		s.push(10);
		return s;
	}

	/**
	 * Make map fit in given bounding box.
	 * For more details on options, see https://maplibre.org/maplibre-gl-js/docs/API/classes/maplibregl.Map/#fitbounds
	 * @param {import("maplibre-gl").LngLatBoundsLike} bounds 
	 * @param {import("maplibre-gl").FitBoundsOptions} options 
	 */
	fitBounds(bounds, options = null) {
		this._map.fitBounds(bounds, options);
	}

	/**
	 * Force refresh of GeoVisio vector tiles on the map.
	 */
	reloadVectorTiles() {
		reloadVectorTiles(this);
	}

	/**
	 * Highlights a certain sequence/picture on map.
	 * 
	 * @param {string} [seqId] The sequence ID (or null to unselect)
	 * @param {string} [picId] The picture ID (or null to only select sequence)
	 */
	select(seqId = null, picId = null) {
		this._selectedSeqId = seqId;
		this._selectedPicId = picId;
		reloadLayersStyles(this);

		// Move thumbnail to match selected element
		if(picId || seqId) {
			const layer = picId ? "pictures" : "sequences";
			const features = this._map.queryRenderedFeatures({
				layers: [...this._userLayers].map(l => `${l}_${layer}`),
				filter: ["==", ["get", "id"], picId || seqId]
			});

			if(features.length >= 0 && features[0] != null) {
				attachPreviewToPictures(
					this, this._api,
					{ features },
					layer
				);
			}
		}

		/**
		 * Event for sequence/picture selection
		 *
		 * @event select
		 * @type {object}
		 * @property {string} seqId The selected sequence ID
		 * @property {string} picId The selected picture ID (or null if not a precise picture clicked)
		 */
		this.dispatchEvent(new CustomEvent("select", {
			detail: {
				seqId,
				picId,
			}
		}));
	}
}

export default StandaloneMap;