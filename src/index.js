import Viewer from "./Viewer";
import StandaloneMap from "./StandaloneMap";

export {
	Viewer as default,
	Viewer,
	StandaloneMap,
};
