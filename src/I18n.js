import TRANSLATIONS from "./translations.json";

/**
 * Get text labels translations in given language
 *
 * @param {string} lang The language code (fr, en)
 * @returns {object} Translations in given language, with fallback to english
 */
export function getTranslations(lang = "") {
	// Lang exists -> send it
	if(TRANSLATIONS[lang]) {
		return TRANSLATIONS[lang];
	}

	// Look for primary lang
	if(lang.includes("-")) {
		const primaryLang = lang.split("-")[0];
		if(TRANSLATIONS[primaryLang]) { return TRANSLATIONS[primaryLang]; }
	}

	if(lang.includes("_")) {
		const primaryLang = lang.split("_")[0];
		if(TRANSLATIONS[primaryLang]) { return TRANSLATIONS[primaryLang]; }
	}

	// Else, fallbacks to English
	return TRANSLATIONS.en;
}