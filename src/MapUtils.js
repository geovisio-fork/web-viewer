// DO NOT REMOVE THE "!": bundled builds breaks otherwise !!!
import maplibregl from "!maplibre-gl";
import { getDistance } from "./Utils";
import LoaderImg from "./img/logo_anime.gif";

/**
 * Get the GIF shown while thumbnail loads
 * @param {object} lang Translations
 * @returns The DOM element for this GIF
 */
export function getThumbGif(lang) {
	const thumbGif = document.createElement("img");
	thumbGif.src = LoaderImg;
	thumbGif.alt = lang.loading;
	thumbGif.title = lang.loading;
	thumbGif.classList.add("gvs-map-thumb", "gvs-map-thumb-loader");
	return thumbGif;
}

/**
 * Creates source and layers for pictures and sequences.
 * 
 * @param {object} mapHandler The main object containing a MaplibreGL "_map" property
 * @param {API} api The API handler
 * @param {string} tilesUrl The vector tile source URL
 * @param {boolean} isSmall Is the map shown on a small screen
 * @param {string} id The source and layer ID prefix
 * @param {object} seqLayerStyle The MaplibreGL JS style for sequences
 * @param {object} picLayerStyle The MaplibreGL JS style for pictures
 * @param {function} onPicClick Click handler for pictures layer
 * @param {function} onSeqClick Click handler for sequences layer
 */
export function createPicturesTilesLayer(
	mapHandler, api, tilesUrl, isSmall, id,
	seqLayerStyle, picLayerStyle,
	onPicClick, onSeqClick
) {
	mapHandler._userLayers.add(id);

	mapHandler._map.addSource(id, {
		"type": "vector",
		"tiles": [ tilesUrl ],
		"minzoom": 0,
		"maxzoom": 14
	});

	const seqLayerId = `${id}_sequences`;
	mapHandler._map.addLayer({
		"id": seqLayerId,
		"type": "line",
		"source": id,
		"source-layer": "sequences",
		...seqLayerStyle
	});

	const picLayerId = `${id}_pictures`;
	mapHandler._map.addLayer({
		"id": picLayerId,
		"type": "circle",
		"source": id,
		"source-layer": "pictures",
		...picLayerStyle
	});

	// Map interaction events (pointer cursor, click)
	mapHandler._picPopup = new maplibregl.Popup({
		closeButton: false,
		closeOnClick: !isSmall,
		offset: 3
	});
	mapHandler._picPopup.on("close", () => { delete mapHandler._picPopup._picId; });

	mapHandler._map.on("mousemove", picLayerId, e => {
		mapHandler._map.getCanvas().style.cursor = "pointer";
		attachPreviewToPictures(mapHandler, api, e, picLayerId);
	});

	mapHandler._map.on("mouseleave", picLayerId, () => {
		mapHandler._map.getCanvas().style.cursor = "";
		mapHandler._picPopup._picId;
		mapHandler._picPopup.remove();
	});

	mapHandler._map.on("click", picLayerId, e => {
		e.preventDefault();
		// Get first feature
		const f = e.features.pop();
		if(f) {
			onPicClick(f);
		}
	});

	mapHandler._map.on("mousemove", seqLayerId, e => {
		if(mapHandler._map.getZoom() <= 15) {
			mapHandler._map.getCanvas().style.cursor = "pointer";
			if(e.features[0].properties.id) {
				attachPreviewToPictures(mapHandler, api, e, seqLayerId);
			}
		}
	});

	mapHandler._map.on("mouseleave", seqLayerId, () => {
		mapHandler._map.getCanvas().style.cursor = "";
		mapHandler._picPopup._picId;
		mapHandler._picPopup.remove();
	});

	mapHandler._map.on("click", seqLayerId, e => {
		e.preventDefault();
		if(mapHandler._map.getZoom() <= 15) {
			onSeqClick(e.features[0].properties.id, e.lngLat);
		}
	});

	mapHandler._map.on("click", (e) => {
		if(e.defaultPrevented === false) {
			mapHandler._picPopup.remove();
		}
	});
}

/**
 * Forces reload of pictures/sequences layer styles.
 * This is useful after a map theme change.
 * @param {object} mapHandler The main object containing a MaplibreGL "_map" property
 */
export function reloadLayersStyles(mapHandler) {
	const updateStyle = (layer, style) => {
		[...mapHandler._userLayers].forEach(dl => {
			for(let p in style.layout) {
				mapHandler._map.setLayoutProperty(`${dl}_${layer}`, p, style.layout[p]);
			}
			for(let p in style.paint) {
				mapHandler._map.setPaintProperty(`${dl}_${layer}`, p, style.paint[p]);
			}
		});
	};
	updateStyle("pictures", mapHandler._getPicturesLayerStyleProperties());
	updateStyle("sequences", mapHandler._getSequencesLayerStyleProperties());
}

/**
 * Filter the visible data content in all visible map layers
 * @param {object} mapHandler The main object containing a MaplibreGL "_map" property
 * @param {string} dataType sequences or pictures
 * @param {object} filter The MapLibre GL filter rule to apply
 */
export function filterUserLayersContent(mapHandler, dataType, filter) {
	[...mapHandler._userLayers].forEach(dl => {
		mapHandler._map.setFilter(`${dl}_${dataType}`, filter);
	});
}

/**
 * Make given user layers visible on map, and hide all others (if any)
 * 
 * @param {object} mapHandler The main object containing a MaplibreGL "_map" property
 * @param {string[]} visibleIds The user layers IDs to display
 * @param {API} api The API handler
 * @param {object} seqLayerStyle The MaplibreGL JS style for sequences
 * @param {object} picLayerStyle The MaplibreGL JS style for pictures
 * @param {function} onPicClick Click handler for pictures layer
 * @param {function} onSeqClick Click handler for sequences layer
 */
export function switchUserLayers(
	mapHandler, visibleIds, api,
	seqLayerStyle, picLayerStyle,
	onPicClick, onSeqClick
) {
	// Create any missing user layer
	visibleIds.filter(id => !mapHandler._userLayers.has(id)).forEach(id => {
		createPicturesTilesLayer(
			mapHandler,
			api,
			id === "geovisio" ? api.getPicturesTilesUrl() : api.getUserPicturesTilesUrl(id),
			mapHandler?.viewer?.isSmall() || false,
			id,
			seqLayerStyle,
			picLayerStyle,
			onPicClick,
			onSeqClick
		);
	});

	// Switch visibility
	[...mapHandler._userLayers].forEach(l => {
		mapHandler._map.setLayoutProperty(`${l}_pictures`, "visibility", visibleIds.includes(l) ? "visible" : "none");
		mapHandler._map.setLayoutProperty(`${l}_sequences`, "visibility", visibleIds.includes(l) ? "visible" : "none");
	});
}

/**
 * Creates popup manager for preview of pictures.
 * 
 * @param {object} mapHandler The main object containing a MaplibreGL "_map" property
 */
export function reloadVectorTiles(mapHandler) {
	[...mapHandler._userLayers].forEach(dl => {
		const s = mapHandler._map.getSource(dl);
		s.setTiles(s.tiles);
	});
}

/**
 * Creates popup manager for preview of pictures.
 * 
 * @param {object} mapHandler The main object containing a MaplibreGL "_map" property
 * @param {API} api The API handler
 * @param {object} e The event thrown by MapLibre
 * @param {string} from The event source layer
 */
export function attachPreviewToPictures(mapHandler, api, e, from) {
	let f = e.features[0];
	if(!f || f.properties.id == mapHandler._picPopup._picId) { return; }

	let coordinates = from.endsWith("pictures") ? f.geometry.coordinates.slice() : e.lngLat;

	// If no coordinates found, find from geometry (nearest to map center)
	if(!coordinates) {
		const coords = f.geometry.type === "LineString" ? [f.geometry.coordinates] : f.geometry.coordinates;
		let prevDist = null;
		const mapBbox = mapHandler._map.getBounds();
		const mapCenter = mapBbox.getCenter();
		for(let i=0; i < coords.length; i++) {
			for(let j=0; j < coords[i].length; j++) {
				if(mapBbox.contains(coords[i][j])) {
					let dist = mapCenter.distanceTo(new maplibregl.LngLat(...coords[i][j]));
					if(prevDist === null || dist < prevDist) {
						coordinates = coords[i][j];
						prevDist = dist;
					}
				}
			}
		}

		if(!coordinates) { return; }
	}

	// Display thumbnail
	mapHandler._picPopup
		.setLngLat(coordinates)
		.addTo(mapHandler._map);
	
	// Only show GIF loader if thumbnail is not in browser cache
	if(!mapHandler._picThumbUrl[f.properties.id]) {
		mapHandler._picPopup.setDOMContent(mapHandler._thumbGif);
	}

	mapHandler._picPopup._loading = f.properties.id;
	mapHandler._picPopup._picId = f.properties.id;

	const displayThumb = thumbUrl => {
		if(mapHandler._picPopup._loading === f.properties.id) {
			delete mapHandler._picPopup._loading;

			if(thumbUrl) {
				let content = document.createElement("img");
				content.classList.add("gvs-map-thumb");
				content.alt = mapHandler._lang.thumbnail;
				let img = new Image();
				img.src = thumbUrl;

				img.addEventListener("load", () => {
					if(f.properties.hidden) {
						content.children[0].src = img.src;
					}
					else {
						content.src = img.src;
					}
					mapHandler._picPopup.setDOMContent(content);
				});

				if(f.properties.hidden) {
					const legend = document.createElement("div");
					legend.classList.add("gvs-map-thumb-legend");
					legend.appendChild(document.createTextNode(mapHandler._lang.not_public));
					const container = document.createElement("div");
					container.appendChild(content);
					container.appendChild(legend);
					content = container;
				}
			}
			else {
				mapHandler._picPopup.setHTML(`<i>${mapHandler._lang.no_thumbnail}</i>`);
			}
		}
	};

	// Click on a single picture
	if(from.endsWith("pictures")) {
		getPictureThumbURL(mapHandler, api, f.properties.id).then(displayThumb);
	}
	// Click on a sequence + sequences pictures in cache
	else if(mapHandler._seqPictures[f.properties.id]) {
		getSequenceThumbURL(mapHandler, api, f.properties.id, coordinates).then(displayThumb);
	}
	// Click on a sequence + no sequences pictures in cache
	else {
		Promise.race([
			getSequenceThumbURL(mapHandler, api, f.properties.id),
			getSequenceThumbURL(mapHandler, api, f.properties.id, coordinates)
		]).then(displayThumb);
	}
}

/**
 * Get picture thumbnail URL for a given picture ID.
 * It handles a client-side cache based on raw API responses.
 *
 * @param {object} mapHandler The main object containing a MaplibreGL "_map" property
 * @param {API} api The API handler
 * @param {string} picId The picture ID
 * @param {string} [seqId] The sequence ID (can speed up search if available)
 * @returns {Promise} Promise resolving on picture thumbnail URL, or null on timeout
 * 
 * @private
 */
export function getPictureThumbURL(mapHandler, api, picId, seqId) {
	let res = null;

	if(picId) {
		if(mapHandler._picThumbUrl[picId] !== undefined) {
			res = typeof mapHandler._picThumbUrl[picId] === "string" ? Promise.resolve(mapHandler._picThumbUrl[picId]) : mapHandler._picThumbUrl[picId];
		}
		else {
			mapHandler._picThumbUrl[picId] = api.getPictureThumbnailURL(picId, seqId).then(url => {
				if(url) {
					mapHandler._picThumbUrl[picId] = url;
					return url;
				}
				else {
					mapHandler._picThumbUrl[picId] = null;
					return null;
				}
			})
				.catch(() => {
					mapHandler._picThumbUrl[picId] = null;
				});
			res = mapHandler._picThumbUrl[picId];
		}
	}

	return res;
}

/**
 * Get picture thumbnail URL for a given sequence ID
 * It handles a client-side cache based on raw API responses.
 *
 * @param {object} mapHandler The main object containing a MaplibreGL "_map" property
 * @param {API} api The API handler
 * @param {string} seqId The sequence ID
 * @param {LngLat} [coordinates] The map coordinates
 * @returns {Promise} Promise resolving on picture thumbnail URL, or null on timeout
 */
function getSequenceThumbURL(mapHandler, api, seqId, coordinates) {
	if(coordinates) {
		return getPictureIdForSequence(mapHandler, api, seqId, coordinates)
			.then(picId => {
				if(mapHandler._seqPictures[seqId]) {
					const pic = mapHandler._seqPictures[seqId].find(p => p.id === picId);
					if(pic) {
						return api.findThumbnailInPictureFeature(pic);
					}
				}
				
				return api.getPictureThumbnailURL(picId, seqId);
			});
	}
	else {
		return api.getPictureThumbnailURLForSequence(seqId);
	}
}

/**
 * Get a picture ID for a given sequence (near given coordinates)
 *
 * @param {object} mapHandler The main object containing a MaplibreGL "_map" property
 * @param {API} api The API handler
 * @param {string} seqId The sequence ID
 * @param {LngLat} coords The map coordinates to use
 * @returns {Promise} Promise resolving on picture ID, or null on timeout
 * @private
 */
export function getPictureIdForSequence(mapHandler, api, seqId, coords) {
	// Find nearest picture
	const findPic = () => {
		if(!mapHandler._seqPictures[seqId]) { return null; }

		let minDist, picId = null;
		mapHandler._seqPictures[seqId].forEach((p) => {
			const d = getDistance([coords.lng, coords.lat], p.geometry.coordinates);
			if(!picId || d <= minDist) {
				minDist = d;
				picId = p.id;
			}
		});
		return picId;
	};

	if(!mapHandler._seqPictures[seqId]) {
		mapHandler._seqPictures[seqId] = api.getSequencePictures(seqId)
			.then(res => {
				mapHandler._seqPictures[seqId] = res;
				return findPic();
			})
			.catch(e => {
				console.error(e);
				mapHandler._seqPictures[seqId] = null;
				return null;
			});
		return mapHandler._seqPictures[seqId];
	}
	else if(Array.isArray(mapHandler._seqPictures[seqId])) {
		return Promise.resolve(findPic());
	}
	else if (mapHandler._seqPictures[seqId] instanceof Promise) {
		return mapHandler._seqPictures[seqId];
	}
	else {
		return Promise.resolve(null);
	}
}

/**
 * Transforms a set of parameters into an URL-ready string
 * It also removes null/undefined values
 *
 * @param {object} params The parameters object
 * @return {string} The URL query part
 * @private
 */
export function geocoderParamsToURLString(params) {
	let p = {};
	Object.entries(params)
		.filter(e => e[1] !== undefined && e[1] !== null)
		.forEach(e => p[e[0]] = e[1]);

	return new URLSearchParams(p).toString();
}

/**
 * Nominatim (OSM) geocoder, ready to use for our Map
 */
export function forwardGeocodingNominatim(config) {
	// Transform parameters into Nominatim format
	const params = {
		q: config.query,
		countrycodes: config.countries,
		limit: config.limit,
		viewbox: config.bbox,
	};

	return fetch(`https://nominatim.openstreetmap.org/search?${geocoderParamsToURLString(params)}&format=geojson&polygon_geojson=1&addressdetails=1`)
		.then(res => res.json())
		.then(res => {
			const finalRes = { features: [] };
			const listedNames = [];
			res.features.forEach(f => {
				if(!listedNames.includes(f.properties.display_name)) {
					finalRes.features.push({
						place_type: ["place"],
						place_name: f.properties.display_name,
						center: [
							f.bbox[0] +
						(f.bbox[2] - f.bbox[0]) / 2,
							f.bbox[1] +
						(f.bbox[3] - f.bbox[1]) / 2
						],
						...f
					});
					listedNames.push(f.properties.display_name);
				}
			});
			return finalRes;
		});
}

/**
 * Base adresse nationale (FR) geocoder, ready to use for our Map
 * @param {object} config Configuration sent by MapLibre GL Geocoder, following the geocoderApi format ( https://github.com/maplibre/maplibre-gl-geocoder/blob/main/API.md#setgeocoderapi )
 * @returns {object} GeoJSON Feature collection in Carmen GeoJSON format
 */
export function forwardGeocodingBAN(config) {
	// Transform parameters into BAN format
	const params = { q: config.query, limit: config.limit };
	if(typeof config.proximity === "string") {
		const [lat, lon] = config.proximity.split(",").map(v => parseFloat(v.trim()));
		params.lat = lat;
		params.lon = lon;
	}

	const toPlaceName = p => [p.name, p.district, p.city].filter(v => v).join(", ");

	return fetch(`https://api-adresse.data.gouv.fr/search/?${geocoderParamsToURLString(params)}`)
		.then(res => res.json())
		.then(res => {
			res.features = res.features.map(f => ({
				place_type: ["place"],
				place_name: toPlaceName(f.properties),
				center: f.geometry.coordinates,
				...f
			}));
			return res;
		});
}