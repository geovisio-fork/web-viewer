import Viewer from "../Viewer";
import URLHash from "../URLHash";

jest.mock("../Viewer", () => (function () {
	return {
		addEventListener: jest.fn(),
		removeEventListener: jest.fn(),
		goToPicture: jest.fn(),
		setFocus: jest.fn(),
		setXYZ: jest.fn(),
		setFilters: jest.fn(),
		isMapWide: jest.fn(),
		switchMapBackground: jest.fn(),
		switchVisibleUsers: jest.fn(),
		getVisibleUsers: () => ["geovisio"],
		popupContainer: {
			classList: {
				contains: jest.fn(),
			},
		},
		map: {
			_map: {
				on: jest.fn(),
				off: jest.fn(),
				getCenter: () => jest.fn(),
				getZoom: jest.fn(),
				dragRotate: { isEnabled: () => true },
				touchZoomRotate: { isEnabled: () => true },
				getBearing: jest.fn(),
				jumpTo: jest.fn(),
				getPitch: jest.fn(),
			},
		},
		getPictureMetadata: jest.fn(),
		getTransitionDuration: jest.fn(),
		setTransitionDuration: jest.fn(),
	};
}));

describe("constructor", () => {
	it("inits", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh).toBeDefined();
		expect(uh._viewer).toBe(v);
	});
});

describe("destroy", () => {
	it("works", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh).toBeDefined();
		expect(uh._viewer).toBe(v);
		uh.destroy();
		expect(uh._viewer).toBeUndefined();
		expect(v.removeEventListener.mock.calls.length).toBe(7);
	});
});

describe("bindMapEvents", () => {
	it("works", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		uh.bindMapEvents();
		expect(v.map._map.on.mock.calls.length).toBe(1);
		expect(v.map._map.on.mock.calls[0][0]).toBe("moveend");
		expect(typeof v.map._map.on.mock.calls[0][1]).toBe("function");
	});
});

describe("getHashString", () => {
	it("works without any specific values set", () => {
		const v = new Viewer();
		v.getPictureMetadata = () => null;
		delete v.map;
		const uh = new URLHash(v);
		expect(uh.getHashString()).toBe("#");
	});

	it("works with picture metadata", () => {
		const v = new Viewer();
		v.getPictureMetadata = () => ({ "id": "cbfc3add-8173-4464-98c8-de2a43c6a50f" });
		delete v.map;
		const uh = new URLHash(v);
		uh._getXyzHashString = () => "0/1/2";
		expect(uh.getHashString()).toBe("#pic=cbfc3add-8173-4464-98c8-de2a43c6a50f&xyz=0/1/2");
	});

	it("works with map started + wide", () => {
		const v = new Viewer();
		v.isMapWide = () => true;
		v.getPictureMetadata = () => null;
		v.popupContainer.classList.contains = () => true;
		const uh = new URLHash(v);
		uh._getMapHashString = () => "18/0.5/-12";
		expect(uh.getHashString()).toBe("#focus=map&map=18/0.5/-12");
	});

	it("works with map + picture wide", () => {
		const v = new Viewer();
		v.getPictureMetadata = () => ({ "id": "cbfc3add-8173-4464-98c8-de2a43c6a50f" });
		v.isMapWide = () => false;
		v.popupContainer.classList.contains = () => true;
		const uh = new URLHash(v);
		uh._getXyzHashString = () => "0/1/2";
		uh._getMapHashString = () => "18/0.5/-12";
		expect(uh.getHashString()).toBe("#focus=pic&map=18/0.5/-12&pic=cbfc3add-8173-4464-98c8-de2a43c6a50f&xyz=0/1/2");
	});

	it("works with map filters", () => {
		const v = new Viewer();
		v.isMapWide = () => false;
		v.popupContainer.classList.contains = () => true;
		v._mapFilters = {
			"minDate": "2023-01-01",
			"maxDate": "2023-08-08",
			"camera": "sony",
			"type": "flat",
			"theme": "age",
		};
		const uh = new URLHash(v);
		uh._getMapHashString = () => "18/0.5/-12";
		expect(uh.getHashString()).toBe("#camera=sony&date_from=2023-01-01&date_to=2023-08-08&focus=pic&map=18/0.5/-12&pic_type=flat&theme=age");
	});

	it("works with speed", () => {
		const v = new Viewer();
		v.getTransitionDuration = () => 250;
		delete v.map;
		const uh = new URLHash(v);
		expect(uh.getHashString()).toBe("#speed=250");
	});

	it("works with popup", () => {
		const v = new Viewer();
		v.popupContainer.classList.contains = () => false;
		const uh = new URLHash(v);
		uh._getMapHashString = () => "18/0.5/-12";
		expect(uh.getHashString()).toBe("#focus=meta&map=18/0.5/-12");
	});
});

describe("_getCurrentHash", () => {
	it("works if empty", () => {
		delete window.location;
		window.location = { hash: "" };
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh._getCurrentHash()).toStrictEqual({});
	});

	it("works with single param", () => {
		delete window.location;
		window.location = { hash: "#a=b" };
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh._getCurrentHash()).toStrictEqual({"a": "b"});
	});

	it("works with multiple params", () => {
		delete window.location;
		window.location = { hash: "#a=b&c=d" };
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh._getCurrentHash()).toStrictEqual({"a": "b", "c": "d"});
	});
});

describe("_getMapHashString", () => {
	it("works with zoom+center", () => {
		const v = new Viewer();
		v.map._map = {
			getZoom: () => 18,
			getCenter: () => ({ lng: -12.5, lat: 48.75 }),
			getBearing: () => null,
			getPitch: () => null,
		};

		const uh = new URLHash(v);
		expect(uh._getMapHashString()).toBe("18/48.75/-12.5");
	});

	it("works with zoom+center+bearing", () => {
		const v = new Viewer();
		v.map._map = {
			getZoom: () => 18,
			getCenter: () => ({ lng: -12.5, lat: 48.75 }),
			getBearing: () => 12,
			getPitch: () => null,
		};

		const uh = new URLHash(v);
		expect(uh._getMapHashString()).toBe("18/48.75/-12.5/12");
	});

	it("works with zoom+center+pitch", () => {
		const v = new Viewer();
		v.map._map = {
			getZoom: () => 18,
			getCenter: () => ({ lng: -12.5, lat: 48.75 }),
			getBearing: () => null,
			getPitch: () => 65,
		};

		const uh = new URLHash(v);
		expect(uh._getMapHashString()).toBe("18/48.75/-12.5/0/65");
	});

	it("works with zoom+center+bearing+pitch", () => {
		const v = new Viewer();
		v.map._map = {
			getZoom: () => 18,
			getCenter: () => ({ lng: -12.5, lat: 48.75 }),
			getBearing: () => 42,
			getPitch: () => 65,
		};

		const uh = new URLHash(v);
		expect(uh._getMapHashString()).toBe("18/48.75/-12.5/42/65");
	});
});

describe("_getXyzHashString", () => {
	it("works", () => {
		const v = new Viewer();
		v.getXYZ = () => ({ x: 12, y: 50, z: 75 });
		const uh = new URLHash(v);
		expect(uh._getXyzHashString()).toBe("12.00/50.00/75");
	});

	it("rounds to 2 decimals", () => {
		const v = new Viewer();
		v.getXYZ = () => ({ x: 12.123456, y: 50.789456, z: 75 });
		const uh = new URLHash(v);
		expect(uh._getXyzHashString()).toBe("12.12/50.79/75");
	});

	it("works without z", () => {
		const v = new Viewer();
		v.getXYZ = () => ({ x: 12, y: 50 });
		const uh = new URLHash(v);
		expect(uh._getXyzHashString()).toBe("12.00/50.00/0");
	});
});

describe("_onHashChange", () => {
	it("works", () => {
		const v = new Viewer();
		v.map._map.dragRotate.isEnabled = () => false;
		const uh = new URLHash(v);

		uh._getCurrentHash = () => ({
			pic: "cbfc3add-8173-4464-98c8-de2a43c6a50f",
			focus: "map",
			xyz: "1/2/3",
			map: "15/48.7/-12.5",
			speed: "300",
		});

		uh._onHashChange();

		expect(v.goToPicture.mock.calls).toEqual([["cbfc3add-8173-4464-98c8-de2a43c6a50f"]]);
		expect(v.setFocus.mock.calls).toEqual([["map"]]);
		expect(v.map._map.jumpTo.mock.calls).toEqual([[{ center: [-12.5, 48.7], zoom: 15, pitch: 0 }]]);
		expect(v.setXYZ.mock.calls).toEqual([[1, 2, 3]]);
		expect(v.setTransitionDuration.mock.calls).toEqual([["300"]]);
	});

	it("doesnt call map if no map params", () => {
		const v = new Viewer();
		const uh = new URLHash(v);

		uh._getCurrentHash = () => ({
			pic: "cbfc3add-8173-4464-98c8-de2a43c6a50f",
			xyz: "1/2/3",
		});

		uh._onHashChange();

		expect(v.goToPicture.mock.calls).toEqual([["cbfc3add-8173-4464-98c8-de2a43c6a50f"]]);
		expect(v.setFocus.mock.calls.length).toBe(0);
		expect(v.map._map.jumpTo.mock.calls.length).toBe(0);
		expect(v.setXYZ.mock.calls).toEqual([[1, 2, 3]]);
	});

	it("doesnt call psv if no related params", () => {
		const v = new Viewer();
		v.map._map.dragRotate.isEnabled = () => false;
		const uh = new URLHash(v);

		uh._getCurrentHash = () => ({
			focus: "map",
			map: "15/48.7/-12.5"
		});

		uh._onHashChange();

		expect(v.goToPicture.mock.calls.length).toEqual(0);
		expect(v.setFocus.mock.calls).toEqual([["map"]]);
		expect(v.map._map.jumpTo.mock.calls).toEqual([[{ center: [-12.5, 48.7], zoom: 15, pitch: 0 }]]);
		expect(v.setXYZ.mock.calls.length).toEqual(0);
	});
});

describe("getMapOptionsFromHashString", () => {
	it("works without map", () => {
		const v = new Viewer();
		delete v.map;
		const uh = new URLHash(v);
		expect(uh.getMapOptionsFromHashString("18/-12.5/48.7")).toEqual({ center: [48.7, -12.5], zoom: 18, pitch: 0 });
	});

	it("works with map", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh.getMapOptionsFromHashString("18/-12.5/48.7/15/12")).toEqual({ center: [48.7, -12.5], zoom: 18, pitch: 12, bearing: 15 });
	});

	it("nulls if string is invalid", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh.getMapOptionsFromHashString("bla/bla/bla")).toBeNull();
	});
});

describe("getXyzOptionsFromHashString", () => {
	it("works", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh.getXyzOptionsFromHashString("18/-12.5/48.7")).toEqual({ x: 18, y: -12.5, z: 48.7 });
	});

	it("nulls if string is invalid", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh.getXyzOptionsFromHashString("bla/bla/bla")).toBeNull();
	});
});

describe("getMapFiltersFromHashVals", () => {
	it("works", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		const vals = {
			"date_from": "2023-01-01",
			"date_to": "2023-05-05",
			"pic_type": "equirectangular",
			"camera": "sony",
			"whatever": "whenever",
			"theme": "type",
		};
		expect(uh.getMapFiltersFromHashVals(vals)).toEqual({
			"minDate": "2023-01-01",
			"maxDate": "2023-05-05",
			"type": "equirectangular",
			"camera": "sony",
			"theme": "type",
		});
	});
});

describe("_updateHash", () => {
	it("works", async () => {
		delete window.history;
		delete window.location;

		window.history = { replaceState: jest.fn(), state: {} };
		window.location = { href: "http://localhost:5000/#a=b&b=c" };

		const v = new Viewer();
		const uh = new URLHash(v);
		uh.getHashString = () => "#c=d";

		uh._updateHash();
		await new Promise((r) => setTimeout(r, 1000));

		expect(window.history.replaceState.mock.calls).toEqual([[{}, null, "http://localhost:5000/#c=d"]]);
	});

	it("deduplicates calls", async () => {
		delete window.history;
		delete window.location;

		window.history = { replaceState: jest.fn(), state: {} };
		window.location = { href: "http://localhost:5000/#a=b&b=c" };

		const v = new Viewer();
		const uh = new URLHash(v);
		uh.getHashString = () => "#c=d";

		for(let i=0; i <= 10; i++) {
			uh._updateHash();
		}

		await new Promise((r) => setTimeout(r, 1000));

		expect(window.history.replaceState.mock.calls).toEqual([[{}, null, "http://localhost:5000/#c=d"]]);
	});
});
