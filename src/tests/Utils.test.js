import * as Utils from "../Utils";

describe("getDistance", () => {
	it("works", () => {
		const p1 = [1,1];
		const p2 = [2,2];
		const res = Utils.getDistance(p1, p2);
		expect(res).toBe(Math.sqrt(2));
	});
});

describe("sortPicturesInDirection", () => {
	it("works with next/prev links", () => {
		const sort = Utils.sortPicturesInDirection([0,0]);
		let res = sort({rel: "prev"}, {rel: "next"});
		expect(res).toBe(0);
		res = sort({rel: "prev"}, {rel: "related"});
		expect(res).toBe(-1);
		res = sort({rel: "related"}, {rel: "next"});
		expect(res).toBe(1);
	});

	it("works with related at different dates", () => {
		const sort = Utils.sortPicturesInDirection([0,0]);
		let res = sort(
			{rel: "related", date: "2023-01-01"},
			{rel: "related", date: "2022-01-01"}
		);
		expect(res).toBe(-1); // Most recent goes first
		res = sort(
			{rel: "related", date: "2022-01-01"},
			{rel: "related", date: "2023-01-01"}
		);
		expect(res).toBe(1);
	});

	it("works with related at same date", () => {
		const sort = Utils.sortPicturesInDirection([0,0]);
		let res = sort(
			{rel: "related", date: "2023-01-01", geometry: {coordinates: [0.1,0.1]}},
			{rel: "related", date: "2023-01-01", geometry: {coordinates: [0.5,0.5]}}
		);
		expect(res).toBeLessThan(0); // Nearest goes first
		res = sort(
			{rel: "related", date: "2023-01-01", geometry: {coordinates: [0.5,0.5]}},
			{rel: "related", date: "2023-01-01", geometry: {coordinates: [0.1,0.1]}}
		);
		expect(res).toBeGreaterThan(0);
		res = sort(
			{rel: "related", date: "2023-01-01", geometry: {coordinates: [0.1,0.1]}},
			{rel: "related", date: "2023-01-01", geometry: {coordinates: [0.1,0.1]}}
		);
		expect(res).toBe(0);
	});
});

describe('getSimplifiedAngle', () => {
	it('returns "N"', () => {
		expect(Utils.getSimplifiedAngle([0, 0], [0, 1])).toBe("N");
	});

	it('returns "ENE"', () => {
		expect(Utils.getSimplifiedAngle([0, 0], [1, 1])).toBe("ENE");
	});

	it('returns "ESE"', () => {
		expect(Utils.getSimplifiedAngle([0, 0], [1, -1])).toBe("ESE");
	});

	it('returns "S"', () => {
		expect(Utils.getSimplifiedAngle([0, 0], [0, -1])).toBe("S");
	});

	it('returns "WNW"', () => {
		expect(Utils.getSimplifiedAngle([0, 0], [-1, 1])).toBe("WNW");
	});

	it('returns "WSW"', () => {
		expect(Utils.getSimplifiedAngle([0, 0], [-1, -1])).toBe("WSW");
	});
});