import Map from "../Map";
import Viewer from "../Viewer";
import { forwardGeocodingBAN, forwardGeocodingNominatim } from "../MapUtils";

jest.mock("maplibre-gl", () => ({
	addProtocol: jest.fn(),
	GeolocateControl: jest.fn(),
	Map: function(){
		return {
			addControl: jest.fn(),
			on: jest.fn(),
			resize: jest.fn(),
			addSource: jest.fn(),
			addLayer: jest.fn(),
			loaded: jest.fn(),
			remove: jest.fn(),
			setLayoutProperty: jest.fn(),
			setPaintProperty: jest.fn(),
		};
	},
	AttributionControl: function() {
		return {
			_container: { classList: { add: jest.fn() } },
			onRemove: jest.fn(),
		};
	},
	Marker: jest.fn(),
	Popup: function(){
		return {
			on: jest.fn()
		};
	}
}));

jest.mock("../Viewer", () => (function (mapContainer) {
	return {
		mapContainer,
		isMapWide: () => false,
		isSmall: () => false,
		addEventListener: jest.fn(),
		_api: {
			getDataBbox: () => Promise.resolve(),
			getPicturesTilesUrl: () => "http://localhost:5000/api/map/",
			getExamplePictureMetadataForSequence: () => Promise.resolve({ id: "12345" }),
			onceReady: () => Promise.resolve(),
		},
	};
}));

describe("constructor", () => {
	it("inits", () => {
		const d = document.createElement("div");
		const p = new Viewer(d);
		const m = new Map(p);
		expect(m).toBeDefined();
	});
});

describe("destroy", () => {
	it("works", () => {
		const d = document.createElement("div");
		const p = new Viewer(d);
		const m = new Map(p);
		expect(m).toBeDefined();
		m.destroy();
		expect(m._map).toBeUndefined();
		expect(m._attribution).toBeUndefined();
		expect(m.viewer).toBeUndefined();
	});
});

describe("_initGeocoder", () => {
	it("works", () => {
		const m = new Map(new Viewer());
		m._initGeocoder();
		expect(m.geocoder).toEqual(forwardGeocodingNominatim);
	});

	it("handles geocoder engine param", () => {
		const m = new Map(new Viewer());
		m._initGeocoder({ engine: "ban" });
		expect(m.geocoder).toEqual(forwardGeocodingBAN);
	});
});

describe("_getPictureMarker", () => {
	it("works", () => {
		const m = new Map(new Viewer());
		const res = m._getPictureMarker();
		expect(res).toBeDefined();
	});
});

describe("_listenToViewerEvents", () => {
	it("works", () => {
		const m = new Map(new Viewer());
		m._listenToViewerEvents();
		expect(m.viewer.addEventListener.mock.calls).toMatchSnapshot();
	});
});
