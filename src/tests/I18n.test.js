import { getTranslations } from "../I18n";

describe("getTranslations", () => {
	it("works with default lang", () => {
		const res = getTranslations("en");
		expect(res.map.loading).toBe("Loading...");
	});

	it("works with existing lang", () => {
		const res = getTranslations("fr");
		expect(res.map.loading).toBe("Chargement...");
	});

	it("fallbacks to English if lang not found", () => {
		const res = getTranslations("de");
		expect(res.map.loading).toBe("Loading...");
	});

	it("fallbacks to English if lang is undefined", () => {
		const res = getTranslations();
		expect(res.map.loading).toBe("Loading...");
	});

	it("works when primary lang code exists", () => {
		const res = getTranslations("fr_FR");
		expect(res.map.loading).toBe("Chargement...");
	});
});
