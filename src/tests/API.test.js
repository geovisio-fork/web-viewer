import API from "./../API";

const ENDPOINT = "https://panoramax.ign.fr/api";
const VALID_LANDING = {
	stac_version: "1.0.0",
	links: [
		{ "rel": "data", "type": "application/rss+xml", "href": ENDPOINT+"/collections?format=rss" },
		{ "rel": "data", "type": "application/json", "href": ENDPOINT+"/collections" },
		{ "rel": "search", "type": "application/geo+json", "href": ENDPOINT+"/search" },
		{ "rel": "xyz", "type": "application/vnd.mapbox-vector-tile", "href": ENDPOINT+"/map/{z}/{x}/{y}.mvt" },
		{ "rel": "collection-preview", "type": "image/jpeg", "href": ENDPOINT+"/collections/{id}/thumb.jpg" },
		{ "rel": "item-preview", "type": "image/jpeg", "href": ENDPOINT+"/pictures/{id}/thumb.jpg" },
	],
	"extent": {
		"spatial": {
			"bbox": [[-0.586, 0, 6.690, 49.055]]
		},
		"temporal": {
			"interval": [[ "2019-08-18T14:11:29+00:00", "2023-05-30T18:16:21.167000+00:00" ]]
		}
	}
};
const LANDING_NO_PREVIEW = {
	stac_version: "1.0.0",
	links: [
		{ "rel": "data", "type": "application/json", "href": ENDPOINT+"/collections" },
		{ "rel": "search", "type": "application/geo+json", "href": ENDPOINT+"/search" },
	]
};

describe("constructor", () => {
	it("works with valid endpoint", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		expect(api._endpoint).toBe(ENDPOINT);
	});

	it("works with relative path", () => {
		const api = new API("/api", { skipReadLanding: true });
		expect(api._endpoint).toBe("http://localhost/api");
	});

	it("handles tiles overrides", () => {
		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => Promise.resolve(VALID_LANDING)
		}));

		const api = new API(ENDPOINT, { tiles: "https://my.custom.tiles/" });
		return api.onceReady().then(() => {
			expect(api._endpoint).toBe(ENDPOINT);
			expect(api._endpoints.tiles).toBe("https://my.custom.tiles/");
		});
	});

	it("fails if endpoint is invalid", () => {
		expect(() => new API("not an url")).toThrow("endpoint parameter is not a valid URL: not an url");
	});

	it("fails if endpoint is empty", () => {
		expect(() => new API()).toThrow("endpoint parameter is empty or not a valid string");
	});

	it("accepts fetch options", () => {
		const api = new API("/api", { skipReadLanding: true, fetch: { bla: "bla" } });
		expect(api._getFetchOptions()).toEqual({ bla: "bla" });
	});
});

describe("_parseLanding", () => {
	global.console = { warn: jest.fn() };
	
	it("handles overrides for tiles URL", () => {
		const api = new API (ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING, { tiles: "https://my.custom.tiles/" });
		expect(api._endpoints.tiles).toBe("https://my.custom.tiles/");
	});

	it("fails if landing JSON lacks info", () => {
		const api = new API (ENDPOINT, { skipReadLanding: true });
		expect(() => api._parseLanding({})).toThrow("API Landing page doesn't contain 'links' list");
	});

	it("fails if search link is not valid", () => {
		const api = new API (ENDPOINT, { skipReadLanding: true });
		const landing = {
			stac_version: "1.0.0",
			links: [
				{ "rel": "search", "href": "bla", "type": "application/geo+json" }
			]
		};
		expect(() => api._parseLanding(landing)).toThrow("API search endpoint is not a valid URL: bla");
	});

	it("fails if STAC data link is not valid", () => {
		const api = new API (ENDPOINT, { skipReadLanding: true });
		const landing = {
			stac_version: "1.0.0",
			links: [
				{ "rel": "data", "href": "bla", "type": "application/json" }
			]
		};
		expect(() => api._parseLanding(landing)).toThrow("API data endpoint (application/json) is not a valid URL: bla");
	});

	it("fails if RSS data link is not valid", () => {
		const api = new API (ENDPOINT, { skipReadLanding: true });
		const landing = {
			stac_version: "1.0.0",
			links: [
				{ "rel": "data", "href": "bla", "type": "application/rss+xml" }
			]
		};
		expect(() => api._parseLanding(landing)).toThrow("API data endpoint (application/rss+xml) is not a valid URL: bla");
	});

	it("fails if xyz link is not valid", () => {
		const api = new API (ENDPOINT, { skipReadLanding: true });
		const landing = {
			stac_version: "1.0.0",
			links: [
				{ "rel": "xyz", "href": "bla", "type": "application/vnd.mapbox-vector-tile" }
			]
		};
		expect(() => api._parseLanding(landing)).toThrow("API xyz endpoint is not a valid URL: bla");
	});

	it("fails if collection-preview link is not valid", () => {
		const api = new API (ENDPOINT, { skipReadLanding: true });
		const landing = {
			stac_version: "1.0.0",
			links: [
				{ "rel": "collection-preview", "href": "bla", "type": "image/jpeg" }
			]
		};
		expect(() => api._parseLanding(landing)).toThrow("API collection-preview endpoint is not a valid URL: bla");
	});

	it("fails if item-preview link is not valid", () => {
		const api = new API (ENDPOINT, { skipReadLanding: true });
		const landing = {
			stac_version: "1.0.0",
			links: [
				{ "rel": "item-preview", "href": "bla", "type": "image/jpeg" }
			]
		};
		expect(() => api._parseLanding(landing)).toThrow("API item-preview endpoint is not a valid URL: bla");
	});

	it("fails if API version is not supported", () => {
		const api = new API (ENDPOINT, { skipReadLanding: true });
		const landing = { stac_version: "0.1", links: [] };
		expect(() => api._parseLanding(landing)).toThrow("API is not in a supported version (GeoVisio viewer supports only 1.0, API is 0.1)");
	});

	it("fails if no collection link is set", () => {
		const api = new API (ENDPOINT, { skipReadLanding: true });
		const landing = {
			stac_version: "1.0.0",
			links: [
				{ "rel": "search", "href": "https://geovisio.fr/api/search", "type": "application/geo+json" }
			]
		};
		expect(() => api._parseLanding(landing)).toThrow("API doesn't offer a 'data' endpoint in its links");
	});

	it("fails if no search link is set", () => {
		const api = new API (ENDPOINT, { skipReadLanding: true });
		const landing = {
			stac_version: "1.0.0",
			links: [
				{ "rel": "data", "href": "https://geovisio.fr/api/collections", "type": "application/json" }
			]
		};
		expect(() => api._parseLanding(landing)).toThrow("API doesn't offer a 'search' endpoint in its links");
	});
});

describe("getPicturesAroundCoordinatesUrl", () => {
	it("works with valid coordinates", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		expect(api.getPicturesAroundCoordinatesUrl(48.7, -1.25)).toBe(`${ENDPOINT}/search?bbox=[-1.2505,48.6995,-1.2495,48.7005]`);
	});

	it("fails if coordinates are invalid", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		expect(() => api.getPicturesAroundCoordinatesUrl()).toThrow("lat and lon parameters should be valid numbers");
	});
});

describe("_getMapRequestTransform", () => {
	it("does nothing if no tiles enabled", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(LANDING_NO_PREVIEW);
		expect(api._getMapRequestTransform()).toBe(undefined);
	});

	it("does nothing if no fetch options defined", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		expect(api._getMapRequestTransform()).toBe(undefined);
	});

	it("returns a function with correct options if fetch options defined", () => {
		const api = new API(ENDPOINT, {
			skipReadLanding: true,
			fetch: {
				credentials: "include",
				headers: { "Accept-Header": "Whatever" }
			}
		});
		api._parseLanding(VALID_LANDING);

		// With tiles endpoint called
		const res = api._getMapRequestTransform();
		const res1 = res(ENDPOINT+"/map/8/1234/4567.mvt");
		expect(res1).toEqual({
			url: ENDPOINT+"/map/8/1234/4567.mvt",
			credentials: "include",
			headers: { "Accept-Header": "Whatever" }
		});

		// With external endpoint called
		const res2 = res("https://my-tile-provider.fr/map/1/2/3.mvt");
		expect(res2).toEqual(undefined);
	});
});

describe("getPictureMetadataUrl", () => {
	it("works with valid ID", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		expect(api.getPictureMetadataUrl("whatever-id")).toBe(`${ENDPOINT}/search?ids=whatever-id`);
	});

	it("works with valid ID and sequence", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		expect(api.getPictureMetadataUrl("whatever-id", "my-sequence")).toBe(`${ENDPOINT}/collections/my-sequence/items/whatever-id`);
	});

	it("fails if picId is invalid", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		expect(() => api.getPictureMetadataUrl()).toThrow("picId should be a valid picture unique identifier");
	});
});

describe("getPicturesTilesUrl", () => {
	it("works if URL is set in options", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		expect(api.getPicturesTilesUrl()).toBe(ENDPOINT+"/map/{z}/{x}/{y}.mvt");
	});

	it("fails with custom endpoint and no pictures URL set", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding({
			stac_version: "1.0.0",
			links: [
				{ "rel": "data", "type": "application/json", "href": ENDPOINT+"/collections" },
				{ "rel": "search", "type": "application/geo+json", "href": ENDPOINT+"/search" }
			]
		});
		expect(() => api.getPicturesTilesUrl()).toThrow("Pictures vector tiles URL is unknown");
	});
});

describe("findThumbnailInPictureFeature", () => {
	it("works if a thumbnail exists", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		const res = api.findThumbnailInPictureFeature({
			assets: {
				t: {
					roles: ["thumbnail"],
					type: "image/jpeg",
					href: "https://geovisio.fr/thumb.jpg"
				}
			}
		});
		expect(res).toEqual("https://geovisio.fr/thumb.jpg");
	});

	it("works if a visual exists", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		const res = api.findThumbnailInPictureFeature({
			assets: {
				t: {
					roles: ["visual"],
					type: "image/jpeg",
					href: "https://geovisio.fr/thumb.jpg"
				}
			}
		});
		expect(res).toEqual("https://geovisio.fr/thumb.jpg");
	});

	it("works if no thumbnail is found", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		const res = api.findThumbnailInPictureFeature({});
		expect(res).toBe(null);
	});
});

describe("getPictureThumbnailURLForSequence", () => {
	it("works with a collection-preview endpoint", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		return api.getPictureThumbnailURLForSequence("12345").then(url => {
			expect(url).toBe(ENDPOINT+"/collections/12345/thumb.jpg");
		});
	});

	it("works if a preview is defined in sequence metadata", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(LANDING_NO_PREVIEW);
		const seq = {
			links: [
				{ "type": "image/jpeg", "rel": "preview", "href": "https://geovisio.fr/preview/thumb.jpg" }
			]
		};
		return api.getPictureThumbnailURLForSequence("12345", seq).then(url => {
			expect(url).toBe("https://geovisio.fr/preview/thumb.jpg");
		});
	});


	it("works with an existing sequence", () => {
		const resPicId = "cbfc3add-8173-4464-98c8-de2a43c6a50f";
		const thumbUrl = "http://my.custom.api/pic/thumb.jpg";
		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => Promise.resolve({
				features: [ {
					"id": resPicId,
					"assets": {
						"thumb": {
							"href": thumbUrl,
							"roles": ["thumbnail"],
							"type": "image/jpeg"
						}
					}
				}]
			})
		}));

		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(LANDING_NO_PREVIEW);
		return api.getPictureThumbnailURLForSequence("208b981a-262e-4966-97b6-98ee0ceb8df0").then(url => {
			expect(url).toBe(thumbUrl);
		});
	});

	it("works with no results", () => {
		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => Promise.resolve({
				features: []
			})
		}));

		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(LANDING_NO_PREVIEW);
		return api.getPictureThumbnailURLForSequence("208b981a-262e-4966-97b6-98ee0ceb8df0").then(url => {
			expect(url).toBe(null);
		});
	});
});

describe("getPictureThumbnailURL", () => {
	it("works with a item-preview endpoint", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		return api.getPictureThumbnailURL("12345").then(url => {
			expect(url).toBe(ENDPOINT+"/pictures/12345/thumb.jpg");
		});
	});

	it("works with picture and sequence ID defined", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(LANDING_NO_PREVIEW);

		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => Promise.resolve({
				"assets": {
					"thumb": {
						"href": ENDPOINT+"/pictures/pic1/thumb.jpg",
						"roles": ["thumbnail"],
						"type": "image/jpeg"
					}
				}
			})
		}));

		return api.getPictureThumbnailURL("pic1", "seq1").then(url => {
			expect(url).toBe(ENDPOINT+"/pictures/pic1/thumb.jpg");
		});
	});

	it("works with picture and sequence ID defined, but no thumb found", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(LANDING_NO_PREVIEW);

		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => Promise.resolve({})
		}));

		return api.getPictureThumbnailURL("pic1", "seq1").then(url => {
			expect(url).toBe(null);
		});
	});

	it("works with picture ID defined", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(LANDING_NO_PREVIEW);

		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => Promise.resolve({
				features: [{
					"assets": {
						"thumb": {
							"href": ENDPOINT+"/pictures/pic1/thumb.jpg",
							"roles": ["thumbnail"],
							"type": "image/jpeg"
						}
					}
				}]
			})
		}));

		return api.getPictureThumbnailURL("pic1").then(url => {
			expect(url).toBe(ENDPOINT+"/pictures/pic1/thumb.jpg");
		});
	});

	it("works with picture ID defined but no results", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(LANDING_NO_PREVIEW);

		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => Promise.resolve({
				features: []
			})
		}));

		return api.getPictureThumbnailURL("pic1").then(url => {
			expect(url).toBe(null);
		});
	});
});

describe("getRSSURL", () => {
	it("works without RSS", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(LANDING_NO_PREVIEW);
		expect(api.getRSSURL()).toBeNull();
	});

	it("works with RSS and no bbox", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		expect(api.getRSSURL()).toBe(ENDPOINT+"/collections?format=rss");
	});

	it("works with RSS and bbox with query string", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		const bbox = {
			getSouth: () => -1.7,
			getNorth: () => -1.6,
			getWest: () => 47.1,
			getEast: () => 48.2
		};
		expect(api.getRSSURL(bbox)).toBe(ENDPOINT+"/collections?format=rss&bbox=47.1,-1.7,48.2,-1.6");
	});

	it("works with RSS and bbox without query string", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding({
			stac_version: "1.0.0",
			links: [
				{ "rel": "data", "type": "application/json", "href": ENDPOINT+"/collections" },
				{ "rel": "search", "type": "application/geo+json", "href": ENDPOINT+"/search" },
				{ "rel": "data", "href": ENDPOINT+"/collections", "type": "application/rss+xml" }
			]
		});
		const bbox = {
			getSouth: () => -1.7,
			getNorth: () => -1.6,
			getWest: () => 47.1,
			getEast: () => 48.2
		};
		expect(api.getRSSURL(bbox)).toBe(ENDPOINT+"/collections?bbox=47.1,-1.7,48.2,-1.6");
	});
});

describe("getSequencePictures", () => {
	it("works with results", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);

		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => Promise.resolve({
				features: [{
					id: "12345"
				}]
			})
		}));

		return api.getSequencePictures("999").then(features => {
			expect(features).toEqual([{ "id": "12345" }]);
		});
	});

	it("works with empty results", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);

		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => Promise.resolve({})
		}));

		return api.getSequencePictures("999").then(features => {
			expect(features).toBe(null);
		});
	});
});

describe("getDataBbox", () => {
	it("works with landing spatial extent defined", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(VALID_LANDING);
		expect(api.getDataBbox()).toEqual([[-0.586, 0], [6.690, 49.055]]);
	});

	it("works with no landing spatial extent defined", () => {
		const api = new API(ENDPOINT, { skipReadLanding: true });
		api._parseLanding(LANDING_NO_PREVIEW);
		expect(api.getDataBbox()).toBe(null);
	});
});

describe("isValidHttpUrl", () => {
	it("works with valid endpoint", () => {
		expect(API.isValidHttpUrl(ENDPOINT)).toBeTruthy();
	});

	it("fails if endpoint is invalid", () => {
		expect(API.isValidHttpUrl("not an url")).toBeFalsy();
	});
});
