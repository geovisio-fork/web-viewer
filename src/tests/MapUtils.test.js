import * as MapUtils from "../MapUtils";
import { VECTOR_STYLES } from "../Map";
import PICTURES_EXAMPLE from "./data/Viewer_pictures_1.json";
import GEOCODER_NOMINATIM from "./data/Map_geocoder_nominatim.json";
import GEOCODER_BAN from "./data/Map_geocoder_ban.json";


jest.mock("maplibre-gl", () => ({
	addProtocol: jest.fn(),
	Popup: function() {
		return {
			on: jest.fn(),
		};
	},
	LngLat: jest.fn(),
}));


describe("getThumbGif", () => {
	it("works", () => {
		const lang = { loading: "Loading..." };
		const res = MapUtils.getThumbGif(lang);
		expect(res).toBeDefined();
		expect(res.tagName).toBe("IMG");
		expect(res.alt).toBe(lang.loading);
		expect(res.title).toBe(lang.loading);
	});
});

describe("createPicturesTilesLayer", () => {
	it("works", () => {
		const m = {
			viewer: {
				_api: {},
			},
			_map: {
				addSource: jest.fn(),
				addLayer: jest.fn(),
				on: jest.fn(),
			},
			_userLayers: {
				add: jest.fn(),
			},
		};

		MapUtils.createPicturesTilesLayer(
			m,
			m.viewer._api,
			"https://tiles.server/{z}/{x}/{y}.mvt",
			false,
			"mylayer",
			{},
			{},
			() => {},
			() => {}
		);
		expect(m._map.addSource.mock.calls).toMatchSnapshot();
		expect(m._map.addLayer.mock.calls).toMatchSnapshot();
	});
});

describe("reloadLayersStyles", () => {
	it("works", () => {
		const m = {
			_userLayers: new Set(["geovisio"]),
			_map: {
				setLayoutProperty: jest.fn(),
				setPaintProperty: jest.fn(),
			},
			_getPicturesLayerStyleProperties: () => VECTOR_STYLES.PICTURES,
			_getSequencesLayerStyleProperties: () => VECTOR_STYLES.SEQUENCES,
		};

		MapUtils.reloadLayersStyles(m);
		
		expect(m._map.setLayoutProperty.mock.calls).toMatchSnapshot();
		expect(m._map.setPaintProperty.mock.calls).toMatchSnapshot();
	});
});

describe("filterUserLayersContent", () => {
	it("works", () => {
		const m = {
			_userLayers: new Set(["geovisio"]),
			_map: {
				setFilter: jest.fn(),
			},
		};

		MapUtils.filterUserLayersContent(m, "pictures", {bla:"bla"});
		expect(m._map.setFilter.mock.calls).toMatchSnapshot();
	});
});

describe("switchUserLayers", () => {
	it("works", () => {
		const m = {
			_userLayers: new Set(["geovisio", "1234"]),
			_map: {
				setLayoutProperty: jest.fn(),
			},
			viewer: {
				_api: {},
			},
		};

		MapUtils.switchUserLayers(
			m,
			["1234"],
			m.viewer._api,
			VECTOR_STYLES.SEQUENCES,
			VECTOR_STYLES.PICTURES,
			() => {},
			() => {},
		);
		expect(m._map.setLayoutProperty.mock.calls).toMatchSnapshot();
	});

	it("creates missing layers", () => {
		const m = {
			_userLayers: new Set(["geovisio"]),
			viewer: {
				_api: {
					getUserPicturesTilesUrl: () => "https://my.server/usertiles/{z}/{x}/{y}.mvt",
				},
				isSmall: () => false,
			},
			_map: {
				setLayoutProperty: jest.fn(),
				addSource: jest.fn(),
				addLayer: jest.fn(),
				on: jest.fn(),
			},
		};

		MapUtils.switchUserLayers(
			m,
			["1234"],
			m.viewer._api,
			VECTOR_STYLES.SEQUENCES,
			VECTOR_STYLES.PICTURES,
			() => {},
			() => {},
		);
		expect(m._map.setLayoutProperty.mock.calls).toMatchSnapshot();
	});
});

describe("reloadVectorTiles", () => {
	it("works", () => {
		const setter = jest.fn();
		const m = {
			_userLayers: new Set(["geovisio"]),
			_map: {
				getSource: () => ({
					setTiles: setter,
				}),
			},
		};

		MapUtils.reloadVectorTiles(m);
		expect(setter.mock.calls).toMatchSnapshot();
	});
});

describe("getPictureThumbURL", () => {
	it("finds url against API", async () => {
		const m = {
			viewer: {
				_api: {
					getPictureThumbnailURL: () => Promise.resolve("res")
				}
			},
			_picThumbUrl: {},
		};
		const res = await MapUtils.getPictureThumbURL(m, m.viewer._api, "id");
		expect(res).toBe("res");
	});

	it("hits cache if any", async () => {
		const m = {
			viewer: {
				_api: {
					getPictureThumbnailURL: () => Promise.resolve("res")
				}
			},
			_picThumbUrl: {},
		};
		await MapUtils.getPictureThumbURL(m, m.viewer._api, "id");
		const res = await MapUtils.getPictureThumbURL(m, m.viewer._api, "id");
		expect(res).toBe("res");
		expect(m._picThumbUrl["id"]).toBe("res");
	});

	it("nulls if no response from API", async () => {
		const m = {
			viewer: {
				_api: {
					getPictureThumbnailURL: () => Promise.resolve()
				}
			},
			_picThumbUrl: {},
		};
		const res = await MapUtils.getPictureThumbURL(m, m.viewer._api, "id");
		expect(res).toBeNull();
	});

	it("awaits if a call is still running", async () => {
		let called = 0;
		const m = {
			viewer: {
				_api: {
					getPictureThumbnailURL: () => {
						called++;
						return new Promise(resolve => setTimeout(() => resolve("res"), 200));
					}
				}
			},
			_picThumbUrl: {},
		};
		
		MapUtils.getPictureThumbURL(m, m.viewer._api, "id");
		const res = await MapUtils.getPictureThumbURL(m, m.viewer._api, "id");
		expect(res).toBe("res");
		expect(called).toBe(1);
	});
});

describe("getPictureIdForSequence", () => {
	it("finds ID", async () => {
		const m = { _seqPictures: {}, viewer: { _api: {} } };
		m.viewer._api.getSequencePictures = () => Promise.resolve(PICTURES_EXAMPLE.features);
		const res = await MapUtils.getPictureIdForSequence(m, m.viewer._api, "id", { lng: 2.1822941, lat: 48.9810589 });
		expect(res).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
		expect(m._seqPictures["id"][0].id).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
	});

	it("nulls if no thumb", async () => {
		const m = { _seqPictures: {}, viewer: { _api: {} } };
		m.viewer._api.getSequencePictures = () => Promise.resolve([]);
		const res = await MapUtils.getPictureIdForSequence(m, m.viewer._api, "id", { lng: 2.1822941, lat: 48.9810589 });
		expect(res).toBeNull();
		expect(m._seqPictures["id"]).toEqual([]);
	});

	it("hits cache if any", async () => {
		const m = { _seqPictures: {}, viewer: { _api: {} } };
		m.viewer._api.getSequencePictures = () => Promise.resolve(PICTURES_EXAMPLE.features);
		await MapUtils.getPictureIdForSequence(m, m.viewer._api, "id", { lng: 2.1822941, lat: 48.9810589 });
		const res = await MapUtils.getPictureIdForSequence(m, m.viewer._api, "id", { lng: 2.1822941, lat: 48.9810589 });
		expect(res).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
		expect(m._seqPictures["id"][0].id).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
	});

	it("awaits if a call is still running", async () => {
		const m = { _seqPictures: {}, viewer: { _api: {} } };
		let called = 0;
		m.viewer._api.getSequencePictures = () => {
			called++;
			return new Promise(resolve => setTimeout(() => resolve(PICTURES_EXAMPLE.features), 200));
		};
		MapUtils.getPictureIdForSequence(m, m.viewer._api, "id", { lng: 2.1822941, lat: 48.9810589 });
		const res = await MapUtils.getPictureIdForSequence(m, m.viewer._api, "id", { lng: 2.1822941, lat: 48.9810589 });
		expect(res).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
		expect(m._seqPictures["id"][0].id).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
		expect(called).toBe(1);
	});
});

describe("geocoderParamsToURLString", () => {
	it("works", () => {
		const p = { bla: "blorg", you: 1 };
		const r = MapUtils.geocoderParamsToURLString(p);
		expect(r).toEqual("bla=blorg&you=1");
	});

	it("handles special characters", () => {
		const p = { bbox: "12,14,-45,78" };
		const r = MapUtils.geocoderParamsToURLString(p);
		expect(r).toEqual("bbox=12%2C14%2C-45%2C78");
	});

	it("filters nulls", () => {
		const p = { val1: undefined, val2: null, val3: 0 };
		const r = MapUtils.geocoderParamsToURLString(p);
		expect(r).toEqual("val3=0");
	});
});

describe("forwardGeocodingNominatim", () => {
	it("works", () => {
		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => GEOCODER_NOMINATIM
		}));

		// Search config
		const cfg = { query: "bla", limit: 5, bbox: "17.7,-45.2,17.8,-45.1" };

		return MapUtils.forwardGeocodingNominatim(cfg).then(res => {
			expect(global.fetch.mock.calls).toEqual([["https://nominatim.openstreetmap.org/search?q=bla&limit=5&viewbox=17.7%2C-45.2%2C17.8%2C-45.1&format=geojson&polygon_geojson=1&addressdetails=1"]]);
			expect(res).toMatchSnapshot();
		});
	});
});

describe("forwardGeocodingBAN", () => {
	it("works", () => {
		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => GEOCODER_BAN
		}));

		// Search config
		const cfg = { query: "bla", limit: 5, proximity: "17.7,-45.2" };

		return MapUtils.forwardGeocodingBAN(cfg).then(res => {
			expect(global.fetch.mock.calls).toEqual([["https://api-adresse.data.gouv.fr/search/?q=bla&limit=5&lat=17.7&lon=-45.2"]]);
			expect(res).toMatchSnapshot();
		});
	});
});
